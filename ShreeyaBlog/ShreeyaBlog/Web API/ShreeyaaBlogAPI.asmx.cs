﻿using System;
using System.Collections.Generic;
using System.Data.Entity.Migrations;
using System.IO;
using System.Linq;
using System.Net.Mail;
using System.Security.Claims;
using System.Security.Policy;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.Services;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using Newtonsoft.Json;
using ShreeyaBlog.Classes;
using ShreeyaBlog.Models;
using System.Transactions;
using System.Web.Mvc;
using System.Threading.Tasks;

namespace ShreeyaBlog.Web_API
{
    /// <summary>
    /// Summary description for ShreeyaaBlogAPI
    /// </summary>
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
    [System.Web.Script.Services.ScriptService]
    public class ShreeyaaBlogAPI : System.Web.Services.WebService
    {

        
        ShreeyaBlogEntities dbcontext = new ShreeyaBlogEntities();
        CommentAndReply obj = new CommentAndReply();

        [WebMethod]
        public string HelloWorld()
        {
            return "Hello World";
        }


        //------------------------------------Subscription Begin-----------------------------------------------
        [WebMethod]
        public string bindSubscription()
        {
            ShreeyaBlogEntities dbcontext = new ShreeyaBlogEntities();

            String returnData = "{\"Subscription\":[";

            var _subscription = dbcontext.Subscriptions.Where(c => c.SubscriptionID != 0).OrderBy(n => n.SubscriptionID).ToList();
            if (_subscription.Count != 0)
            {
                var last = _subscription.Last();
                foreach (var loop in _subscription)
                {
                    int ID = loop.SubscriptionID;
                    string Name = loop.Name.ToString();


                    returnData += "{\"SubscriptionID\":\"" + ID
                            + "\",\"Name\":\"" + Name;

                    if (loop.Equals(last))
                        returnData += "\"}";
                    else
                        returnData += "\"},";
                }
            }
            return returnData + "]}";
        }
        //------------------------------------Subscription End-------------------------------------------------



        [WebMethod]
        public string Register(string EmailID, string Password, string ConfirmPassword, string FirstName, string LastName, string MobileNo, int SubscribedPlan, string SubscribedTest)
        {

            TransactionOptions options = new TransactionOptions();
            options.IsolationLevel = System.Transactions.IsolationLevel.ReadCommitted;
            options.Timeout = new TimeSpan(0, 15, 0);
            using (TransactionScope scope = new TransactionScope(TransactionScopeOption.Required, options))
            {
                string returndata = "{\"Registration\":";
                try
                {


                    //var validation = new Regex("([a-zA-Z]{1,})([@$!%*#?&]{1,})([0-9]{1,})");
                    //var validation = new Regex("^(?=.*[a - z])(?=.*[A - Z])(?=.*\\d)(?=.*[^\\da - zA - Z]).{ 8,15}$");
                    var validation = new Regex("^(?=.*[a - z])(?=.*[A - Z])(?=.*\\d)(?=.*[#$^+=!*()@%&]).{8,}$");

                    bool UserFound = false;
                    string alert = ""; int returnProfileID;

                   

                    var userManager = Context.GetOwinContext().GetUserManager<ApplicationUserManager>();
                    var signinManager = Context.GetOwinContext().GetUserManager<ApplicationSignInManager>();
                    UserFound = new UserExistence(EmailID).isExist;

                    var user = new ApplicationUser { UserName = EmailID, Email = EmailID, EmailConfirmed = true };
                    var result = userManager.Create(user, Password);
                    //try
                    //{
                    if (Password.Equals(ConfirmPassword))
                    {
                        //Adding Role to User
                        if (result.Succeeded)
                        {
                            signinManager.SignIn(user, isPersistent: false, rememberBrowser: false);
                            userManager.AddToRole(user.Id, "User");

                            if (UserFound)
                            {
                                alert = "Profile already Exists";
                                returndata += JsonConvert.SerializeObject(alert);
                                return returndata += "}";
                            }
                            else
                            {
                                string filesnames = "default.jpg";

                                Profile pfdata = new Profile()
                                {
                                    Image = filesnames,
                                    FirstName = FirstName,
                                    LastName = LastName,
                                    UserName = EmailID,
                                    //Qualification = Qualification,
                                    //About = About,
                                    //DOB = date,
                                    //Aim = Aim,
                                    //AspiringExams = AspiringExams,
                                    //Address = Address,
                                    MobileNo = MobileNo,
                                    SubscribedPlan = SubscribedPlan,
                                    SubscribedTest = SubscribedTest,
                                    isActive = false,
                                    CreatedDate = DateTime.Now,
                                };
                                dbcontext.Profiles.Add(pfdata);
                                dbcontext.SaveChanges();

                                returnProfileID = pfdata.ProfileID;

                                alert = "Registeration Sucessfull";
                                returndata += JsonConvert.SerializeObject(alert);

                                scope.Complete();

                                return returndata += "}";
                            }
                        }
                        //New Password Validation
                        else
                        {
                            if (UserFound)
                            {
                                alert = "Profile already Exists";
                                returndata += JsonConvert.SerializeObject(alert);
                                return returndata += "}";
                            }
                            else if (validation.IsMatch(ConfirmPassword) || (ConfirmPassword.Length < 8))
                            {
                                alert = "Password Should Contain atleast 1 Uppercase , 1 Special Character with minimum 8 Characters";
                            }
                            else
                            {
                                alert = "Something went wrong ! Please Try Again";
                                returndata += JsonConvert.SerializeObject(alert);
                                return returndata += "}";
                            }
                            returndata += JsonConvert.SerializeObject(alert);
                            return returndata += "}";
                        }
                    }
                    else
                    {
                        alert = "Password and Confirm Password Mismatch";
                        returndata += JsonConvert.SerializeObject(alert);
                        return returndata += "}";
                    }
                    //}
                    //catch (Exception)
                    //{
                    //    alert = "Something went wrong ! Please Try Again";
                    //    returndata += JsonConvert.SerializeObject(alert);
                    //    return returndata += "}";
                    //}
                }
                catch (Exception Ex)
                {

                    scope.Dispose();

                    string alert  = "Error during Registration";
                    returndata += JsonConvert.SerializeObject(alert);
                    return returndata += "}";

                }
            }
        }


        //------------------------------------Registration End-------------------------------------------------

        //------------------------------------Login Begin-----------------------------------------------

        [WebMethod]
        public string Login(string EmailID, string Password)
        {
            int returnProfileID = 0;
            var manager = Context.GetOwinContext().GetUserManager<ApplicationUserManager>();
            var signinManager = Context.GetOwinContext().GetUserManager<ApplicationSignInManager>();
            var userManager = Context.GetOwinContext().GetUserManager<ApplicationUserManager>();

            string returndata = "{\"Profile\":";

            var result = signinManager.PasswordSignIn(EmailID, Password, false, shouldLockout: false);
            switch (result)
            {
                case SignInStatus.Success:
                    var user = userManager.FindAsync(EmailID, Password);

                    var isexists = (from i in dbcontext.Profiles
                                    where i.UserName == EmailID
                                    select new
                                    {
                                        username = i.UserName
                                    }).FirstOrDefault();

                   
                    if (isexists != null)
                    {
                        //string alert = "";
                        var RegUser = (from profile in dbcontext.Profiles
                                       join subcription in dbcontext.Subscriptions on profile.SubscribedPlan equals subcription.SubscriptionID
                                       where profile.UserName == EmailID
                                       select new UserProfile
                                       {
                                           ProfileID = profile.ProfileID,
                                           Image = profile.Image,
                                           FirstName = profile.FirstName,
                                           LastName = profile.LastName,
                                           UserName = profile.UserName,
                                           //Qualification = profile.Qualification,
                                           //About = profile.About,
                                           //DOB = profile.DOB,
                                           //Aim = profile.Aim,
                                           //AspiringExams = profile.AspiringExams,
                                           //Address = profile.Address,
                                           MobileNo = profile.MobileNo,
                                           //SubscribedPlan = subcription.Name,
                                           SubscribedTest = profile.SubscribedTest,
                                           isActive = profile.isActive,
                                           CreatedDate = profile.CreatedDate
                                       }).ToList();

                        Profile UserDetail = dbcontext.Profiles.Where(u => u.UserName == EmailID).FirstOrDefault();
                        returnProfileID = UserDetail.ProfileID;

                        DateTime SubscribedDate = Convert.ToDateTime(UserDetail.CreatedDate);
                        DateTime ExpiryDate = Convert.ToDateTime(null);

                        //Subscription Type
                        var SubscriptionID = UserDetail.SubscribedPlan;
                        int ReamainingDays_1 = 0, ReamainingDays_2 = 0, ReamainingDays_3 = 0, ReamainingDays_4 = 0;
                        switch (SubscriptionID)
                        {
                            case 1:
                                ExpiryDate = SubscribedDate.AddMonths(1);
                                ReamainingDays_1 = (ExpiryDate - DateTime.Now).Days;
                                break;

                            case 2:
                                ExpiryDate = SubscribedDate.AddMonths(3);
                                ReamainingDays_2 = (ExpiryDate - DateTime.Now).Days;
                                break;

                            case 3:
                                ExpiryDate = SubscribedDate.AddMonths(6);
                                ReamainingDays_3 = (ExpiryDate - DateTime.Now).Days;
                                break;

                            case 4:
                                ExpiryDate = SubscribedDate.AddMonths(12);
                                ReamainingDays_4 = (ExpiryDate - DateTime.Now).Days;
                                break;

                        }
                        if (ReamainingDays_1 <= 0 && ReamainingDays_2 <= 0 && ReamainingDays_3 <= 0 && ReamainingDays_4 <= 0)
                        {
                            string tempreturn = "";
                            UserDetail.isActive = false;
                            dbcontext.Profiles.AddOrUpdate(UserDetail);
                            dbcontext.SaveChanges();
                            // return tempreturn += JsonConvert.SerializeObject("Subscription Expired ! Contact Admin");
                            return returndata += "{\"Message\":\"Error\", \"Alert\": \"Subscription Expired ! Contact Admin\" }}";
                        }
                        else if (UserDetail.isActive != true)
                        {
                            return returndata += "{\"Message\":\"Error\", \"Alert\": \"Account Activation is Pending. Contact Admin\" }}";
                            // string tempreturn = "";
                            // return tempreturn += JsonConvert.SerializeObject("Account Activation is Pending. Contact Admin");

                            //alert = "Account Activation is Pending. Contact Admin";
                            //return returndata += JsonConvert.SerializeObject(alert) + "}";
                        }
                        else
                        {

                            Profile pro = (from p in dbcontext.Profiles
                                              where p.UserName == EmailID
                                             select p).SingleOrDefault();

                            pro.AppToken = DateTime.Now.ToString();

                            dbcontext.SaveChanges();

                            String profileData = "{\"Profile\":[";

                            profileData += "{\"ProfileID\":\"" + UserDetail.ProfileID ;
                            profileData += "\",\"Email\":\"" + UserDetail.UserName;
                            profileData += "\",\"AppToken\":\"" + pro.AppToken;

                            profileData += "\"}";

                            return profileData + "]}";


                            //return returndata += JsonConvert.SerializeObject(UserDetail.ProfileID) + "}";
                            //return returndata += JsonConvert.SerializeObject(RegUser) + "}";
                        }
                    }
                    else
                    {
                     //   string tempreturn = "";
                     //   return tempreturn += JsonConvert.SerializeObject("Profile Doesn't Exist");
                        return returndata += "{\"Message\":\"Error\", \"Alert\": \"Profile Doesn't Exist\" }}";
                    }

                case SignInStatus.LockedOut:
                    return returndata += "{\"Message\":\"Error\", \"Alert\": \"Account Lockedout...!\" }}";
                   // return JsonConvert.SerializeObject("Account Lockedout...!");

                  //  return ";

                case SignInStatus.Failure:
                    //return "Invalid Login";
                    return returndata += "{\"Message\":\"Error\", \"Alert\": \"Signin Failure, Please verify credentials\" }}";
                 //   return JsonConvert.SerializeObject("status:Error, msg:Signin Failure, Please check credentials..!");
                   // return "Signin Failure..! Please Enter Valid Credentials";
                default: break;
            }
            return null;
        }

        //------------------------------------Login End-------------------------------------------------

        //------------------------------------ChangePassword Begin-------------------------------------------------

        [WebMethod]
        public string ChangePassword(int ProfileID, string OldPassword, string NewPassword)
        {//emaild, oldpassword, newpassword
            string returndata = "{\"ChangePassword\":";

            //if (NewPassword.Equals(ConfirmPassword))
            //{
            var userManager = Context.GetOwinContext().GetUserManager<ApplicationUserManager>();
            var signinManager = Context.GetOwinContext().GetUserManager<ApplicationSignInManager>();

            string getEmailID = dbcontext.Profiles.Where(p => p.ProfileID == ProfileID).Select(e => e.UserName).FirstOrDefault();
            string hardcodeUserID = dbcontext.AspNetUsers.Where(e => e.UserName == getEmailID).Select(i => i.Id).FirstOrDefault();

            //var result = userManager.ChangePassword(User.Identity.GetUserId(), OldPassword, NewPassword);
            var result = userManager.ChangePassword(hardcodeUserID, OldPassword, NewPassword);
            if (result.Succeeded)
            {
                var user = userManager.FindById(User.Identity.GetUserId());
                if (user != null)
                {
                    signinManager.SignIn(user, isPersistent: false, rememberBrowser: false);
                }
                string alert = "Password changed sucessfully";
                returndata += JsonConvert.SerializeObject(alert);
            }
            else
            {
                string alert = "Password Mismatch ! Please Try Again";
                returndata += JsonConvert.SerializeObject(alert);
            }
            //}
            //else
            //{
            //    string alert = "New Password and Confirm Password aren't matching";
            //    returndata += JsonConvert.SerializeObject(alert);
            //}           
            return returndata += "}";
        }



        //------------------------------------ChangePassword End-------------------------------------------------

        //------------------------------------Profile Creation Begin-----------------------------------------------

        [WebMethod]
        //public string CreateProfile(string RegisteredEmailID, HttpPostedFileBase FileUpload, string FirstName, string LastName, string dob, string Qualification, string About, DateTime DOB, string Aim, string AspiringExams, string Address, string MobileNo, int SubscribedPlan, string SubscribedTest)
        public string CreateProfile(string RegisteredEmailID, HttpPostedFileBase FileUpload, string FirstName, string LastName, /*string dob, string Qualification, string About, DateTime DOB, string Aim, string AspiringExams, string Address,*/ string MobileNo, int SubscribedPlan, string SubscribedTest)
        {
            string returndata = "{\"Profile\":";
            bool UserFound = new UserExistence(User.Identity.Name).isExist;
            if (!UserFound)
            {
                string alert = "Profile already Exists! Instead Try Updating";
                returndata += JsonConvert.SerializeObject(alert);
            }
            else
            {
                //to check if the user is registered
                //bool UserRegistered = dbcontext.AspNetUsers.Where(user => user.Email == RegisteredEmailID).Any();
                //if (!UserRegistered)
                //{

                //var date = DateTime.Parse(dob);

                string filesnames = "default.jpg";
                if (FileUpload != null)
                {
                    if (!System.IO.Directory.Exists(Server.MapPath("\\Content\\Uploads\\Profile" + 1)))
                    {
                        string savedFileName = DateTime.Now.ToString("MMddHHmmssfff") + Path.GetFileName(FileUpload.FileName);
                        FileUpload.SaveAs(Path.Combine(Server.MapPath("~/Content/Uploads/Profile"), savedFileName));
                        filesnames = savedFileName;
                    }
                }

                Profile pfdata = new Profile()
                {
                    Image = filesnames,
                    FirstName = FirstName,
                    LastName = LastName,
                    UserName = User.Identity.Name,
                    //Qualification = Qualification,
                    //About = About,
                    //DOB = date,
                    //Aim = Aim,
                    //AspiringExams = AspiringExams,
                    //Address = Address,
                    MobileNo = MobileNo,
                    SubscribedPlan = SubscribedPlan,
                    SubscribedTest = SubscribedTest,
                    isActive = true,
                    CreatedDate = DateTime.Now,
                };
                dbcontext.Profiles.Add(pfdata);
                dbcontext.SaveChanges();
                //}
                string alert = "Profile Created Sucessfully";//Redirect to "HomePage or ViewProfile"
                returndata += JsonConvert.SerializeObject(alert);
            }
            return returndata += "}";
        }

        //------------------------------------Profile Creation End-------------------------------------------------

        //------------------------------------Profile View Begin-----------------------------------------------
        /*
        [WebMethod]
        public string ProfileView(string EmailID)
        {
            ShreeyaBlogEntities dbcontext = new ShreeyaBlogEntities();

            string returndata = "{\"Profile\":";
            bool UserFound = new UserExistence(EmailID).isExist;
            if (!UserFound)
            {
                string alert = "Profile doesn't Exists! Create Profile";//Redirect to "CreateProfile"
                returndata += JsonConvert.SerializeObject(alert);
            }
            else
            {
                //var RegUser = dbcontext.Profiles.Where(u => u.UserName == User.Identity.Name.ToString()).FirstOrDefault();
                var RegUser = (from profile in dbcontext.Profiles
                               join subcription in dbcontext.Subscriptions on profile.SubscribedPlan equals subcription.SubscriptionID
                               where profile.UserName == EmailID
                               select new UserProfile
                               {
                                   ProfileID = profile.ProfileID,
                                   Image = profile.Image,
                                   FirstName = profile.FirstName,
                                   LastName = profile.LastName,
                                   UserName = profile.UserName,
                                   //Qualification = profile.Qualification,
                                   //About = profile.About,
                                   //DOB = profile.DOB,
                                   //Aim = profile.Aim,
                                   //AspiringExams = profile.AspiringExams,
                                   //Address = profile.Address,
                                   MobileNo = profile.MobileNo,
                                   //SubscribedPlan = subcription.Name,
                                   SubscribedTest = profile.SubscribedTest,
                                   isActive = profile.isActive,
                                   CreatedDate = profile.CreatedDate
                               }).ToList();

                returndata += JsonConvert.SerializeObject(RegUser);
            }
            return returndata += "}";
        }
        */

        //------------------------------------Profile View End-------------------------------------------------

        [WebMethod]
        public string ProfileView(string EmailID)
        {
            ShreeyaBlogEntities dbcontext = new ShreeyaBlogEntities();

            string returndata = "{\"Profile\":";
            bool UserFound = new UserExistence(EmailID).isExist;
            if (!UserFound)
            {
                string alert = "Profile doesn't Exists! Create Profile";//Redirect to "CreateProfile"
                returndata += JsonConvert.SerializeObject(alert);
            }
            else
            {
                //var RegUser = dbcontext.Profiles.Where(u => u.UserName == User.Identity.Name.ToString()).FirstOrDefault();
                var RegUser = (from profile in dbcontext.Profiles
                               join subcription in dbcontext.Subscriptions on profile.SubscribedPlan equals subcription.SubscriptionID
                               where profile.UserName == EmailID
                               select new UserProfile
                               {
                                   ProfileID = profile.ProfileID,
                                   Image = profile.Image,
                                   FirstName = profile.FirstName,
                                   LastName = profile.LastName,
                                   UserName = profile.UserName,
                                   MobileNo = profile.MobileNo,
                                   SubscribedTest = profile.SubscribedTest,
                                   isActive = profile.isActive,
                                   CreatedDate = profile.CreatedDate
                               })
                               .AsEnumerable()
                               .Select(p => new UserProfile
                               {
                                   ProfileID = p.ProfileID,
                                   Image = p.Image,
                                   FirstName = p.FirstName,
                                   LastName = p.LastName,
                                   UserName = p.UserName,
                                   MobileNo = p.MobileNo,
                                   SubscribedTest = p.SubscribedTest,
                                   isActive = p.isActive,
                                   dateString = p.CreatedDate.Value.ToString("dd-MM-yyyy")
                               }).ToList();

                returndata += JsonConvert.SerializeObject(RegUser);
            }
            return returndata += "}";
        }

        private int GetProfileIDs(string Username)
        {
            int _pid = dbcontext.Profiles.Where(email => email.UserName == Username).Select(id => id.ProfileID).FirstOrDefault();
          //  this._profileID = _pid;
            return _pid;
        }

        //------------------------------------Profile Update Begin-----------------------------------------------
        [WebMethod]
        public string ProfileUpdate(string RegisteredEmailID, string FileUpload, string FirstName, string LastName, string MobileNo, /*int SubscribedPlan,*/ string SubscribedTest)
        {//User cannot Update the plan, it has to be done by admin end
            ShreeyaBlogEntities dbcontext = new ShreeyaBlogEntities();

            string returndata = "{\"Profile\":";
            bool UserFound = new UserExistence(RegisteredEmailID).isExist;
            if (!UserFound)
            {

                return returndata += "{\"Message\":\"Error\", \"Alert\": \"Profile doesn't Exists! Create Profile\" }}";

             //   string alert = "Profile doesn't Exists! Create Profile";//Redirect to "CreateProfile"
             //   returndata += JsonConvert.SerializeObject(alert);
            }
            else
            {
                //to check if the user is registered
                bool UserRegistered = dbcontext.AspNetUsers.Where(user => user.Email == RegisteredEmailID).Any();
                if (UserRegistered)
                {
                    int ProfileID = GetProfileIDs(RegisteredEmailID);

                    //var date = DateTime.Parse(dob);

                    FileUpload = FileUpload == null ? "default.jpg" : FileUpload;

                    Profile profEdit = dbcontext.Profiles.Find(ProfileID);
                    if (profEdit.ProfileID == ProfileID)
                    {
                        profEdit.Image = FileUpload;
                        profEdit.FirstName = FirstName;
                        profEdit.LastName = LastName;
                        profEdit.MobileNo = MobileNo;
                        //profEdit.Qualification = Qualification;
                        //profEdit.About = About;
                        //profEdit.Aim = Aim;
                        //profEdit.MobileNo = MobileNo;
                        //profEdit.AspiringExams = AspiringExams;
                        //profEdit.Address = Address;
                        //profEdit.SubscribedPlan = SubscribedPlan;
                        profEdit.SubscribedTest = SubscribedTest;
                        profEdit.ModifiedDate = DateTime.Now;
                    };
                    dbcontext.Profiles.AddOrUpdate(profEdit);
                    dbcontext.SaveChanges();
                }

                return returndata += "{\"Message\":\"Success\", \"Alert\": \"Profile Updated Sucessfully\" }}";

             //   string alert = "Profile Updated Sucessfully";//Redirect to "View Profile"
              //  returndata += JsonConvert.SerializeObject(alert);
            }
          //  return returndata += "}";
        }

        //------------------------------------Profile Update End-------------------------------------------------


        /// <summary>
        /// ----------------------------------Basic Details Begin-------------------------------------------
        /// </summary>
        /// <returns></returns>





        /// <summary>
        ///  -----------------------------------------------Tabs Begin-----------------
        /// </summary>
        /// <returns></returns>

        [WebMethod]
        public string Tab()
        {
            ShreeyaBlogEntities dbcontext = new ShreeyaBlogEntities();

            String returnData = "{\"TabMaster\":[";

            var headerData = dbcontext.TabMasters.Where(c => c.TabID != 0).OrderBy(o => o.OrderID).ToList();
            var last = headerData.Last();
            foreach (var headloop in headerData)
            {
                int ID = headloop.TabID;
                string Name = headloop.TabName.ToString();
                string IconImage = headloop.IconImage.ToString();
                string SubTab = headloop.SubTab.ToString();

                string SubTabreturnData = "SubMenu: []";
                SubTabreturnData = SubTabMaster(ID);

                returnData += "{\"TabID\":\"" + ID + "\"," +
                                "\"TabName\":\"" + Name + "\"," +
                                 "\"SubExists\":\"" + SubTab + "\"," +
                                  "\"IconImage\":\"" + IconImage + "\",";

                returnData += SubTabreturnData;

                if (headloop.Equals(last))
                    returnData += "}";
                else
                    returnData += "},";
            }
            return returnData + "]}";
        }

        [WebMethod]
        public string SubTabMaster(int TabID)
        {
            ShreeyaBlogEntities dbcontext = new ShreeyaBlogEntities();
            String returnData = "\"SubTabMaster\":[";

            switch (TabID)
            {
                case 1:

                    var headerData = dbcontext.StudyMaterials.Where(c => c.isActive == true && c.ParentID == null).OrderBy(n => n.StudyMaterialName).ToList();
                    var last = headerData.Last();
                    foreach (var headloop in headerData)
                    {
                        int SubTabID = headloop.StudyID;
                        string categoryName = headloop.StudyMaterialName.ToString();
                        bool NestedTab = dbcontext.StudyMaterials.Where(p => p.ParentID == SubTabID).Any();
                        string IconImage = headloop.IconImage.ToString();

                        string SubTabreturnData = "SubMenu: []";
                        SubTabreturnData = NestedTabMaster(SubTabID, TabID);

                        returnData += "{\"TabID\":\"" + SubTabID + "\"," +
                                        "\"TabName\":\"" + categoryName + "\"," +
                                         "\"NestedTab\":\"" + NestedTab + "\"," +
                                          "\"IconImage\":\"" + IconImage + "\",";

                        returnData += SubTabreturnData;

                        if (headloop.Equals(last))
                            returnData += "}";
                        else
                            returnData += "},";
                    }
                    return returnData + "]";

                case 2:
                    var headerData2 = dbcontext.ScienceTechnologies.Where(c => c.Active == true && c.ParentID == null).OrderBy(n => n.Name).ToList();
                    var last2 = headerData2.Last();
                    foreach (var headloop in headerData2)
                    {
                        int categoryId = headloop.ID;
                        string categoryName = headloop.Name.ToString();
                        string IconImage = headloop.IconImage.ToString();

                        returnData += "{\"TabID\":\"" + categoryId
                                    + "\",\"TabName\":\"" + categoryName
                                    + "\",\"IconImage\":\"" + IconImage;

                        if (headloop.Equals(last2))
                            returnData += "\"}";
                        else
                            returnData += "\"},";
                    }
                    return returnData + "]";

                case 3:
                    var headerData3 = dbcontext.Categories.Where(c => c.isActive == true && c.ParentID == null).OrderBy(n => n.Oredr).ToList();
                    var last3 = headerData3.Last();
                    foreach (var headloop in headerData3)
                    {
                        int categoryId = headloop.CategoryId;
                        string categoryName = headloop.CategoryName.ToString();
                        string IconImage = headloop.IconImage.ToString();

                        returnData += "{\"TabID\":\"" + categoryId
                                    + "\",\"TabName\":\"" + categoryName
                                    + "\",\"IconImage\":\"" + IconImage;

                        if (headloop.Equals(last3))
                            returnData += "\"}";
                        else
                            returnData += "\"},";
                    }
                    return returnData + "]";

                case 4:
                    var headerData4 = dbcontext.Talents.Where(c => c.isActive == true && c.ParentID == null).OrderBy(n => n.Name).ToList();
                    var last4 = headerData4.Last();
                    foreach (var headloop in headerData4)
                    {
                        int categoryId = headloop.ID;
                        string categoryName = headloop.Name.ToString();
                        string IconImage = headloop.IconImage.ToString();

                        returnData += "{\"TabID\":\"" + categoryId
                                    + "\",\"TabName\":\"" + categoryName
                                    + "\",\"IconImage\":\"" + IconImage;

                        if (headloop.Equals(last4))
                            returnData += "\"}";
                        else
                            returnData += "\"},";
                    }
                    return returnData + "]";

                case 5:
                    var headerData5 = dbcontext.Learns.Where(c => c.Active == true && c.ParentID == null).OrderBy(n => n.Name).ToList();
                    var last5 = headerData5.Last();
                    foreach (var headloop in headerData5)
                    {
                        int categoryId = headloop.ID;
                        string categoryName = headloop.Name.ToString();
                        //  string IconImage = headloop.IconImage.ToString();
                        string IconImage = "test.png";

                        returnData += "{\"TabID\":\"" + categoryId
                                    + "\",\"TabName\":\"" + categoryName
                                    + "\",\"IconImage\":\"" + IconImage;

                        if (headloop.Equals(last5))
                            returnData += "\"}";
                        else
                            returnData += "\"},";
                    }
                    return returnData + "]";

                case 6:
                case 7:
                case 8:
                case 9:
                case 10:
                case 11:
                case 13:
                    return returnData + "]";

                default: break;
            }
            return null;
        }

        [WebMethod]
        public string NestedTabMaster(int SubTabID, int TabID)
        {
            ShreeyaBlogEntities dbcontext = new ShreeyaBlogEntities();
            String returnData = "\"NestedTabMaster\":[";

            switch (TabID)
            {
                case 1:
                    var headerData1 = dbcontext.StudyMaterials.Where(c => c.isActive == true && c.ParentID == SubTabID).OrderBy(n => n.StudyMaterialName).ToList();
                    if (headerData1.Count != 0)
                    {
                        var last1 = headerData1.Last();
                        foreach (var headloop in headerData1)
                        {
                            int ID = headloop.StudyID;
                            string categoryName = headloop.StudyMaterialName.ToString();
                            string IconImage = headloop.IconImage.ToString();

                            //string LoopSubTabreturnData = "LoopSubMenu: []";
                            //LoopSubTabreturnData = Loop(SubTabID);

                            returnData += "{\"TabID\":\"" + ID
                                    + "\",\"TabName\":\"" + categoryName
                                    + "\",\"IconImage\":\"" + IconImage;

                            //+ "\",\"IconImage\":\"" + IconImage + "\",";
                            //returnData += LoopSubTabreturnData;

                            if (headloop.Equals(last1))
                                returnData += "\"}";
                            else
                                returnData += "\"},";
                        }
                    }
                    return returnData + "]";
            }
            return null;
        }

        /// <summary>
        ///  -----------------------------------------------Tabs End-----------------
        /// </summary>
        /// <returns></returns>



        /// <summary>
        ///  -----------------------------------------------Home Begin-----------------
        /// </summary>
        /// <returns></returns>

        [WebMethod]
        public string DashBoard()
        {
            ShreeyaBlogEntities dbcontext = new ShreeyaBlogEntities();

            String returndata = "{\"DashBoard\":[";

            var Editorial = (from _notes in dbcontext.Editorials
                             where _notes.isDeleted != true
                             select new PostCategory
                             {
                                 PostId = _notes.EditorialID,
                                 Title = _notes.Title,
                                 Description = _notes.Description,
                                 DefaultImage = _notes.EditorialImage,
                                 Likes = _notes.Likes,
                                 CreatedDate = _notes.CreatedDate
                             }).OrderByDescending(d => d.CreatedDate).Take(2).ToList();
            returndata += "{\"Editorial\":" + JsonConvert.SerializeObject(Editorial, Formatting.Indented) + ",";


            var PersonalityDevelopment = (from _notes in dbcontext.PersonalityDevlopments
                                          where _notes.Deleted != true
                                          select new PostCategory
                                          {
                                              PostId = _notes.PDID,
                                              Title = _notes.Title,
                                              Description = _notes.Description,
                                              DefaultImage = _notes.DefaultImage,
                                              Likes = _notes.Likes,
                                              CreatedDate = _notes.CreatedDate
                                          }).OrderByDescending(d => d.CreatedDate).FirstOrDefault();
            returndata += "\"PersonalityDevelopment\":" + JsonConvert.SerializeObject(PersonalityDevelopment, Formatting.Indented) + ",";

            var Notification = (from _notes in dbcontext.Notifications
                                where _notes.Deleted != true
                                select new PostCategory
                                {
                                    PostId = _notes.ID,
                                    Title = _notes.Title,
                                    Description = _notes.Description,
                                    Attachment = _notes.Attachment,
                                    Likes = _notes.Likes,
                                    CreatedDate = _notes.CreatedDate
                                }).OrderByDescending(d => d.CreatedDate).FirstOrDefault();
            returndata += "\"NewNotification\":" + JsonConvert.SerializeObject(Notification, Formatting.Indented) + ",";


            var Categories = (from d in dbcontext.Categories where d.isActive != false select d).Take(4).ToList();
            List<PostCategory> polist = new List<PostCategory>();
            foreach (var cat in Categories)
            {
                var Latest = (from postData in dbcontext.Posts
                              where postData.CategoryId == cat.CategoryId && postData.IsDeleted == false
                              orderby postData.PublishedDate descending
                              select new PostCategory
                              {
                                  PostId = postData.PostId,
                                  CategoryId = postData.CategoryId,
                                  CategoryName = cat.CategoryName,
                                  Title = postData.Title,
                                  Slug = postData.Slug,
                                  Description = postData.Description,
                                  DefaultImage = postData.DefaultImage,
                                  IsPublished = postData.IsPublished,
                                  PublishedDate = postData.PublishedDate,
                                  ModifyDate = postData.ModifyDate,

                              }).FirstOrDefault();
                if (Latest != null)
                {
                    polist.Add(Latest);
                }
            }
            List<PostCategory> p2 = polist;

            return returndata += "\"Latest\":" + JsonConvert.SerializeObject(polist, Formatting.Indented) + "}]}"; ;
        }

        /// <summary>
        ///  -------------------------------------New Begin-----------------
        /// </summary>
        /// <returns></returns>


        //Latest - ip: TabID, SubMenuID, o/p: PostID
        [WebMethod]
        public string Latest(int TabID, int PageID)
        {
            int pageSize = 10;
            ShreeyaBlogEntities dbcontext = new ShreeyaBlogEntities();

            String returnData = "{\"Latest\":";

            switch (TabID)
            {
                case 1:
                    //Study Materials
                    var Latest1 = (from _notes in dbcontext.StudyMaterialPosts
                                   join category in dbcontext.StudyMaterials on _notes.StudyID equals category.StudyID
                                   //where _notes.StudyID == SubTabID
                                   select new PostCategory
                                   {

                                       PostId = _notes.StudyMaterialID,
                                       CategoryId = (int)_notes.StudyID,
                                       Title = _notes.Title,
                                       // Description = _notes.Description,
                                       DefaultImage = _notes.DefaultImage,
                                       CreatedDate = _notes.CreatedDate
                                   }).OrderByDescending(d => d.CreatedDate).ToList();

                    // Latest1.Skip((PageID - 1) * pageSize).Take(pageSize);
                    return returnData += JsonConvert.SerializeObject(Latest1.Skip((PageID - 1) * pageSize).Take(pageSize)) + ", \"RecordsCount\":" + Latest1.Count() + "}";

                case 2:
                    //Science & Technology
                    var Latest2 = (from _notes in dbcontext.STPosts
                                   join category in dbcontext.ScienceTechnologies on _notes.SubID equals category.ID
                                   //where _notes.SubID == SubTabID
                                   select new PostCategory
                                   {
                                       PostId = _notes.ID,
                                       CategoryId = (int)_notes.SubID,
                                       Title = _notes.Title,
                                       //    Description = _notes.Description,
                                       DefaultImage = _notes.DefaultImage,
                                       CreatedDate = _notes.CreatedDate
                                   }).OrderByDescending(d => d.CreatedDate).ToList();
                    return returnData += JsonConvert.SerializeObject(Latest2.Skip((PageID - 1) * pageSize).Take(pageSize)) + ", \"RecordsCount\":" + Latest2.Count() + "}";

                case 3:
                    //Current Affairs
                    var Latest3 = (from postdata in dbcontext.Posts
                                   join catdata in dbcontext.Categories on postdata.CategoryId equals catdata.CategoryId
                                   where postdata.IsDeleted != true /*&& catdata.CategoryId == SubTabID*/
                                   select new PostCategory
                                   {

                                       PostId = postdata.PostId,
                                       CategoryId = postdata.CategoryId,
                                       CategoryName = catdata.CategoryName,
                                       Title = postdata.Title,
                                       Slug = postdata.Slug,
                                       //  Description = postdata.Description,
                                       DefaultImage = postdata.DefaultImage,
                                       IsPublished = postdata.IsPublished,
                                       PublishedDate = postdata.PublishedDate,
                                       ModifyDate = postdata.ModifyDate,
                                   }).OrderByDescending(x => x.PublishedDate).ToList<PostCategory>();


                    return returnData += JsonConvert.SerializeObject(Latest3.Skip((PageID - 1) * pageSize).Take(pageSize)) + ", \"RecordsCount\":" + Latest3.Count() + "}";

                case 4:
                    //TalentPlatform
                    var Latest4 = (from _notes in dbcontext.TalentPosts
                                   join category in dbcontext.Talents on _notes.SubID equals category.ID
                                   //where _notes.SubID == SubTabID
                                   select new PostCategory
                                   {
                                       PostId = _notes.ID,
                                       CategoryId = (int)_notes.SubID,
                                       Title = _notes.Title,
                                       //    Description = _notes.Description,
                                       DefaultImage = _notes.DefaultImage,
                                       CreatedDate = _notes.CreatedDate
                                   }).OrderByDescending(d => d.CreatedDate).ToList();
                    return returnData += JsonConvert.SerializeObject(Latest4.Skip((PageID - 1) * pageSize).Take(pageSize)) + ", \"RecordsCount\":" + Latest4.Count() + "}";

                case 5:
                    //Learn More
                    var Latest5 = (from posts in dbcontext.LearnPosts
                                   join category in dbcontext.Learns on posts.SubID equals category.ID
                                   where posts.Deleted != true /*&& posts.SubID == SubTabID*/
                                   select new PostCategory
                                   {
                                       PostId = posts.ID,
                                       CategoryId = posts.SubID,
                                       CategoryName = category.Name,
                                       Title = posts.Title,
                                       // Description = posts.Description,
                                       DefaultImage = posts.DefaultImage,
                                       IsPublished = (bool)posts.Published,
                                       PublishedDate = posts.CreatedDate,
                                       ModifyDate = posts.ModifiedDate,
                                   }).OrderByDescending(x => x.PublishedDate).ToList<PostCategory>();
                    return returnData += JsonConvert.SerializeObject(Latest5.Skip((PageID - 1) * pageSize).Take(pageSize)) + ", \"RecordsCount\":" + Latest5.Count() + "}";

                case 6:
                    //Person Personality
                    var Latest6 = (from _person in dbcontext.Person_Personality
                                   where _person.Published == true
                                   select new PostCategory
                                   {
                                       PostId = _person.ID,
                                       Title = _person.Title,
                                       DefaultImage = _person.DefaultImage,
                                       //  Description = _person.Description,
                                       IsPublished = (bool)_person.Published,
                                       CreatedDate = _person.CreatedDate
                                   }).OrderByDescending(c => c.CreatedDate).ToList();
                    return returnData += JsonConvert.SerializeObject(Latest6.Skip((PageID - 1) * pageSize).Take(pageSize)) + ", \"RecordsCount\":" + Latest6.Count() + "}";


                case 7:
                    //Editorial
                    var Latest7 = (from _person in dbcontext.Editorials
                                   where _person.isPublished == true
                                   select new PostCategory
                                   {
                                       PostId = _person.EditorialID,
                                       Title = _person.Title,
                                       DefaultImage = _person.EditorialImage,
                                       //  Description = _person.Description,
                                       IsPublished = (bool)_person.isPublished,
                                       CreatedDate = _person.CreatedDate
                                   }).OrderByDescending(c => c.CreatedDate).ToList();
                    return returnData += JsonConvert.SerializeObject(Latest7.Skip((PageID - 1) * pageSize).Take(pageSize)) + ", \"RecordsCount\":" + Latest7.Count() + "}";

                case 8:
                    //Personality Devlopment
                    var Latest8 = (from _person in dbcontext.PersonalityDevlopments
                                   where _person.Published == true
                                   select new PostCategory
                                   {
                                       PostId = _person.PDID,
                                       Title = _person.Title,
                                       DefaultImage = _person.DefaultImage,
                                       // Description = _person.Description,
                                       IsPublished = (bool)_person.Published,
                                       CreatedDate = _person.CreatedDate
                                   }).OrderByDescending(c => c.CreatedDate).ToList();
                    return returnData += JsonConvert.SerializeObject(Latest8.Skip((PageID - 1) * pageSize).Take(pageSize)) + ", \"RecordsCount\":" + Latest8.Count() + "}";

                case 9:
                    //New Notifications
                    var Latest9 = (from _person in dbcontext.Notifications
                                   where _person.Published == true
                                   select new PostCategory
                                   {
                                       PostId = _person.ID,
                                       Title = _person.Title,
                                       Attachment = _person.Attachment,
                                       //  Description = _person.Description,
                                       IsPublished = (bool)_person.Published,
                                       CreatedDate = _person.CreatedDate
                                   }).OrderByDescending(c => c.CreatedDate).ToList();
                    return returnData += JsonConvert.SerializeObject(Latest9.Skip((PageID - 1) * pageSize).Take(pageSize)) + ", \"RecordsCount\":" + Latest9.Count() + "}";

                default: break;
            }

            return null;
        }


        //Category - TabID, SubMenuID - List Category
        [WebMethod]
        public string Category(int TabID, int SubTabID)
        {
            int RecordperPage = 10;
            String returnData = "{\"Category\":";

            switch (TabID)
            {
                case 1:
                    //Study Materials
                    var Category1 = (from _notes in dbcontext.StudyMaterialPosts
                                     join category in dbcontext.StudyMaterials on _notes.StudyID equals category.StudyID
                                     where _notes.StudyID == SubTabID
                                     select new PostCategory
                                     {
                                         PostId = _notes.StudyMaterialID,
                                         CategoryId = (int)_notes.StudyID,
                                         Title = _notes.Title,
                                         Description = _notes.Description,
                                         DefaultImage = _notes.DefaultImage,
                                         CreatedDate = _notes.CreatedDate
                                     }).OrderByDescending(d => d.CreatedDate).ToList();
                    return returnData += JsonConvert.SerializeObject(Category1) + "}";

                case 2:
                    //Science & Technology
                    var Category2 = (from _notes in dbcontext.STPosts
                                     join category in dbcontext.ScienceTechnologies on _notes.SubID equals category.ID
                                     where _notes.SubID == SubTabID
                                     select new PostCategory
                                     {
                                         PostId = _notes.ID,
                                         CategoryId = (int)_notes.SubID,
                                         Title = _notes.Title,
                                         Description = _notes.Description,
                                         DefaultImage = _notes.DefaultImage,
                                         CreatedDate = _notes.CreatedDate
                                     }).OrderByDescending(d => d.CreatedDate).ToList();
                    return returnData += JsonConvert.SerializeObject(Category2) + "}";

                case 3:
                    //Current Affairs
                    var Category3 = (from postdata in dbcontext.Posts
                                     join catdata in dbcontext.Categories on postdata.CategoryId equals catdata.CategoryId
                                     where postdata.IsDeleted != true && catdata.CategoryId == SubTabID
                                     select new PostCategory
                                     {
                                         PostId = postdata.PostId,
                                         CategoryId = postdata.CategoryId,
                                         CategoryName = catdata.CategoryName,
                                         Title = postdata.Title,
                                         Slug = postdata.Slug,
                                         Description = postdata.Description,
                                         DefaultImage = postdata.DefaultImage,
                                         IsPublished = postdata.IsPublished,
                                         PublishedDate = postdata.PublishedDate,
                                         ModifyDate = postdata.ModifyDate,
                                     }).OrderByDescending(x => x.PublishedDate).ToList<PostCategory>();

                    return returnData += JsonConvert.SerializeObject(Category3) + "}";

                case 4:
                    //TalentPlatform
                    var Category4 = (from _notes in dbcontext.TalentPosts
                                     join category in dbcontext.Talents on _notes.SubID equals category.ID
                                     where _notes.SubID == SubTabID
                                     select new PostCategory
                                     {
                                         PostId = _notes.ID,
                                         CategoryId = (int)_notes.SubID,
                                         Title = _notes.Title,
                                         Description = _notes.Description,
                                         DefaultImage = _notes.DefaultImage,
                                         CreatedDate = _notes.CreatedDate
                                     }).OrderByDescending(d => d.CreatedDate).ToList();
                    return returnData += JsonConvert.SerializeObject(Category4) + "}";

                case 5:
                    //Learn More
                    var Category5 = (from posts in dbcontext.LearnPosts
                                     join category in dbcontext.Learns on posts.SubID equals category.ID
                                     where posts.Deleted != true && posts.SubID == SubTabID
                                     select new PostCategory
                                     {
                                         PostId = posts.ID,
                                         CategoryId = posts.SubID,
                                         CategoryName = category.Name,
                                         Title = posts.Title,
                                         Description = posts.Description,
                                         DefaultImage = posts.DefaultImage,
                                         IsPublished = (bool)posts.Published,
                                         PublishedDate = posts.CreatedDate,
                                         ModifyDate = posts.ModifiedDate,
                                     }).OrderByDescending(x => x.PublishedDate).ToList<PostCategory>();
                    return returnData += JsonConvert.SerializeObject(Category5) + "}";

                case 6:
                    //Person Personality
                    var Category6 = (from _person in dbcontext.Person_Personality
                                     where _person.Published == true
                                     select new PostCategory
                                     {
                                         PostId = _person.ID,
                                         Title = _person.Title,
                                         DefaultImage = _person.DefaultImage,
                                         Description = _person.Description,
                                         IsPublished = (bool)_person.Published,
                                         CreatedDate = _person.CreatedDate
                                     }).OrderByDescending(c => c.CreatedDate).ToList();
                    return returnData += JsonConvert.SerializeObject(Category6) + "}";


                case 7:
                    //Editorial
                    var Category7 = (from _person in dbcontext.Editorials
                                     where _person.isPublished == true
                                     select new PostCategory
                                     {
                                         PostId = _person.EditorialID,
                                         Title = _person.Title,
                                         DefaultImage = _person.EditorialImage,
                                         Description = _person.Description,
                                         IsPublished = (bool)_person.isPublished,
                                         CreatedDate = _person.CreatedDate
                                     }).OrderByDescending(c => c.CreatedDate).ToList();
                    return returnData += JsonConvert.SerializeObject(Category7) + "}";

                case 8:
                    //Personality Devlopment
                    var Category8 = (from _person in dbcontext.PersonalityDevlopments
                                     where _person.Published == true
                                     select new PostCategory
                                     {
                                         PostId = _person.PDID,
                                         Title = _person.Title,
                                         DefaultImage = _person.DefaultImage,
                                         Description = _person.Description,
                                         IsPublished = (bool)_person.Published,
                                         CreatedDate = _person.CreatedDate
                                     }).OrderByDescending(c => c.CreatedDate).ToList();
                    return returnData += JsonConvert.SerializeObject(Category8) + "}";

                case 9:
                    //New Notifications
                    var Category9 = (from _person in dbcontext.Notifications
                                     where _person.Published == true
                                     select new PostCategory
                                     {
                                         PostId = _person.ID,
                                         Title = _person.Title,
                                         Attachment = _person.Attachment,
                                         Description = _person.Description,
                                         IsPublished = (bool)_person.Published,
                                         CreatedDate = _person.CreatedDate
                                     }).OrderByDescending(c => c.CreatedDate).ToList();
                    return returnData += JsonConvert.SerializeObject(Category9) + "}";

                default: break;
            }
            return null;
        }


        [WebMethod]
        public string SubHeadLines(int SubTabID)
        {
            String returnData = "\"SubHeadlines\":[";

            var SubCategoryID = dbcontext.StudyMaterials.Where(p => p.ParentID == SubTabID).Select(id => id.StudyID).ToList();
            foreach (var IDs in SubCategoryID)
            {
                var last1 = SubCategoryID.Last();
                var Category1 = (from _notes in dbcontext.StudyMaterialPosts
                                 join category in dbcontext.StudyMaterials on _notes.StudyID equals category.StudyID
                                 where _notes.StudyID == IDs
                                 select new TitlesModel
                                 {
                                     PostId = _notes.StudyMaterialID,
                                     CategoryId = (int)_notes.StudyID,
                                     CategoryName = category.StudyMaterialName,
                                     Title = _notes.Title,
                                     CreatedDate = _notes.CreatedDate
                                 }).OrderBy(d => d.CreatedDate).ToList();
                returnData += "{\"Sub\":" + JsonConvert.SerializeObject(Category1) + ",";

                //if (SubCategoryID.Equals(last1))
                //    returnData += "\"}";
                //else
                //    returnData += "\"},";
            }
            return returnData + "}]";
        }


        //Headlines - TabID, SubMenuID, CategoryID
        [WebMethod]
        public string Headlines(int TabID, int SubTabID)
        {
            ShreeyaBlogEntities dbcontext = new ShreeyaBlogEntities();

            String returnData = "{\"Headlines\":";

            switch (TabID)
            {
                case 1:

                    //string SubHeadreturnData = "SubHead: []";
                    //SubHeadreturnData = SubHeadLines(SubTabID);

                    //Study Materials

                    //  var cats = new List<TitlesModel> ();
                    var cats = new List<List<TitlesModel>>();
                    int i = 0;
                    var SubCategoryID = dbcontext.StudyMaterials.Where(p => p.ParentID == SubTabID).Select(id => id.StudyID).ToList();
                    foreach (var IDs in SubCategoryID)
                    {
                        var Category1 = (from _notes in dbcontext.StudyMaterialPosts
                                         join category in dbcontext.StudyMaterials on _notes.StudyID equals category.StudyID
                                         where _notes.StudyID == IDs
                                         select new TitlesModel
                                         {

                                             DefaultImage = _notes.DefaultImage,
                                             Description = _notes.Description,
                                             PostId = _notes.StudyMaterialID,
                                             CategoryId = (int)_notes.StudyID,
                                             CategoryName = category.StudyMaterialName,
                                             Title = _notes.Title,
                                             CreatedDate = _notes.CreatedDate
                                         }).OrderBy(d => d.CreatedDate).ToList();
                        cats.Add(Category1);
                        i++;
                    }
                    returnData += JsonConvert.SerializeObject(cats);

                    return returnData += "}";



                case 2:
                    //Science & Technology

                    var Category2 = (from _notes in dbcontext.STPosts
                                     join category in dbcontext.ScienceTechnologies on _notes.SubID equals category.ID
                                     where _notes.SubID == SubTabID
                                     select new TitlesModel
                                     {
                                         DefaultImage = _notes.DefaultImage,
                                         Description = _notes.Description,

                                         PostId = _notes.ID,
                                         CategoryId = (int)_notes.SubID,
                                         Title = _notes.Title,
                                         CreatedDate = _notes.CreatedDate
                                     }).OrderBy(d => d.CreatedDate).ToList();

                    return returnData += JsonConvert.SerializeObject(Category2) + "}";

                case 3:
                    //current affairs
                    var Category3 = (from postdata in dbcontext.Posts
                                     join catdata in dbcontext.Categories on postdata.CategoryId equals catdata.CategoryId
                                     where postdata.IsDeleted != true && catdata.CategoryId == SubTabID
                                     select new TitlesModel
                                     {
                                         DefaultImage = postdata.DefaultImage,
                                         Description = postdata.Description,

                                         PostId = postdata.PostId,
                                         CategoryId = postdata.CategoryId,
                                         CategoryName = catdata.CategoryName,
                                         Title = postdata.Title,
                                         CreatedDate = postdata.PublishedDate,
                                     }).OrderByDescending(x => x.CreatedDate).ToList();

                    return returnData += JsonConvert.SerializeObject(Category3) + "}";

                case 4:
                    //Talent Platform
                    var Category4 = (from _notes in dbcontext.TalentPosts
                                     join category in dbcontext.Talents on _notes.SubID equals category.ID
                                     where _notes.IsDeleted != true && _notes.SubID == SubTabID
                                     select new TitlesModel
                                     {
                                         DefaultImage = _notes.DefaultImage,
                                         Description = _notes.Description,

                                         PostId = _notes.ID,
                                         CategoryId = (int)_notes.SubID,
                                         CategoryName = category.Name,
                                         Title = _notes.Title,
                                         CreatedDate = _notes.CreatedDate
                                     }).OrderBy(d => d.CreatedDate).ToList();
                    return returnData += JsonConvert.SerializeObject(Category4) + "}";

                case 5:
                    //Learn More
                    var Category5 = (from posts in dbcontext.LearnPosts
                                     join category in dbcontext.Learns on posts.SubID equals category.ID
                                     where posts.Deleted != true && category.ID == SubTabID
                                     select new TitlesModel
                                     {
                                         DefaultImage = posts.DefaultImage,
                                         Description = posts.Description,


                                         PostId = posts.ID,
                                         CategoryId = posts.SubID,
                                         CategoryName = category.Name,
                                         Title = posts.Title,
                                         CreatedDate = posts.CreatedDate,
                                     }).OrderByDescending(x => x.CreatedDate).ToList();
                    return returnData += JsonConvert.SerializeObject(Category5) + "}";


                case 6:
                    //Person Personality
                    var Category6 = (from _notes in dbcontext.Person_Personality
                                     where _notes.Deleted != true
                                     select new TitlesModel
                                     {
                                         DefaultImage = _notes.DefaultImage,
                                         Description = _notes.Description,

                                         PostId = _notes.ID,
                                         Title = _notes.Title,
                                         CreatedDate = _notes.CreatedDate
                                     }).ToList();
                    return returnData += JsonConvert.SerializeObject(Category6) + "}";

                case 7:
                    //Editorials
                    var Category7 = (from _notes in dbcontext.Editorials
                                     where _notes.isDeleted != true
                                     select new TitlesModel
                                     {
                                         DefaultImage = _notes.EditorialImage,
                                         Description = _notes.Description,

                                         PostId = _notes.EditorialID,
                                         Title = _notes.Title,
                                         CreatedDate = _notes.CreatedDate
                                     }).ToList();
                    return returnData += JsonConvert.SerializeObject(Category7) + "}";

                case 8:
                    //Personality Development
                    var Category8 = (from _notes in dbcontext.PersonalityDevlopments
                                     where _notes.Deleted != true
                                     select new TitlesModel
                                     {
                                         DefaultImage = _notes.DefaultImage,
                                         Description = _notes.Description,


                                         PostId = _notes.PDID,
                                         Title = _notes.Title,
                                         CreatedDate = _notes.CreatedDate
                                     }).ToList();
                    return returnData += JsonConvert.SerializeObject(Category8) + "}";


                case 9:
                    //New Notifications
                    var Category9 = (from _notes in dbcontext.Notifications
                                     where _notes.Deleted != true
                                     select new TitlesModel
                                     {
                                         DefaultImage = _notes.Attachment,
                                         Description = _notes.Description,

                                         PostId = _notes.ID,
                                         Title = _notes.Title,
                                         CreatedDate = _notes.CreatedDate
                                     }).ToList();
                    return returnData += JsonConvert.SerializeObject(Category9) + "}";

                default: break;
            }

            return null;
        }



        //[HttpPost]
        //[Route("upload2")]
        //public async Task<string> UploadFiles()
        //{
        //    var ctx = HttpContext.Current;
        //    var root = ctx.Server.MapPath("~/Upload");
        //    //  var root = "http://test.nearmalls.in/Upload/";
        //    var provider = new MultipartFormDataStreamProvider(root);

        //    try
        //    {
        //        await Request.Content.ReadAsMultipartAsync(provider);

        //        foreach (var file in provider.FileData)
        //        {
        //            var name = file.Headers.ContentDisposition.FileName;

        //            name = name.Trim('"');

        //            var localFileName = file.LocalFileName;
        //            var filePath = Path.Combine(root, name);

        //            File.Move(localFileName, filePath);

        //        }
        //    }
        //    catch (Exception Ex)
        //    {
        //        return $"Error: {Ex.Message}";
        //    }
        //    return "File Uploaded";
        //}




        //DetailedPost - TabID, SubMenuID, CategoryID, PostID - Comments Display
        [WebMethod]
        public string DetailedPost(int PostID, int TabID, int ProfileID)
        {
            String returndata = "{\"DetailedPost\":[";

            //bool UserFound = new UserExistence(User.Identity.Name).isExist;
            bool UserFound = new UserExistence(ProfileID).isExist;
            if (!UserFound)
            {
                //Login Required
                //redirect to Login Page
                string alert = "Invalid User! Please Login and Continue";
                return returndata += "{\"Alert\":" + JsonConvert.SerializeObject(alert) + "}";
            }
            else
            {
                string ListingComments = "ListingComments: []";
                string ListingReplies = "ListingReplies: []";

                switch (TabID)
                {
                    //Study Materials
                    case 1:
                        bool isLiked1 = new LikeCommentDetail(TabID, PostID, ProfileID)._isLiked;
                        var Post1 = (from _notes in dbcontext.StudyMaterialPosts
                                     where _notes.StudyMaterialID == PostID
                                     select new PostCategory
                                     {
                                         PostId = _notes.StudyMaterialID,
                                         CategoryId = (int)_notes.StudyID,
                                         Title = _notes.Title,
                                         Description = _notes.Description,
                                         isLiked = isLiked1,
                                         Likes = _notes.Likes,
                                         Comments = _notes.Comments,
                                         DefaultImage = _notes.DefaultImage,
                                         CreatedDate = _notes.CreatedDate
                                     }).ToList();
                        returndata += "{\"PostDetails\":" + JsonConvert.SerializeObject(Post1, Formatting.Indented) + ",";

                        //Comments & Replies
                        ListingComments = ListComment(TabID, PostID);
                        returndata += ListingComments + ",";

                        ListingReplies = ListReply(TabID, PostID);
                        returndata += ListingReplies;

                        return returndata + "}]}";

                    //Science & Technology
                    case 2:
                        bool isLiked2 = new LikeCommentDetail(TabID, PostID, ProfileID)._isLiked;

                        var Post2 = (from _notes in dbcontext.STPosts
                                     where _notes.ID == PostID
                                     select new PostCategory
                                     {
                                         PostId = _notes.ID,
                                         CategoryId = (int)_notes.SubID,
                                         Title = _notes.Title,
                                         Description = _notes.Description,
                                         DefaultImage = _notes.DefaultImage,
                                         Likes = _notes.Likes,
                                         Comments = _notes.Comments,
                                         isLiked = isLiked2,
                                         CreatedDate = _notes.CreatedDate
                                     }).ToList();
                        returndata += "{\"PostDetails\":" + JsonConvert.SerializeObject(Post2, Formatting.Indented) + ",";

                        ListingComments = ListComment(TabID, PostID);
                        returndata += ListingComments + ",";

                        ListingReplies = ListReply(TabID, PostID);
                        returndata += ListingReplies;

                        return returndata + "}]}";

                    //Current Affairs
                    case 3:
                        //get likes count and comment count
                        bool isLiked3 = new LikeCommentDetail(TabID, PostID, ProfileID)._isLiked;

                        var Post3 = (from postdata1 in dbcontext.Posts
                                     join catdata1 in dbcontext.Categories on postdata1.CategoryId equals catdata1.CategoryId
                                     where postdata1.IsDeleted != true && postdata1.PostId == PostID
                                     select new PostCategory
                                     {
                                         PostId = postdata1.PostId,
                                         CategoryId = postdata1.CategoryId,
                                         CategoryName = catdata1.CategoryName,
                                         Title = postdata1.Title,
                                         Slug = postdata1.Slug,
                                         Description = postdata1.Description,
                                         DefaultImage = postdata1.DefaultImage,
                                         Likes = postdata1.Likes,
                                         Comments = postdata1.Comments,
                                         IsPublished = postdata1.IsPublished,
                                         PublishedDate = postdata1.PublishedDate,
                                         ModifyDate = postdata1.ModifyDate,
                                         isLiked = isLiked3,
                                     }).OrderByDescending(x => x.PublishedDate).ToList();
                        returndata += "{\"PostDetails\":" + JsonConvert.SerializeObject(Post3, Formatting.Indented) + ",";

                        ListingComments = ListComment(TabID, PostID);
                        returndata += ListingComments + ",";

                        ListingReplies = ListReply(TabID, PostID);
                        returndata += ListingReplies;

                        return returndata + "}]}";

                    //Talent Platform
                    case 4:
                        //get likes count and comment count
                        bool isLiked4 = new LikeCommentDetail(TabID, PostID, ProfileID)._isLiked;

                        var Post4 = (from _notes in dbcontext.STPosts
                                     where _notes.ID == PostID
                                     select new PostCategory
                                     {
                                         PostId = _notes.ID,
                                         CategoryId = (int)_notes.SubID,
                                         Title = _notes.Title,
                                         Description = _notes.Description,
                                         DefaultImage = _notes.DefaultImage,
                                         Likes = _notes.Likes,
                                         Comments = _notes.Comments,
                                         isLiked = isLiked4,
                                         CreatedDate = _notes.CreatedDate
                                     }).ToList();
                        returndata += "{\"PostDetails\":" + JsonConvert.SerializeObject(Post4, Formatting.Indented) + ",";

                        //Comments & Replies
                        ListingComments = ListComment(TabID, PostID);
                        returndata += ListingComments + ",";

                        ListingReplies = ListReply(TabID, PostID);
                        returndata += ListingReplies;

                        return returndata + "}]}";

                    //Learn More
                    case 5:
                        //get likes count and comment count
                        bool isLiked5 = new LikeCommentDetail(TabID, PostID, ProfileID)._isLiked;
                        var Post5 = (from posts in dbcontext.LearnPosts
                                     join category in dbcontext.Learns on posts.SubID equals category.ID
                                     where posts.Deleted != true && posts.ID == PostID
                                     select new PostCategory
                                     {
                                         PostId = posts.ID,
                                         CategoryId = posts.SubID,
                                         CategoryName = category.Name,
                                         Title = posts.Title,
                                         Description = posts.Description,
                                         DefaultImage = posts.DefaultImage,
                                         isLiked = isLiked5,
                                         Likes = posts.Likes,
                                         Comments = posts.Comments,
                                         IsPublished = (bool)posts.Published,
                                         PublishedDate = posts.CreatedDate,
                                         ModifyDate = posts.ModifiedDate
                                     }).OrderByDescending(x => x.PublishedDate).ToList();
                        returndata += "{\"PostDetails\":" + JsonConvert.SerializeObject(Post5, Formatting.Indented) + ",";

                        //Comments & Replies
                        ListingComments = ListComment(TabID, PostID);
                        returndata += ListingComments + ",";

                        ListingReplies = ListReply(TabID, PostID);
                        returndata += ListingReplies;

                        return returndata + "}]}";

                    //Person-Personality
                    case 6:
                        //get likes count and comment count
                        bool isLiked6 = new LikeCommentDetail(TabID, PostID, ProfileID)._isLiked;
                        var Post6 = (from _notes in dbcontext.Person_Personality
                                     where _notes.ID == PostID
                                     select new PostCategory
                                     {
                                         PostId = _notes.ID,
                                         Title = _notes.Title,
                                         Description = _notes.Description,
                                         DefaultImage = _notes.DefaultImage,
                                         Likes = _notes.Likes,
                                         Comments = _notes.Comments,
                                         isLiked = isLiked6,
                                         CreatedDate = _notes.CreatedDate
                                     }).ToList();
                        returndata += "{\"PostDetails\":" + JsonConvert.SerializeObject(Post6, Formatting.Indented) + ",";

                        //Comments & Replies
                        ListingComments = ListComment(TabID, PostID);
                        returndata += ListingComments + ",";

                        ListingReplies = ListReply(TabID, PostID);
                        returndata += ListingReplies;

                        return returndata + "}]}";

                    //Editorial
                    case 7:
                        //get likes count and comment count
                        bool isLiked7 = new LikeCommentDetail(TabID, PostID, ProfileID)._isLiked;
                        var Post7 = (from _notes in dbcontext.Editorials
                                     where _notes.EditorialID == PostID
                                     select new PostCategory
                                     {
                                         PostId = _notes.EditorialID,
                                         Title = _notes.Title,
                                         Description = _notes.Description,
                                         DefaultImage = _notes.EditorialImage,
                                         Likes = _notes.Likes,
                                         Comments = _notes.Comments,
                                         isLiked = isLiked7,
                                         CreatedDate = _notes.CreatedDate
                                     }).ToList();

                        returndata += "{\"PostDetails\":" + JsonConvert.SerializeObject(Post7, Formatting.Indented) + ",";

                        //Comments & Replies
                        ListingComments = ListComment(TabID, PostID);
                        returndata += ListingComments + ",";

                        ListingReplies = ListReply(TabID, PostID);
                        returndata += ListingReplies;

                        return returndata + "}]}";

                    //Personality Development
                    case 8:
                        //get likes count and comment count
                        bool isLiked8 = new LikeCommentDetail(TabID, PostID, ProfileID)._isLiked;
                        var Post8 = (from _notes in dbcontext.PersonalityDevlopments
                                     where _notes.PDID == PostID
                                     select new PostCategory
                                     {
                                         PostId = _notes.PDID,
                                         Title = _notes.Title,
                                         Description = _notes.Description,
                                         DefaultImage = _notes.DefaultImage,
                                         Likes = _notes.Likes,
                                         Comments = _notes.Comments,
                                         isLiked = isLiked8,
                                         CreatedDate = _notes.CreatedDate
                                     }).ToList();

                        returndata += "{\"PostDetails\":" + JsonConvert.SerializeObject(Post8, Formatting.Indented) + ",";

                        //Comments & Replies
                        ListingComments = ListComment(TabID, PostID);
                        returndata += ListingComments + ",";

                        ListingReplies = ListReply(TabID, PostID);
                        returndata += ListingReplies;

                        return returndata + "}]}";

                    //New Notification
                    case 9:
                        //get likes count and comment count
                        bool isLiked9 = new LikeCommentDetail(TabID, PostID, ProfileID)._isLiked;
                        var Post9 = (from _notes in dbcontext.Notifications
                                     where _notes.ID == PostID
                                     select new PostCategory
                                     {
                                         PostId = _notes.ID,
                                         Title = _notes.Title,
                                         Description = _notes.Description,
                                         Attachment = _notes.Attachment,
                                         Likes = _notes.Likes,
                                         Comments = _notes.Comments,
                                         CreatedDate = _notes.CreatedDate
                                     }).ToList();

                        returndata += "{\"PostDetails\":" + JsonConvert.SerializeObject(Post9, Formatting.Indented) + ",";

                        //Comments & Replies
                        ListingComments = ListComment(TabID, PostID);
                        returndata += ListingComments + ",";

                        ListingReplies = ListReply(TabID, PostID);
                        returndata += ListingReplies;

                        return returndata + "}]}";

                    default: break;
                }
                return null;
            }
        }


        ///  /// <summary>
        ///  -------------------------------------New End-----------------
        /// </summary>
        /// <returns></returns>



        //------------------------------------Contact Details Begin-----------------------------------------------


        [WebMethod]
        public string Contact(string firstname, string lastname, string email, string phone, string message)
        {
            string returndata = "{\"Enquiry\":";

            SmtpClient smtp1;
            string from = "technical@shlrtechnosoft.in";
            string toEmail = "shilpah@shlrtechnosoft.com";

            using (MailMessage mail = new MailMessage())
            {
                mail.From = new MailAddress(from);
                mail.To.Add(toEmail);

                mail.Subject = "SJSMAG Enquiry";

                mail.Body = "Hello " + toEmail + Environment.NewLine +
                    "You Got A New Enquiry..!" + Environment.NewLine + Environment.NewLine +
                    "Name : " + firstname + Environment.NewLine +
                    "Last Name : " + lastname + Environment.NewLine +
                    "Email : " + email + Environment.NewLine +
                    "Phone : " + phone + Environment.NewLine +
                    "Message : " + message + Environment.NewLine;

                mail.IsBodyHtml = false;

                smtp1 = new SmtpClient();
                smtp1.Host = "smtp.gmail.com";
                smtp1.Port = 587;

                smtp1.Credentials = new System.Net.NetworkCredential
                ("technical@shlrtechnosoft.in", "Technical@123");

                smtp1.EnableSsl = true;
                smtp1.Send(mail);
            }

            string alert = "Mail Sent";
            returndata += JsonConvert.SerializeObject(alert);

            return returndata += "}";
        }

        //------------------------------------Contact Details End-----------------------------------------------

        /// <summary>
        ///  -----------------------------------------------Home End-----------------
        /// </summary>
        /// <returns></returns>


        [WebMethod]
        public string ValidateToken(string EmailID, string Token)
        {
            ShreeyaBlogEntities dbcontext = new ShreeyaBlogEntities();
            string returndata = "{\"ValidateToken\":";

            var isexists = (from i in dbcontext.Profiles
                            where i.UserName == EmailID && i.AppToken==Token
                            select new
                            {
                                username = i.UserName
                            }).FirstOrDefault();
            Profile UserDetail = dbcontext.Profiles.Where(u => u.UserName == EmailID && u.AppToken == Token).FirstOrDefault();
            if (isexists !=null)
            {
                returndata += JsonConvert.SerializeObject("Valid");

            }
            else
            {
                returndata += JsonConvert.SerializeObject("Invalid");

            }

            return returndata += "}";
         
        }


        [WebMethod]
        public string ContactInfo(string EmailID)
        {
            ShreeyaBlogEntities dbcontext = new ShreeyaBlogEntities();
            string returndata = "{\"ContactInfo\":";
            {

                var RegUser = (from Contact_Info in dbcontext.Contact_Info
                               select new Contact_info
                               {
                                   Address = Contact_Info.Address,
                                   PhoneNo = Contact_Info.PhoneNo,
                                   Email = Contact_Info.Email,
                                   WebSite = Contact_Info.WebSite
                               }).ToList();

                returndata += JsonConvert.SerializeObject(RegUser);
            }
            return returndata += "}";
        }


        /// <summary>
        /// Like , Comment , Reply -----------------------------------------------Begin---------------
        /// </summary>
        /// <returns></returns>



        [WebMethod]
        public string LikedPost(int ProfileID, int PageID)
        {
            int pageSize = 10;
            ShreeyaBlogEntities dbcontext = new ShreeyaBlogEntities();
            string returndata = "{\"LikedPost\":";

            bool UserFound = new UserExistence(ProfileID).isExist;
            if (!UserFound)
            {
                //Login Required
                //redirect to Login Page
                string alert = "Invalid User! Please Login and Continue";
                return returndata += "{\"Alert\":" + JsonConvert.SerializeObject(alert) + "}";
            }
            else
            {

                var RegUser = (from likedposts in dbcontext.Likes
                               join posts in dbcontext.Posts on likedposts.PostID equals posts.PostId
                               join category in dbcontext.Categories on posts.CategoryId equals category.CategoryId
                               join tabmaster in dbcontext.TabMasters on category.TabID equals tabmaster.TabID
                               where likedposts.ProfileID == ProfileID
                               select new Liked_posts
                               {
                                  
                                   ID = likedposts.PostID,
                                   TabID = likedposts.SectionID,
                                   Category = tabmaster.TabName,
                                   DefaultImage= posts.DefaultImage,
                                   PostTitle = posts.Title,
                                   CategoryID = category.CategoryId,
                                   LikedOn = likedposts.CreatedDate,

                               }).ToList();

                returndata += JsonConvert.SerializeObject(RegUser.Skip((PageID - 1) * pageSize).Take(pageSize)) + ", \"RecordsCount\":" + RegUser.Count();

//                returndata += JsonConvert.SerializeObject(RegUser);

            }
            return returndata += "}";
        }



        [WebMethod]
        public string LikePost(int PostID, int UserID, int isLiked, int TabID)
        {
            ShreeyaBlogEntities dbcontext = new ShreeyaBlogEntities();

            string LikeDetails = ""; int? likeCount = 0;

            //int getUserID = new GetProfileID(User.Identity.Name)._profileID;
            //bool isnull = string.IsNullOrEmpty(User.Identity.Name);
            bool UserFound = new UserExistence(UserID).isExist;
            if (!UserFound)
            {
                //Login Required
                //redirect to Login Page
                string alert = "Invalid User! Please Login and Continue";
                LikeDetails += "{\"LikeDetails\":";
                LikeDetails += JsonConvert.SerializeObject(alert);
                LikeDetails += "}";
                return LikeDetails;
            }
            else
            {
                if (isLiked == 1)
                {
                    CommentAndReply obj = new CommentAndReply();
                    likeCount = obj.addLike(PostID, TabID);

                    Like like = new Like();
                    {
                        like.PostID = PostID;
                        like.ProfileID = UserID;
                        like.Liked = true;
                        like.SectionID = TabID;
                        like.CreatedDate = DateTime.Now;
                        dbcontext.Likes.Add(like);
                        int save = dbcontext.SaveChanges();
                    }
                }
                else
                {
                    CommentAndReply obj = new CommentAndReply();
                    likeCount = obj.unLike(PostID, TabID, (int)UserID);
                }
            }
            LikeDetails += "{\"LikeDetails\":";
            LikeDetails += JsonConvert.SerializeObject(likeCount);
            LikeDetails += "}";
            return LikeDetails;
        }

        [WebMethod]
        public string CommentPost(string Comments, int PostID, int UserID, int TabID)
        {
            ShreeyaBlogEntities dbcontext = new ShreeyaBlogEntities();

            string PostComment = ""; int? CommentsCount = 0;

            bool UserFound = new UserExistence(UserID).isExist;
            if (!UserFound)
            {
                //Login Required
                //redirect to Login Page
                string alert = "Invalid User! Please Login and Continue";
                PostComment += "{\"PostComment\":";
                PostComment += JsonConvert.SerializeObject(alert);
                PostComment += "}";
                return PostComment;
            }
            else
            {
                CommentAndReply obj = new CommentAndReply();
                CommentsCount = obj.addComment(UserID, TabID, PostID, Comments);

                Comment _insert = new Comment();
                {
                    _insert.PostID = PostID;
                    _insert.ProfileID = UserID;
                    _insert.Comments = Comments;
                    _insert.ReplyCount = 0;
                    _insert.SectionID = TabID;
                    _insert.CreatedDate = DateTime.Now;
                }
                dbcontext.Comments.Add(_insert);
                int save = dbcontext.SaveChanges();
            }

            PostComment += "{\"PostComment\":";
            PostComment += JsonConvert.SerializeObject(CommentsCount);
            PostComment += "}";

            return PostComment;
        }

        [WebMethod]
        public string ReplyPost(string Reply, int PostID, int CommentID, int UserID, int TabID)
        {
            ShreeyaBlogEntities dbcontext = new ShreeyaBlogEntities();

            string PostReply = ""; int? ReplyCount = 0;
            bool UserFound = new UserExistence(UserID).isExist;
            if (!UserFound)
            {
                //Login Required
                //redirect to Login Page
                string alert = "Invalid User! Please Login and Continue";
                PostReply += "{\"PostReply\":";
                PostReply += JsonConvert.SerializeObject(alert);
                PostReply += "}";
                return PostReply;
            }
            else
            {
                CommentAndReply obj = new CommentAndReply();
                ReplyCount = obj.addReply(Reply, PostID, CommentID, UserID, TabID);
            }

            PostReply += "{\"PostReply\":";
            PostReply += JsonConvert.SerializeObject(ReplyCount);
            PostReply += "}";

            return PostReply;
        }

        [WebMethod]
        public string ListComment(int TabID, int PostID)
        {
            String returndata = "\"CommentDetails\":[";
            List<CommentDetails> ListComments = obj.getComments(TabID, PostID);
            returndata += JsonConvert.SerializeObject(ListComments, Formatting.Indented);
            return returndata + "]";
        }

        [WebMethod]
        public string ListReply(int TabID, int PostID)
        {
            String returndata = "\"ReplyDetails\":[";
            List<CommentDetails> ListReplies = obj.getReplies(TabID, PostID);
            returndata += JsonConvert.SerializeObject(ListReplies, Formatting.Indented);
            return returndata + "]";
        }


        /// <summary>
        /// Like , Comment , Reply -----------------------------------------------End---------------
        /// </summary>
        /// <returns></returns>

    }
}
