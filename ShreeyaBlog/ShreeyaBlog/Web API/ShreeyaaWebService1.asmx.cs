﻿using System;
using System.Collections.Generic;
using System.Data.Entity.Migrations;
using System.IO;
using System.Linq;
using System.Net.Mail;
using System.Security.Claims;
using System.Web;
using System.Web.Services;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using Newtonsoft.Json;
using ShreeyaBlog.Classes;
using ShreeyaBlog.Models;

namespace ShreeyaBlog.Web_API
{
    /// <summary>
    /// Summary description for ShreeyaaWebService
    /// </summary>
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
    [System.Web.Script.Services.ScriptService]

    public interface ToInclude
    {
        //---------------------------------------------Basic Details Begin----------------------------------------------------------------

        //Register
        void Register(string EmailID, string Password, string ConfirmPassword);

        //Login
        void Login(string EmailID, string Password);

        //change Password 
        void ChangePassword(string EmailID, string OldPassword, string NewPassword);

        //CreateProfile
        void CreateProfile(string RegisteredEmailID, HttpPostedFileBase FileUpload, string FirstName, string LastName, string dob, string Qualification, string About, DateTime DOB, string Aim, string AspiringExams, string Address, string MobileNo, string SubscribedPlan, string SubscribedTest);

        //view profile
        void ProfileView();

        //profile update
        void ProfileUpdate(string RegisteredEmailID, HttpPostedFileBase FileUpload, string FirstName, string LastName, string dob, string Qualification, string About, DateTime DOB, string Aim, string AspiringExams, string Address, string MobileNo, string SubscribedPlan, string SubscribedTest);

        //---------------------------------------------Basic Details End----------------------------------------------------------------




        //---------------------------------------------Tab Details Begin----------------------------------------------------------------

        //Load all categories
        void CurrentAffairTab();

        //Load all categories
        void StudyTab();

        //Load all categories
        void LearnMoreTabs();

        //---------------------------------------------Tab Details End------------------------------------------------------------------





        //---------------------------------------------Home Begin------------------------------------------------------------------

        //Latest wrt categories- get latest post(1) from all the category[CurrentAffair]
        void Latest();

        //Contact
        void Contact(string firstname, string lastname, string email, string phone, string message);

        //InactiveUserForm
        void inactive(); //empty static form

        //---------------------------------------------Home End------------------------------------------------------------------




        //---------------------------------------------Category Begin------------------------------------------------------------------

        //load all/(5 per page) Posts w.r.t Category
        void CurrentAffairCategory(int CategoryID);

        //load Description and Sub Categories
        void StudyCategory(int CategoryID);

        //load all/(5 per page) Posts w.r.t Category
        void LearnMoreCategory(int CategoryID);

        //---------------------------------------------Category End------------------------------------------------------------------




        //---------------------------------------------SubCategory Begin------------------------------------------------------------------

        //load latest(1) Posts and list all Titles wrt SubCategory
        void StudySubCategory(int PostID, int TabID, int userProfileID);

        //---------------------------------------------SubCategory End------------------------------------------------------------------





        //---------------------------------------------Forms Begin------------------------------------------------------------------

        //Load Latest Post(1) from all categories along with CategoryName
        void ScienceTechnology();

        //Load Latest Post(1) from all categories along with CategoryName 
        void TalentPlatform();

        //Load all [10/form] +  list titles of PersonPersonality
        void PersonPersonality();

        ////load latest Post(1) + list titles of Editorial
        void Editorial();

        //load latest Post(1) + list titles of PersonalityDevelopment
        void PersonalityDevelopment();


        //Load all Notification [1 per page] + list recent Notification Title
        void Notification();

        //---------------------------------------------Forms End------------------------------------------------------------------




        //---------------------------------------------Posts Begin------------------------------------------------------------------

        //load post
        void CurrentAffairPost(int PostID);


        //load latest(1) Posts and list all Titles wrt SubCategory
        //load SelectedPost and list all Titles of Selected PostCategory
        void ScienceTechnologyPost(int CategoryID, int PostID, int TabID, int userProfileID);

        //load latest(1) Posts and list all Titles of Selected Category
        void TalentPlatformPost(int CategoryID, int PostID);

        //load Post
        void PersonPersonality(int PostID, int TabID, int userProfileID);

        //Load Latest Post wrt Category and all Title of Category 
        void LearnMorePost(int PostID, int TabID, int userProfileID);

        //load Selected Post + list titles of PersonalityDevelopment
        void Editorial(int PostID, int TabID, int userProfileID);

        //load Selected Post + list titles of PersonalityDevelopment
        void PersonalityDevelopment(int PostID, int TabID, int userProfileID);

        //load Selected Post + list titles of Notification
        void Notification(int PostID);

        //---------------------------------------------Posts End------------------------------------------------------------------




        //---------------------------------------------Like, UnLike, Comment, Reply Begin------------------------------------------------------------------

        void Like(int PostID, int UserID, int TabID);
        void Unlike(int PostID, int UserID, int TabID);
        void Comment(int PostID, int UserID, int TabID, string Comment);
        void Reply(int PostID, int UserID, int TabID, string Reply);
        //share();

        //---------------------------------------------Like, UnLike, Comment, Reply End------------------------------------------------------------------

    }


    public class ShreeyaaWebService : System.Web.Services.WebService
    {
        ShreeyaBlogEntities dbcontext = new ShreeyaBlogEntities();
        DerivedIndex objectindex = new DerivedIndex();

        [WebMethod]
        public string HelloWorld()
        {
            return "Hello World";
        }

        [WebMethod]
        public string Test()
        {
            return User.Identity.Name.ToString();
        }



        /// <summary>
        /// ----------------------------------Basic Details Begin-------------------------------------------
        /// </summary>
        /// <returns></returns>


        //------------------------------------Registration Begin-----------------------------------------------

        [WebMethod]
        public string Register(string EmailID, string Password, string ConfirmPassword)
        {
            string returndata = "{\"Registration\":";

            var userManager = Context.GetOwinContext().GetUserManager<ApplicationUserManager>();
            var user = new ApplicationUser { UserName = EmailID, Email = EmailID };
            var result = userManager.Create(user, Password);
            if (result.Succeeded)
            {
                userManager.AddToRole(user.Id, "User");
                string alert = "Registration Sucessfull";//Redirect to "Login"
                returndata += JsonConvert.SerializeObject(alert);
            }
            else
            {
                string alert = "Something went wrong ! Please Try Again";//Redirect to "Register"
                returndata += JsonConvert.SerializeObject(alert);
            }
            return returndata += "}";
        }

        //------------------------------------Registration End-------------------------------------------------



        //------------------------------------Login Begin-----------------------------------------------

        [WebMethod]
        public string Login(string EmailID, string Password)
        {
            var manager = Context.GetOwinContext().GetUserManager<ApplicationUserManager>();
            var signinManager = Context.GetOwinContext().GetUserManager<ApplicationSignInManager>();
            var userManager = Context.GetOwinContext().GetUserManager<ApplicationUserManager>();

            var result = signinManager.PasswordSignIn(EmailID, Password, false, shouldLockout: false);
            switch (result)
            {
                case SignInStatus.Success:
                    var user = userManager.FindAsync(EmailID, Password);

                    var isexists = (from i in dbcontext.Profiles
                                    where i.UserName == EmailID
                                    select new
                                    {
                                        username = i.UserName
                                    }).FirstOrDefault();

                    string returndata = "{\"Profile\":";
                    if (isexists != null)
                    {
                        var RegUser = (from profile in dbcontext.Profiles
                                       where profile.UserName == EmailID
                                       select new UserProfile
                                       {
                                           ProfileID = profile.ProfileID,
                                           Image = profile.Image,
                                           FirstName = profile.FirstName,
                                           LastName = profile.LastName,
                                           UserName = profile.UserName,
                                           Qualification = profile.Qualification,
                                           About = profile.About,
                                           DOB = profile.DOB,
                                           Aim = profile.Aim,
                                           AspiringExams = profile.AspiringExams,
                                           Address = profile.Address,
                                           MobileNo = profile.MobileNo,
                                           SubscribedPlan = profile.SubscribedPlan,
                                           SubscribedTest = profile.SubscribedTest,
                                           isActive = profile.isActive,
                                           CreatedDate = profile.CreatedDate
                                       }).ToList();
                        return returndata += JsonConvert.SerializeObject(RegUser) + "}";
                        // return "Login Sucessfull " + User.Identity.Name + " Redirect to Home Page";
                    }
                    else
                    {
                        var RegUser = EmailID;
                        return returndata += JsonConvert.SerializeObject(RegUser) + "}";
                    }

                case SignInStatus.LockedOut:
                    return "Lockout";

                case SignInStatus.Failure:
                    return "Invalid Login";
                default: break;
            }
            return null;
        }

        //------------------------------------Login End-------------------------------------------------



        //------------------------------------ChangePassword Begin-------------------------------------------------

        [WebMethod]
        public string ChangePassword(string EmailID, string OldPassword, string NewPassword)
        {
            string returndata = "{\"ChangePassword\":";

            var userManager = Context.GetOwinContext().GetUserManager<ApplicationUserManager>();
            var signinManager = Context.GetOwinContext().GetUserManager<ApplicationSignInManager>();

            var result = userManager.ChangePassword(User.Identity.GetUserId(), OldPassword, NewPassword);
            if (result.Succeeded)
            {
                var user = userManager.FindById(User.Identity.GetUserId());
                if (user != null)
                {
                    signinManager.SignIn(user, isPersistent: false, rememberBrowser: false);
                }
                string alert = "Password changed sucessfully";
                returndata += JsonConvert.SerializeObject(alert);
            }
            else
            {
                string alert = "Password Mismatch ! Please Try Again";
                returndata += JsonConvert.SerializeObject(alert);
            }
            return returndata += "}";
        }

        //------------------------------------ChangePassword End-------------------------------------------------




        //------------------------------------Profile Creation Begin-----------------------------------------------

        [WebMethod]
        public string CreateProfile(string RegisteredEmailID, HttpPostedFileBase FileUpload, string FirstName, string LastName, string dob, string Qualification, string About, DateTime DOB, string Aim, string AspiringExams, string Address, string MobileNo, string SubscribedPlan, string SubscribedTest)
        {
            string returndata = "{\"Profile\":";
            bool UserFound = new UserExistence(User.Identity.Name).isExist;
            if (!UserFound)
            {
                string alert = "Profile already Exists! Instead Try Updating";
                returndata += JsonConvert.SerializeObject(alert);
            }
            else
            {
                //to check if the user is registered
                bool UserRegistered = dbcontext.AspNetUsers.Where(user => user.Email == RegisteredEmailID).Any();
                if (!UserRegistered)
                {
                    var date = DateTime.Parse(dob);

                    string filesnames = "default.jpg";
                    if (FileUpload != null)
                    {
                        if (!System.IO.Directory.Exists(Server.MapPath("\\Content\\Uploads\\Profile" + 1)))
                        {
                            string savedFileName = DateTime.Now.ToString("MMddHHmmssfff") + Path.GetFileName(FileUpload.FileName);
                            FileUpload.SaveAs(Path.Combine(Server.MapPath("~/Content/Uploads/Profile"), savedFileName));
                            filesnames = savedFileName;
                        }
                    }

                    Profile pfdata = new Profile()
                    {
                        Image = filesnames,
                        FirstName = FirstName,
                        LastName = LastName,
                        UserName = User.Identity.Name,
                        Qualification = Qualification,
                        About = About,
                        DOB = date,
                        Aim = Aim,
                        AspiringExams = AspiringExams,
                        Address = Address,
                        MobileNo = MobileNo,
                        SubscribedPlan = SubscribedPlan,
                        SubscribedTest = SubscribedTest,
                        isActive = false,
                        CreatedDate = DateTime.Now,
                    };
                    dbcontext.Profiles.Add(pfdata);
                    dbcontext.SaveChanges();
                }
                string alert = "Profile Created Sucessfully";//Redirect to "HomePage or ViewProfile"
                returndata += JsonConvert.SerializeObject(alert);
            }
            return returndata += "}";
        }

        //------------------------------------Profile Creation End-------------------------------------------------




        //------------------------------------Profile View Begin-----------------------------------------------

        [WebMethod]
        public string ProfileView(string EmailID)
        {
            string returndata = "{\"Profile\":";
            bool UserFound = new UserExistence(EmailID).isExist;
            if (!UserFound)
            {
                string alert = "Profile doesn't Exists! Create Profile";//Redirect to "CreateProfile"
                returndata += JsonConvert.SerializeObject(alert);
            }
            else
            {
                //var RegUser = dbcontext.Profiles.Where(u => u.UserName == User.Identity.Name.ToString()).FirstOrDefault();
                var RegUser = (from profile in dbcontext.Profiles
                               where profile.UserName == EmailID
                               select new UserProfile
                               {
                                   ProfileID = profile.ProfileID,
                                   Image = profile.Image,
                                   FirstName = profile.FirstName,
                                   LastName = profile.LastName,
                                   UserName = profile.UserName,
                                   Qualification = profile.Qualification,
                                   About = profile.About,
                                   DOB = profile.DOB,
                                   Aim = profile.Aim,
                                   AspiringExams = profile.AspiringExams,
                                   Address = profile.Address,
                                   MobileNo = profile.MobileNo,
                                   SubscribedPlan = profile.SubscribedPlan,
                                   SubscribedTest = profile.SubscribedTest,
                                   isActive = profile.isActive,
                                   CreatedDate = profile.CreatedDate
                               }).ToList();

                returndata += JsonConvert.SerializeObject(RegUser);
            }
            return returndata += "}";
        }

        //------------------------------------Profile View End-------------------------------------------------





        //------------------------------------Profile Update Begin-----------------------------------------------
        [WebMethod]
        public string ProfileUpdate(string RegisteredEmailID, HttpPostedFileBase FileUpload, string FirstName, string LastName, string dob, string Qualification, string About, DateTime DOB, string Aim, string AspiringExams, string Address, string MobileNo, string SubscribedPlan, string SubscribedTest)
        {
            string returndata = "{\"Profile\":";
            bool UserFound = new UserExistence(User.Identity.Name).isExist;
            if (!UserFound)
            {
                string alert = "Profile doesn't Exists! Create Profile";//Redirect to "CreateProfile"
                returndata += JsonConvert.SerializeObject(alert);
            }
            else
            {
                //to check if the user is registered
                bool UserRegistered = dbcontext.AspNetUsers.Where(user => user.Email == RegisteredEmailID).Any();
                if (!UserRegistered)
                {
                    var ProfileID = new GetProfileID(User.Identity.Name).ProfileID;

                    var date = DateTime.Parse(dob);

                    string filesnames = "default.jpg";
                    if (FileUpload != null)
                    {
                        if (!System.IO.Directory.Exists(Server.MapPath("\\Content\\Uploads\\Profile" + 1)))
                        {
                            string savedFileName = DateTime.Now.ToString("MMddHHmmssfff") + Path.GetFileName(FileUpload.FileName);
                            FileUpload.SaveAs(Path.Combine(Server.MapPath("~/Content/Uploads/Profile"), savedFileName));
                            filesnames = savedFileName;
                        }
                    }

                    Profile profEdit = dbcontext.Profiles.Find(ProfileID);
                    if (profEdit.ProfileID == ProfileID)
                    {
                        profEdit.Image = filesnames;
                        profEdit.FirstName = FirstName;
                        profEdit.LastName = LastName;
                        profEdit.Qualification = Qualification;
                        profEdit.About = About;
                        profEdit.Aim = Aim;
                        profEdit.MobileNo = MobileNo;
                        profEdit.AspiringExams = AspiringExams;
                        profEdit.Address = Address;
                        profEdit.SubscribedPlan = SubscribedPlan;
                        profEdit.SubscribedTest = SubscribedTest;
                        profEdit.ModifiedDate = DateTime.Now;
                    };
                    dbcontext.Profiles.AddOrUpdate(profEdit);
                    dbcontext.SaveChanges();
                }
                string alert = "Profile Updated Sucessfully";//Redirect to "View Profile"
                returndata += JsonConvert.SerializeObject(alert);
            }
            return returndata += "}";
        }

        //------------------------------------Profile Update End-------------------------------------------------



        /// <summary>
        /// ----------------------------------Basic Details Begin-------------------------------------------
        /// </summary>
        /// <returns></returns>





        /// <summary>
        ///  -----------------------------------------------Tabs Begin-----------------
        /// </summary>
        /// <returns></returns>

        [WebMethod]
        public string TabMaster()
        {
            String returnData = "{\"NavigationMaster\":[";

            var headerData = dbcontext.TabMasters.Where(c => c.TabID != 0).OrderBy(o => o.OrderID).ToList();
            var last = headerData.Last();
            foreach (var headloop in headerData)
            {
                int ID = headloop.TabID;
                string Name = headloop.TabName.ToString();

                returnData += "{\"TabId\":\"" + ID
                                + "\",\"TabName\":\"" + Name;
                if (headloop.Equals(last))
                    returnData += "\"}";
                else
                    returnData += "\"},";
            }
            return returnData + "]}";
        }

        [WebMethod]
        public string SubTabMaster(int TabID)
        {
            String returnData = "{\"SubTabMaster\":[";

            switch (TabID)
            {
                case 3:
                    var headerData1 = dbcontext.Categories.Where(c => c.isActive == true && c.ParentID == null).OrderBy(n => n.CategoryName).ToList();
                    var last1 = headerData1.Last();
                    foreach (var headloop in headerData1)
                    {
                        int categoryId = headloop.CategoryId;
                        string categoryName = headloop.CategoryName.ToString();

                        returnData += "{\"categoryId\":\"" + categoryId
                                        + "\",\"categoryName\":\"" + categoryName;
                        if (headloop.Equals(last1))
                            returnData += "\"}";
                        else
                            returnData += "\"},";
                    }
                    return returnData + "]}";

                case 2:
                    var headerData2 = dbcontext.StudyMaterials.Where(c => c.isActive == true && c.ParentID == null).OrderBy(n => n.StudyMaterialName).ToList();
                    var last2 = headerData2.Last();
                    foreach (var headloop in headerData2)
                    {
                        int categoryId = headloop.StudyID;
                        string categoryName = headloop.StudyMaterialName.ToString();

                        returnData += "{\"categoryId\":\"" + categoryId
                                        + "\",\"categoryName\":\"" + categoryName;
                        if (headloop.Equals(last2))
                            returnData += "\"}";
                        else
                            returnData += "\"},";
                    }
                    return returnData + "]}";

                case 6:
                    var headerData3 = dbcontext.Learns.Where(c => c.Active == true && c.ParentID == null).OrderBy(n => n.Name).ToList();
                    var last3 = headerData3.Last();
                    foreach (var headloop in headerData3)
                    {
                        int categoryId = headloop.ID;
                        string categoryName = headloop.Name.ToString();

                        returnData += "{\"categoryId\":\"" + categoryId
                                        + "\",\"categoryName\":\"" + categoryName;
                        if (headloop.Equals(last3))
                            returnData += "\"}";
                        else
                            returnData += "\"},";
                    }
                    return returnData + "]}";

                default: break;
            }
            return null;
        }


        /// <summary>
        ///  -----------------------------------------------Tabs End-----------------
        /// </summary>
        /// <returns></returns>



        /// <summary>
        ///  -----------------------------------------------Home Begin-----------------
        /// </summary>
        /// <returns></returns>

        [WebMethod]
        public string DashBoard()
        {
            String returndata = "{\"DashBoard\":[";

            var Editorial = (from _notes in dbcontext.Editorials
                             where _notes.isDeleted != true
                             select new PostCategory
                             {
                                 PostId = _notes.EditorialID,
                                 Title = _notes.Title,
                                 Description = _notes.Description,
                                 DefaultImage = _notes.EditorialImage,
                                 Likes = _notes.Likes,
                                 CreatedDate = _notes.CreatedDate
                             }).OrderByDescending(d => d.CreatedDate).FirstOrDefault();
            returndata += "\"Editorial\":" + JsonConvert.SerializeObject(Editorial, Formatting.Indented) + ",";


            var PersonalityDevelopment = (from _notes in dbcontext.PersonalityDevlopments
                                          where _notes.Deleted != true
                                          select new PostCategory
                                          {
                                              PostId = _notes.PDID,
                                              Title = _notes.Title,
                                              Description = _notes.Description,
                                              DefaultImage = _notes.DefaultImage,
                                              Likes = _notes.Likes,
                                              CreatedDate = _notes.CreatedDate
                                          }).OrderByDescending(d => d.CreatedDate).FirstOrDefault();
            returndata += "\"PersonalityDevelopment\":" + JsonConvert.SerializeObject(PersonalityDevelopment, Formatting.Indented) + ",";

            var Notification = (from _notes in dbcontext.Notifications
                                where _notes.Deleted != true
                                select new PostCategory
                                {
                                    PostId = _notes.ID,
                                    Title = _notes.Title,
                                    Description = _notes.Description,
                                    Attachment = _notes.Attachment,
                                    Likes = _notes.Likes,
                                    CreatedDate = _notes.CreatedDate
                                }).OrderByDescending(d => d.CreatedDate).ToList();
            returndata += "\"NewNotification\":" + JsonConvert.SerializeObject(Notification, Formatting.Indented) + ",";


            var Categories = (from d in dbcontext.Categories where d.isActive != false select d).Take(4).ToList();
            List<PostCategory> polist = new List<PostCategory>();
            foreach (var cat in Categories)
            {
                var Latest = (from postData in dbcontext.Posts
                              where postData.CategoryId == cat.CategoryId && postData.IsDeleted == false
                              orderby postData.PublishedDate descending
                              select new PostCategory
                              {
                                  PostId = postData.PostId,
                                  CategoryId = postData.CategoryId,
                                  CategoryName = cat.CategoryName,
                                  Title = postData.Title,
                                  Slug = postData.Slug,
                                  Description = postData.Description,
                                  DefaultImage = postData.DefaultImage,
                                  IsPublished = postData.IsPublished,
                                  PublishedDate = postData.PublishedDate,
                                  ModifyDate = postData.ModifyDate,

                              }).FirstOrDefault();

                if (Latest != null)
                {
                    polist.Add(Latest);
                }
            }
            List<PostCategory> p2 = polist;

            return returndata += "\"Latest\":" + JsonConvert.SerializeObject(polist, Formatting.Indented);
        }

        [WebMethod]
        public string OldLatest()
        {
            string latestdetails = "{\"Latestlists\":";

            List<PostCategory> list = new List<PostCategory>();
            list = objectindex.latestCategories();

            latestdetails += JsonConvert.SerializeObject(list);
            return latestdetails += "}";
        }





        /// <summary>
        ///  -------------------------------------New Begin-----------------
        /// </summary>
        /// <returns></returns>


        //Latest - ip: TabID, SubMenuID, o/p: PostID
        [WebMethod]
        public string Latest(int TabID, int SubTabID)
        {
            String returnData = "{\"Latest\":";

            switch (TabID)
            {
                case 1:
                    break;

                case 2:
                    //Science & Technology
                    var Latest2 = (from _notes in dbcontext.STPosts
                                   join category in dbcontext.ScienceTechnologies on _notes.SubID equals category.ID
                                   where _notes.SubID == SubTabID
                                   select new PostCategory
                                   {
                                       PostId = _notes.ID,
                                       CategoryId = (int)_notes.SubID,
                                       Title = _notes.Title,
                                       Description = _notes.Description,
                                       DefaultImage = _notes.DefaultImage,
                                       CreatedDate = _notes.CreatedDate
                                   }).OrderBy(d => d.CreatedDate).ToList();
                    return returnData += JsonConvert.SerializeObject(Latest2) + "}";

                case 3:
                    //Current Affairs
                    var Latest3 = (from postdata in dbcontext.Posts
                                   join catdata in dbcontext.Categories on postdata.CategoryId equals catdata.CategoryId
                                   where postdata.IsDeleted != true && catdata.CategoryId == SubTabID
                                   select new PostCategory
                                   {
                                       PostId = postdata.PostId,
                                       CategoryId = postdata.CategoryId,
                                       CategoryName = catdata.CategoryName,
                                       Title = postdata.Title,
                                       Slug = postdata.Slug,
                                       Description = postdata.Description,
                                       DefaultImage = postdata.DefaultImage,
                                       IsPublished = postdata.IsPublished,
                                       PublishedDate = postdata.PublishedDate,
                                       ModifyDate = postdata.ModifyDate,
                                   }).OrderByDescending(x => x.PublishedDate).ToList<PostCategory>();

                    return returnData += JsonConvert.SerializeObject(Latest3) + "}";

                case 4:
                    //TalentPlatform
                    var Latest4 = (from _notes in dbcontext.TalentPosts
                                   join category in dbcontext.Talents on _notes.SubID equals category.ID
                                   where _notes.SubID == SubTabID
                                   select new PostCategory
                                   {
                                       PostId = _notes.ID,
                                       CategoryId = (int)_notes.SubID,
                                       Title = _notes.Title,
                                       Description = _notes.Description,
                                       DefaultImage = _notes.DefaultImage,
                                       CreatedDate = _notes.CreatedDate
                                   }).OrderBy(d => d.CreatedDate).ToList();
                    return returnData += JsonConvert.SerializeObject(Latest4) + "}";

                case 5:
                    //Learn More
                    var Latest5 = (from posts in dbcontext.LearnPosts
                                   join category in dbcontext.Learns on posts.SubID equals category.ID
                                   where posts.Deleted != true && posts.SubID == SubTabID
                                   select new PostCategory
                                   {
                                       PostId = posts.ID,
                                       CategoryId = posts.SubID,
                                       CategoryName = category.Name,
                                       Title = posts.Title,
                                       Description = posts.Description,
                                       DefaultImage = posts.DefaultImage,
                                       IsPublished = (bool)posts.Published,
                                       PublishedDate = posts.CreatedDate,
                                       ModifyDate = posts.ModifiedDate,
                                   }).OrderByDescending(x => x.PublishedDate).ToList<PostCategory>();
                    return returnData += JsonConvert.SerializeObject(Latest5) + "}";

                case 6:
                    //Person Personality
                    var Latest6 = (from _person in dbcontext.Person_Personality
                                   where _person.Published == true
                                   select new PostCategory
                                   {
                                       PostId = _person.ID,
                                       Title = _person.Title,
                                       DefaultImage = _person.DefaultImage,
                                       Description = _person.Description,
                                       IsPublished = (bool)_person.Published,
                                       CreatedDate = _person.CreatedDate
                                   }).OrderBy(c => c.CreatedDate).ToList();
                    return returnData += JsonConvert.SerializeObject(Latest6) + "}";


                case 7:
                    //Editorial
                    var Latest7 = (from _person in dbcontext.Editorials
                                   where _person.isPublished == true
                                   select new PostCategory
                                   {
                                       PostId = _person.EditorialID,
                                       Title = _person.Title,
                                       DefaultImage = _person.EditorialImage,
                                       Description = _person.Description,
                                       IsPublished = (bool)_person.isPublished,
                                       CreatedDate = _person.CreatedDate
                                   }).OrderBy(c => c.CreatedDate).ToList();
                    return returnData += JsonConvert.SerializeObject(Latest7) + "}";

                case 8:
                    //Personality Devlopment
                    var Latest8 = (from _person in dbcontext.PersonalityDevlopments
                                   where _person.Published == true
                                   select new PostCategory
                                   {
                                       PostId = _person.PDID,
                                       Title = _person.Title,
                                       DefaultImage = _person.DefaultImage,
                                       Description = _person.Description,
                                       IsPublished = (bool)_person.Published,
                                       CreatedDate = _person.CreatedDate
                                   }).OrderBy(c => c.CreatedDate).ToList();
                    return returnData += JsonConvert.SerializeObject(Latest8) + "}";

                case 9:
                    //New Notifications
                    var Latest9 = (from _person in dbcontext.Notifications
                                   where _person.Published == true
                                   select new PostCategory
                                   {
                                       PostId = _person.ID,
                                       Title = _person.Title,
                                       Attachment = _person.Attachment,
                                       Description = _person.Description,
                                       IsPublished = (bool)_person.Published,
                                       CreatedDate = _person.CreatedDate
                                   }).OrderBy(c => c.CreatedDate).ToList();
                    return returnData += JsonConvert.SerializeObject(Latest9) + "}";

                default: break;
            }

            return null;
        }


        //Category - TabID, SubMenuID - List Category
        [WebMethod]
        public string Category(int TabID, int SubTabID)
        {
            String returnData = "{\"Category\":";

            switch (TabID)
            {
                case 1:
                    break;

                case 2:
                    break;

                case 3:
                    break;

                default: break;
            }

            return null;
        }


        //Headlines - TabID, SubMenuID, CategoryID
        [WebMethod]
        public string Headlines(int TabID, int SubTabID)
        {
            String returnData = "{\"Headlines\":";

            switch (TabID)
            {
                case 1:

                    break;

                case 2:
                    //Science & Technology
                    var Category2 = (from _notes in dbcontext.STPosts
                                     join category in dbcontext.ScienceTechnologies on _notes.SubID equals category.ID
                                     where _notes.SubID == SubTabID
                                     select new PostCategory
                                     {
                                         PostId = _notes.ID,
                                         CategoryId = (int)_notes.SubID,
                                         Title = _notes.Title,
                                         Description = _notes.Description,
                                         DefaultImage = _notes.DefaultImage,
                                         CreatedDate = _notes.CreatedDate
                                     }).OrderBy(d => d.CreatedDate).ToList();

                    return returnData += JsonConvert.SerializeObject(Category2) + "}";

                case 3:
                    //current affairs
                    var Category3 = (from postdata in dbcontext.Posts
                                     join catdata in dbcontext.Categories on postdata.CategoryId equals catdata.CategoryId
                                     where postdata.IsDeleted != true && catdata.CategoryId == SubTabID
                                     select new PostCategory
                                     {
                                         PostId = postdata.PostId,
                                         CategoryId = postdata.CategoryId,
                                         CategoryName = catdata.CategoryName,
                                         Title = postdata.Title,
                                         PublishedDate = postdata.PublishedDate,
                                     }).OrderByDescending(x => x.PublishedDate).ToList<PostCategory>();

                    return returnData += JsonConvert.SerializeObject(Category3) + "}";

                case 4:
                    //Talent Platform
                    var Category4 = (from _notes in dbcontext.TalentPosts
                                     join category in dbcontext.Talents on _notes.SubID equals category.ID
                                     where _notes.IsDeleted != true && _notes.SubID == SubTabID
                                     select new PostCategory
                                     {
                                         PostId = _notes.ID,
                                         CategoryId = (int)_notes.SubID,
                                         CategoryName = category.Name,
                                         Title = _notes.Title,
                                         CreatedDate = _notes.CreatedDate
                                     }).OrderBy(d => d.CreatedDate).ToList();
                    return returnData += JsonConvert.SerializeObject(Category4) + "}";

                case 5:
                    //Learn More
                    var Category5 = (from posts in dbcontext.LearnPosts
                                     join category in dbcontext.Learns on posts.SubID equals category.ID
                                     where posts.Deleted != true && category.ID == SubTabID
                                     select new PostCategory
                                     {
                                         PostId = posts.ID,
                                         CategoryId = posts.SubID,
                                         CategoryName = category.Name,
                                         Title = posts.Title,
                                         PublishedDate = posts.CreatedDate,
                                     }).OrderByDescending(x => x.PublishedDate).ToList<PostCategory>();
                    return returnData += JsonConvert.SerializeObject(Category5) + "}";


                case 6:
                    //Person Personality
                    var Category6 = (from _notes in dbcontext.Person_Personality
                                     where _notes.Deleted != true
                                     select new PostCategory
                                     {
                                         PostId = _notes.ID,
                                         Title = _notes.Title,
                                         CreatedDate = _notes.CreatedDate
                                     }).ToList();
                    return returnData += JsonConvert.SerializeObject(Category6) + "}";

                case 7:
                    //Editorials
                    var Category7 = (from _notes in dbcontext.Editorials
                                     where _notes.isDeleted != true
                                     select new PostCategory
                                     {
                                         PostId = _notes.EditorialID,
                                         Title = _notes.Title,
                                         CreatedDate = _notes.CreatedDate
                                     }).ToList();
                    return returnData += JsonConvert.SerializeObject(Category7) + "}";

                case 8:
                    //Personality Development
                    var Category8 = (from _notes in dbcontext.PersonalityDevlopments
                                     where _notes.Deleted != true
                                     select new PostCategory
                                     {
                                         PostId = _notes.PDID,
                                         Title = _notes.Title,
                                         CreatedDate = _notes.CreatedDate
                                     }).ToList();
                    return returnData += JsonConvert.SerializeObject(Category8) + "}";


                case 9:
                    //New Notifications
                    var Category9 = (from _notes in dbcontext.Notifications
                                     where _notes.Deleted != true
                                     select new PostCategory
                                     {
                                         PostId = _notes.ID,
                                         Title = _notes.Title,
                                         CreatedDate = _notes.CreatedDate
                                     }).ToList();
                    return returnData += JsonConvert.SerializeObject(Category9) + "}";

                default: break;
            }

            return null;
        }


        //DetailedPost - TabID, SubMenuID, CategoryID, PostID - Comments Display
        [WebMethod]
        public string DetailedPost(int PostID, int TabID, int ProfileID)
        {
            String returndata = "{\"DetailedPost\":[";



            //bool UserFound = new UserExistence(User.Identity.Name).isExist;
            bool UserFound = new UserExistence(ProfileID).isExist;
            if (!UserFound)
            {
                //Login Required
                //redirect to Login Page
                string alert = "Invalid User! Please Login and Continue";
                return returndata += "{\"Alert\":" + JsonConvert.SerializeObject(alert) + "}";
            }
            else
            {

                switch (TabID)
                {
                    //Study Materials
                    case 1:

                        bool isLiked1 = new LikeCommentDetail(TabID, PostID, ProfileID)._isLiked;
                        var Post1 = (from _notes in dbcontext.StudyMaterialPosts
                                     where _notes.StudyMaterialID == PostID
                                     select new PostCategory
                                     {
                                         PostId = _notes.StudyMaterialID,
                                         CategoryId = (int)_notes.StudyID,
                                         Title = _notes.Title,
                                         Description = _notes.Description,
                                         isLiked = isLiked1,
                                         Likes = _notes.Likes,
                                         DefaultImage = _notes.DefaultImage,
                                         CreatedDate = _notes.CreatedDate
                                     }).ToList();
                        returndata += "{\"Post\":" + JsonConvert.SerializeObject(Post1, Formatting.Indented);

                        return returndata + "}]}";

                    //Science & Technology
                    case 2:
                        bool isLiked2 = new LikeCommentDetail(TabID, PostID, ProfileID)._isLiked;

                        var Notes = (from _notes in dbcontext.STPosts
                                     where _notes.ID == PostID
                                     select new PostCategory
                                     {
                                         PostId = _notes.ID,
                                         CategoryId = (int)_notes.SubID,
                                         Title = _notes.Title,
                                         Description = _notes.Description,
                                         DefaultImage = _notes.DefaultImage,
                                         Likes = _notes.Likes,
                                         isLiked = isLiked2,
                                         CreatedDate = _notes.CreatedDate
                                     }).ToList();
                        returndata += "{\"PostDetails\":" + JsonConvert.SerializeObject(Notes, Formatting.Indented);

                        return returndata + "}]}";

                    //Current Affairs
                    case 3:
                        //get likes count and comment count
                        bool isLiked3 = new LikeCommentDetail(TabID, PostID, ProfileID)._isLiked;

                        var Post3 = (from postdata1 in dbcontext.Posts
                                     join catdata1 in dbcontext.Categories on postdata1.CategoryId equals catdata1.CategoryId
                                     where postdata1.IsDeleted != true && postdata1.PostId == PostID
                                     select new PostCategory
                                     {
                                         PostId = postdata1.PostId,
                                         CategoryId = postdata1.CategoryId,
                                         CategoryName = catdata1.CategoryName,
                                         Title = postdata1.Title,
                                         Slug = postdata1.Slug,
                                         Description = postdata1.Description,
                                         DefaultImage = postdata1.DefaultImage,
                                         Likes = postdata1.Likes,
                                         Comments = postdata1.Comments,
                                         IsPublished = postdata1.IsPublished,
                                         PublishedDate = postdata1.PublishedDate,
                                         ModifyDate = postdata1.ModifyDate,
                                         isLiked = isLiked3,
                                     }).OrderByDescending(x => x.PublishedDate).ToList();
                        returndata += "{\"PostDetails\":" + JsonConvert.SerializeObject(Post3, Formatting.Indented);

                        return returndata + "}]}";

                    //Talent Platform
                    case 4:
                        //get likes count and comment count
                        bool isLiked4 = new LikeCommentDetail(TabID, PostID, ProfileID)._isLiked;

                        var Post4 = (from _notes in dbcontext.STPosts
                                     where _notes.ID == PostID
                                     select new PostCategory
                                     {
                                         PostId = _notes.ID,
                                         CategoryId = (int)_notes.SubID,
                                         Title = _notes.Title,
                                         Description = _notes.Description,
                                         DefaultImage = _notes.DefaultImage,
                                         Likes = _notes.Likes,
                                         isLiked = isLiked4,
                                         CreatedDate = _notes.CreatedDate
                                     }).ToList();
                        returndata += "{\"PostDetails\":" + JsonConvert.SerializeObject(Post4, Formatting.Indented);

                        return returndata + "}]}";

                    //Learn More
                    case 5:
                        //get likes count and comment count
                        bool isLiked5 = new LikeCommentDetail(TabID, PostID, ProfileID)._isLiked;
                        var Post5 = (from posts in dbcontext.LearnPosts
                                     join category in dbcontext.Learns on posts.SubID equals category.ID
                                     where posts.Deleted != true && posts.ID == PostID
                                     select new PostCategory
                                     {
                                         PostId = posts.ID,
                                         CategoryId = posts.SubID,
                                         CategoryName = category.Name,
                                         Title = posts.Title,
                                         Description = posts.Description,
                                         DefaultImage = posts.DefaultImage,
                                         isLiked = isLiked5,
                                         Likes = posts.Likes,
                                         IsPublished = (bool)posts.Published,
                                         PublishedDate = posts.CreatedDate,
                                         ModifyDate = posts.ModifiedDate
                                     }).OrderByDescending(x => x.PublishedDate).ToList();
                        returndata += "{\"PostDetails\":" + JsonConvert.SerializeObject(Post5, Formatting.Indented);

                        return returndata + "}]}";

                    //Person-Personality
                    case 6:
                        //get likes count and comment count
                        bool isLiked6 = new LikeCommentDetail(TabID, PostID, ProfileID)._isLiked;
                        var Post6 = (from _notes in dbcontext.Person_Personality
                                     where _notes.ID == PostID
                                     select new PostCategory
                                     {
                                         PostId = _notes.ID,
                                         Title = _notes.Title,
                                         Description = _notes.Description,
                                         DefaultImage = _notes.DefaultImage,
                                         Likes = _notes.Likes,
                                         isLiked = isLiked6,
                                         CreatedDate = _notes.CreatedDate
                                     }).ToList();
                        returndata += "{\"PostDetails\":" + JsonConvert.SerializeObject(Post6, Formatting.Indented);

                        return returndata + "}]}";

                    //Editorial
                    case 7:
                        //get likes count and comment count
                        bool isLiked7 = new LikeCommentDetail(TabID, PostID, ProfileID)._isLiked;
                        var Post7 = (from _notes in dbcontext.Editorials
                                     where _notes.EditorialID == PostID
                                     select new PostCategory
                                     {
                                         PostId = _notes.EditorialID,
                                         Title = _notes.Title,
                                         Description = _notes.Description,
                                         DefaultImage = _notes.EditorialImage,
                                         Likes = _notes.Likes,
                                         isLiked = isLiked7,
                                         CreatedDate = _notes.CreatedDate
                                     }).ToList();

                        returndata += "{\"PostDetails\":" + JsonConvert.SerializeObject(Post7, Formatting.Indented);
                        return returndata + "}]}";

                    //Personality Development
                    case 8:
                        //get likes count and comment count
                        bool isLiked8 = new LikeCommentDetail(TabID, PostID, ProfileID)._isLiked;
                        var Post8 = (from _notes in dbcontext.PersonalityDevlopments
                                     where _notes.PDID == PostID
                                     select new PostCategory
                                     {
                                         PostId = _notes.PDID,
                                         Title = _notes.Title,
                                         Description = _notes.Description,
                                         DefaultImage = _notes.DefaultImage,
                                         Likes = _notes.Likes,
                                         isLiked = isLiked8,
                                         CreatedDate = _notes.CreatedDate
                                     }).ToList();

                        returndata += "{\"PostDetails\":" + JsonConvert.SerializeObject(Post8, Formatting.Indented);
                        return returndata + "}]}";
                    
                    //New Notification
                    case 9:
                        //get likes count and comment count
                        bool isLiked9 = new LikeCommentDetail(TabID, PostID, ProfileID)._isLiked;
                        var Post9 = (from _notes in dbcontext.Notifications
                                     where _notes.ID == PostID
                                     select new PostCategory
                                     {
                                         PostId = _notes.ID,
                                         Title = _notes.Title,
                                         Description = _notes.Description,
                                         Attachment = _notes.Attachment,
                                         Likes = _notes.Likes,
                                         CreatedDate = _notes.CreatedDate
                                     }).ToList();

                        returndata += "{\"PostDetails\":" + JsonConvert.SerializeObject(Post9, Formatting.Indented);
                        return returndata + "}]}";


                    default: break;
                }
                return null;
            }
        }


        ///  /// <summary>
        ///  -------------------------------------New End-----------------
        /// </summary>
        /// <returns></returns>



        //------------------------------------Contact Details Begin-----------------------------------------------

        [WebMethod]
        public string Contact(string firstname, string lastname, string email, string phone, string message)
        {
            string returndata = "{\"Enquiry\":";

            SmtpClient smtp1;
            string from = "technical@shlrtechnosoft.in";
            string toEmail = "shilpah@shlrtechnosoft.com";

            using (MailMessage mail = new MailMessage())
            {
                mail.From = new MailAddress(from);
                mail.To.Add(toEmail);

                mail.Subject = "Enquiry";

                mail.Body = "Hello " + toEmail + Environment.NewLine +
                    "You Got A New Enquiry..!" + Environment.NewLine + Environment.NewLine +
                    "Name : " + firstname + Environment.NewLine +
                    "Last Name : " + lastname + Environment.NewLine +
                    "Email : " + email + Environment.NewLine +
                    "Phone : " + phone + Environment.NewLine +
                    "Message : " + message + Environment.NewLine;

                mail.IsBodyHtml = false;

                smtp1 = new SmtpClient();
                smtp1.Host = "smtp.gmail.com";
                smtp1.Port = 587;

                smtp1.Credentials = new System.Net.NetworkCredential
                ("technical@shlrtechnosoft.in", "Technical@123");

                smtp1.EnableSsl = true;
                smtp1.Send(mail);
            }

            string alert = "<script> alert('Data Submitted Successfully...Thank you!') </script>";
            returndata += JsonConvert.SerializeObject(alert);
            return returndata += "}";
        }

        //------------------------------------Contact Details End-----------------------------------------------

        /// <summary>
        ///  -----------------------------------------------Home End-----------------
        /// </summary>
        /// <returns></returns>




        /// <summary>
        /// Like , Comment , Reply -----------------------------------------------Begin---------------
        /// </summary>
        /// <returns></returns>

        [WebMethod]
        public string LikePost(int PostID, int UserID, int isLiked, int TabID)
        {
            string LikeDetails = ""; int? likeCount = 0;

            //int getUserID = new GetProfileID(User.Identity.Name)._profileID;
            //bool isnull = string.IsNullOrEmpty(User.Identity.Name);
            bool UserFound = new UserExistence(UserID).isExist;
            if (!UserFound)
            {
                //Login Required
                //redirect to Login Page
                string alert = "Invalid User! Please Login and Continue";
                LikeDetails += "{\"LikeDetails\":";
                LikeDetails += JsonConvert.SerializeObject(alert);
                LikeDetails += "}";
                return LikeDetails;
            }
            else
            {
                if (isLiked == 1)
                {
                    CommentAndReply obj = new CommentAndReply();
                    likeCount = obj.addLike(PostID, TabID);

                    Like like = new Like();
                    {
                        like.PostID = PostID;
                        like.ProfileID = UserID;
                        like.Liked = true;
                        like.SectionID = TabID;
                        like.CreatedDate = DateTime.Now;
                        dbcontext.Likes.Add(like);
                        int save = dbcontext.SaveChanges();
                    }
                }
                else
                {
                    CommentAndReply obj = new CommentAndReply();
                    likeCount = obj.unLike(PostID, TabID, (int)UserID);
                }
            }
            LikeDetails += "{\"LikeDetails\":";
            LikeDetails += JsonConvert.SerializeObject(likeCount);
            LikeDetails += "}";
            return LikeDetails;
        }

        [WebMethod]
        public string CommentPost(string Comments, int PostID, int UserID, int TabID)
        {
            string PostComment = ""; int? CommentsCount = 0;

            bool UserFound = new UserExistence(UserID).isExist;
            if (!UserFound)
            {
                //Login Required
                //redirect to Login Page
                string alert = "Invalid User! Please Login and Continue";
                PostComment += "{\"PostComment\":";
                PostComment += JsonConvert.SerializeObject(alert);
                PostComment += "}";
                return PostComment;
            }
            else
            {
                CommentAndReply obj = new CommentAndReply();
                CommentsCount = obj.addComment(UserID, TabID, PostID, Comments);

                Comment _insert = new Comment();
                {
                    _insert.PostID = PostID;
                    _insert.ProfileID = UserID;
                    _insert.Comments = Comments;
                    _insert.ReplyCount = 0;
                    _insert.SectionID = TabID;
                    _insert.CreatedDate = DateTime.Now;
                }
                dbcontext.Comments.Add(_insert);
                int save = dbcontext.SaveChanges();
            }

            PostComment += "{\"PostComment\":";
            PostComment += JsonConvert.SerializeObject(CommentsCount);
            PostComment += "}";

            return PostComment;
        }

        [WebMethod]
        public string ReplyPost(string Reply, int PostID, int CommentID, int UserID, int TabID)
        {
            string PostReply = ""; int? ReplyCount = 0;
            bool UserFound = new UserExistence(UserID).isExist;
            if (!UserFound)
            {
                //Login Required
                //redirect to Login Page
                string alert = "Invalid User! Please Login and Continue";
                PostReply += "{\"PostReply\":";
                PostReply += JsonConvert.SerializeObject(alert);
                PostReply += "}";
                return PostReply;
            }
            else
            {
                CommentAndReply obj = new CommentAndReply();
                ReplyCount = obj.addReply(Reply, PostID, CommentID, UserID, TabID);
            }

            PostReply += "{\"PostReply\":";
            PostReply += JsonConvert.SerializeObject(ReplyCount);
            PostReply += "}";

            return PostReply;
        }

        /// <summary>
        /// Like , Comment , Reply -----------------------------------------------End-----------------
        /// </summary>
        /// <returns></returns>

    }
}
