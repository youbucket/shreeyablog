﻿using ShreeyaBlog.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity.Core.Objects;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ShreeyaBlog.Classes
{
    interface Index
    {
        List<Greeting> greeting();
        List<Parallax> parallax();
        List<PostCategory> editorial();
        List<PostCategory> personalDevelopment();
        List<PostCategory> notification();
        List<AdvertiseImage> advertiseImage();

        List<PostCategory> latestCategories();
        List<PostCategory> latestPost();
        List<PostCategory> categoryListTake6(int CategoryID);
        List<PostCategory> categoryListTake3(int CategoryID);
        List<PostCategory> categoryListTake2(int CategoryID);
        List<Video> GetVideos();
    }

    public class DerivedIndex : Index
    {
        public ShreeyaBlogEntities dbcontext = new ShreeyaBlogEntities();

        public List<Video> GetVideos()
        {
            return dbcontext.Videos.ToList();
        }

        public List<Greeting> greeting()
        {
            var GreetingList = dbcontext.Greetings.Where(greet => greet.Active != false).OrderBy(cd => cd.CreatedDate).ToList();
            return GreetingList;
        }

        public List<Parallax> parallax()
        {
            var ParallaxList = dbcontext.Parallaxes.Where(a => a.Active == true && a.Category == "Home").OrderBy(cr => cr.CreatedDate).ToList();
            return ParallaxList;
        }

        public List<PostCategory> editorial()
        {
            var Editorial = (from editorial in dbcontext.Editorials
                             where editorial.isPublished == true
                             select new PostCategory
                             {
                                 EditorialID = editorial.EditorialID,
                                 Title = editorial.Title,
                                 Description = editorial.Description,
                                 DefaultImage = editorial.EditorialImage,
                                 CreatedDate = editorial.CreatedDate
                             }).OrderByDescending(p => p.CreatedDate).Take(1).ToList();
            return Editorial;
        }

        public List<PostCategory> personalDevelopment()
        {
            var PDevelopment = (from _pd in dbcontext.PersonalityDevlopments
                                where _pd.Published == true
                                select new PostCategory
                                {
                                    EditorialID = _pd.PDID,
                                    Title = _pd.Title,
                                    Description = _pd.Description,
                                    DefaultImage = _pd.DefaultImage,
                                    CreatedDate = _pd.CreatedDate
                                }).OrderByDescending(p => p.CreatedDate).Take(1).ToList();
            return PDevelopment;
        }

        public List<PostCategory> notification()
        {
            var Notifications = (from _pd in dbcontext.Notifications
                                 where _pd.Published == true
                                 select new PostCategory
                                 {
                                     PostId = _pd.ID,
                                     Title = _pd.Title,
                                 }).OrderByDescending(p => p.PostId).Take(3).ToList();
            return Notifications;

            //var Notifications = dbcontext.Notifications.Where(p => p.Published != false).OrderByDescending(x => x.CreatedDate).Take(5).ToList();
            //return Notifications;
        }

        public List<AdvertiseImage> advertiseImage()
        {
            var ImageList = dbcontext.AdvertiseImages.Where(p => p.Status != false).OrderByDescending(x => x.CreatedDate).ToList();
            return ImageList;
        }

        public List<PostCategory> latestCategories()
        {
            var c = (from d in dbcontext.Categories where d.isActive==true select d ).ToList();
            List<PostCategory> polist = new List<PostCategory>();
            foreach (var ca in c)
            {

                var p1 = (from postData in dbcontext.Posts
                          where postData.CategoryId == ca.CategoryId && postData.IsDeleted == false 
                          orderby postData.PublishedDate descending
                          select new PostCategory
                          {
                              PostId = postData.PostId,
                              CategoryId = postData.CategoryId,
                              CategoryName = ca.CategoryName,
                              Title = postData.Title,
                              Slug = postData.Slug,
                              Description = postData.Description,
                              DefaultImage = postData.DefaultImage,
                              IsPublished = postData.IsPublished,
                              PublishedDate = postData.PublishedDate,
                              ModifyDate = postData.ModifyDate,
                          }).FirstOrDefault();
                if (p1 != null)
                {
                    polist.Add(p1);
                }
            }
            List<PostCategory> p2 = polist;
            return polist;
        }

        public List<PostCategory> latestPost()
        {
            var LatestLive = (from postdata1 in dbcontext.Posts
                              join catdata1 in dbcontext.Categories on postdata1.CategoryId equals catdata1.CategoryId
                              where postdata1.IsDeleted != true && postdata1.IsPublished == true
                              select new PostCategory
                              {
                                  PostId = postdata1.PostId,
                                  CategoryId = postdata1.CategoryId,
                                  CategoryName = catdata1.CategoryName,
                                  Title = postdata1.Title,
                                  Slug = postdata1.Slug,
                                  Description = postdata1.Description,
                                  DefaultImage = postdata1.DefaultImage,
                                  IsPublished = postdata1.IsPublished,
                                  PublishedDate = postdata1.PublishedDate,
                                  ModifyDate = postdata1.ModifyDate,
                              }).OrderByDescending(x => x.PublishedDate).Take(9).ToList();
            return LatestLive;
        }

        public List<PostCategory> categoryListTake6(int CategoryID)
        {
            var CatList = (from postdata1 in dbcontext.Posts
                           join catdata1 in dbcontext.Categories on postdata1.CategoryId equals catdata1.CategoryId
                           where postdata1.IsDeleted != true && catdata1.CategoryId == CategoryID
                           select new PostCategory
                           {
                               PostId = postdata1.PostId,
                               CategoryId = postdata1.CategoryId,
                               CategoryName = catdata1.CategoryName,
                               Title = postdata1.Title,
                               Slug = postdata1.Slug,
                               Description = postdata1.Description,
                               DefaultImage = postdata1.DefaultImage,
                               IsPublished = postdata1.IsPublished,
                               PublishedDate = postdata1.PublishedDate,
                               ModifyDate = postdata1.ModifyDate,
                           }).OrderByDescending(x => x.PublishedDate).Take(6).ToList<PostCategory>();
            return CatList;
        }

        public List<PostCategory> categoryListTake3(int CategoryID)
        {
            var CatList = (from postdata1 in dbcontext.Posts
                           join catdata1 in dbcontext.Categories on postdata1.CategoryId equals catdata1.CategoryId
                           where postdata1.IsDeleted != true && catdata1.CategoryId == CategoryID
                           select new PostCategory
                           {
                               PostId = postdata1.PostId,
                               CategoryId = postdata1.CategoryId,
                               CategoryName = catdata1.CategoryName,
                               Title = postdata1.Title,
                               Slug = postdata1.Slug,
                               Description = postdata1.Description,
                               DefaultImage = postdata1.DefaultImage,
                               IsPublished = postdata1.IsPublished,
                               PublishedDate = postdata1.PublishedDate,
                               ModifyDate = postdata1.ModifyDate,
                           }).OrderByDescending(x => x.PublishedDate).Take(3).ToList<PostCategory>();
            return CatList;
        }

        public List<PostCategory> categoryListTake2(int CategoryID)
        {
            var CatList = (from postdata1 in dbcontext.Posts
                           join catdata1 in dbcontext.Categories on postdata1.CategoryId equals catdata1.CategoryId
                           where postdata1.IsDeleted != true && catdata1.CategoryId == CategoryID
                           select new PostCategory
                           {
                               PostId = postdata1.PostId,
                               CategoryId = postdata1.CategoryId,
                               CategoryName = catdata1.CategoryName,
                               Title = postdata1.Title,
                               Slug = postdata1.Slug,
                               Description = postdata1.Description,
                               DefaultImage = postdata1.DefaultImage,
                               IsPublished = postdata1.IsPublished,
                               PublishedDate = postdata1.PublishedDate,
                               ModifyDate = postdata1.ModifyDate,
                           }).OrderByDescending(x => x.PublishedDate).Take(2).ToList<PostCategory>();
            return CatList;
        }
    }
}
