﻿using ShreeyaBlog.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ShreeyaBlog.Classes
{
    public enum CommentSection : int
    {
        StudyMaterials = 1,
        ScienceTechnology,
        CurrentAffairs,
        TalentPlatform,
        LearnMore,
        PersonPersonality,
        Editorial,
        PersonalityDevelopment
    }
    public class CommentAndReply
    {
        public ShreeyaBlogEntities dbcontext = new ShreeyaBlogEntities();
        public List<CommentDetails> getComments(int _sectionID, int _postID)
        {
            List<CommentDetails> _commentList = new List<CommentDetails>();

            var listofComments = (from _comments in dbcontext.Comments
                                  //join _post in dbcontext.Posts on _comments.PostID equals _post.PostId
                                  join _profile in dbcontext.Profiles on _comments.ProfileID equals _profile.ProfileID
                                  where _comments.SectionID == _sectionID && _comments.PostID == _postID
                                  select new CommentDetails
                                  {
                                      CommentID = _comments.CommentID,
                                      PostId = _comments.PostID,
                                      Image = _profile.Image,
                                      FirstName = _profile.FirstName,
                                      CreatedDate = _comments.CreatedDate,
                                      Comments = _comments.Comments,
                                  }).OrderByDescending(x => x.CreatedDate).ToList<CommentDetails>();

            _commentList = listofComments;
            if (_commentList != null)
            {
                return _commentList;
            }
            return _commentList;
        }

        public List<CommentDetails> getReplies(int _sectionID, int _postID)
        {
            List<CommentDetails> _replyList = new List<CommentDetails>();

            var listofReplies = (from _reply in dbcontext.ReplyComments
                                 //join _post in dbcontext.Posts on _reply.PostID equals _post.PostId
                                 join _profile in dbcontext.Profiles on _reply.ProfileID equals _profile.ProfileID
                                 join _comment in dbcontext.Comments on _reply.CommentID equals _comment.CommentID
                                 where _reply.SectionID == _sectionID && _reply.PostID == _postID
                                 select new CommentDetails
                                 {
                                     CommentID = _reply.CommentID,
                                     Reply = _reply.Reply,
                                     PostId = _reply.PostID,
                                     Image = _profile.Image,
                                     FirstName = _profile.FirstName,
                                     CreatedDate = _reply.CreatedDate
                                 }).OrderBy(x => x.CreatedDate).ToList<CommentDetails>();

            _replyList = listofReplies;

            if (_replyList != null)
            {
                return _replyList;
            }
            return _replyList;
        }

        public int addComment(int UserID, int SectionID, int PostID, string Comment)
        {
            int? CommentCount = 0;
            switch (SectionID)
            {
                case (int)CommentSection.StudyMaterials:
                    {
                        StudyMaterialPost _study = dbcontext.StudyMaterialPosts.FirstOrDefault(a => a.StudyMaterialID == PostID);
                        if (_study != null)
                        {
                            CommentCount = ++_study.Comments;
                        }
                        break;
                    }

                case (int)CommentSection.ScienceTechnology:
                    {
                        STPost _science = dbcontext.STPosts.FirstOrDefault(a => a.ID == PostID);
                        if (_science != null)
                        {
                            CommentCount = ++_science.Comments;
                        }
                        break;
                    }

                case (int)CommentSection.CurrentAffairs:
                    {
                        Post _current = dbcontext.Posts.FirstOrDefault(a => a.PostId == PostID);
                        if (_current != null)
                        {
                            CommentCount = ++_current.Comments;
                        }
                        break;
                    }

                case (int)CommentSection.TalentPlatform:
                    {
                        TalentPost _talent = dbcontext.TalentPosts.FirstOrDefault(a => a.ID == PostID);
                        if (_talent != null)
                        {
                            CommentCount = ++_talent.Comments;
                        }
                        break;
                    }

                case (int)CommentSection.LearnMore:
                    {
                        LearnPost _learn = dbcontext.LearnPosts.FirstOrDefault(a => a.ID == PostID);
                        if (_learn != null)
                        {
                            CommentCount = ++_learn.Comments;
                        }
                        break;
                    }

                case (int)CommentSection.PersonPersonality:
                    {
                        Person_Personality _person = dbcontext.Person_Personality.FirstOrDefault(a => a.ID == PostID);
                        if (_person != null)
                        {
                            CommentCount = ++_person.Comments;
                        }
                        break;
                    }

                case (int)CommentSection.Editorial:
                    {
                        Editorial _editorial = dbcontext.Editorials.FirstOrDefault(a => a.EditorialID == PostID);
                        if (_editorial != null)
                        {
                            CommentCount = ++_editorial.Comments;
                        }
                        break;
                    }

                case (int)Section.PersonalityDevelopment:
                    {
                        PersonalityDevlopment _personality = dbcontext.PersonalityDevlopments.FirstOrDefault(a => a.PDID == PostID);
                        if (_personality != null)
                        {
                            CommentCount = ++_personality.Comments;
                        }
                        break;
                    }

                default:
                    throw new InvalidOperationException("Couldn't process operation: " + SectionID);
            }
            dbcontext.SaveChanges();
            return (int)CommentCount;
        }

        public int addReply(string reply, int PostID, int CommentId, int UserID, int TabID)
        {
            int? ReplyCount = 0;
            using (var Transaction = dbcontext.Database.BeginTransaction())
            {
                try
                {
                    ReplyComment _insert = new ReplyComment();
                    {
                        _insert.CommentID = CommentId;
                        _insert.Reply = reply;
                        _insert.CreatedDate = DateTime.Now;
                        _insert.ProfileID = UserID;
                        _insert.PostID = PostID;
                        _insert.SectionID = TabID;
                    }
                    dbcontext.ReplyComments.Add(_insert);

                    Comment comment = dbcontext.Comments.FirstOrDefault(x => x.CommentID == CommentId && x.SectionID == TabID);
                    ReplyCount = ++comment.ReplyCount;

                    int save = dbcontext.SaveChanges();
                    Transaction.Commit();
                }
                catch
                {
                    Transaction.Rollback();
                }
            }
            return (int)ReplyCount;
        }

        public int addLike(int PostsID, int TabID)
        {
            int? LikeCount = 0;
            switch (TabID)
            {
                case (int)CommentSection.StudyMaterials:
                    {
                        StudyMaterialPost _study = dbcontext.StudyMaterialPosts.FirstOrDefault(a => a.StudyMaterialID == PostsID);
                        if (_study != null)
                        {
                            LikeCount = ++_study.Likes;
                        }
                        break;
                    }

                case (int)CommentSection.ScienceTechnology:
                    {
                        STPost _science = dbcontext.STPosts.FirstOrDefault(a => a.ID == PostsID);
                        if (_science != null)
                        {
                            LikeCount = ++_science.Likes;
                        }
                        break;
                    }

                case (int)CommentSection.CurrentAffairs:
                    {
                        Post _current = dbcontext.Posts.FirstOrDefault(a => a.PostId == PostsID);
                        if (_current != null)
                        {
                            LikeCount = ++_current.Likes;
                        }
                        break;
                    }

                case (int)CommentSection.TalentPlatform:
                    {
                        TalentPost _talent = dbcontext.TalentPosts.FirstOrDefault(a => a.ID == PostsID);
                        if (_talent != null)
                        {
                            LikeCount = ++_talent.Likes;
                        }
                        break;
                    }

                case (int)CommentSection.LearnMore:
                    {
                        LearnPost _learn = dbcontext.LearnPosts.FirstOrDefault(a => a.ID == PostsID);
                        if (_learn != null)
                        {
                            LikeCount = ++_learn.Likes;
                        }
                        break;
                    }

                case (int)CommentSection.PersonPersonality:
                    {
                        Person_Personality _person = dbcontext.Person_Personality.FirstOrDefault(a => a.ID == PostsID);
                        if (_person != null)
                        {
                            LikeCount = ++_person.Likes;
                        }
                        break;
                    }
              
                case (int)CommentSection.Editorial:
                    {
                        Editorial _editorial = dbcontext.Editorials.FirstOrDefault(a => a.EditorialID == PostsID);
                        if (_editorial != null)
                        {
                            LikeCount = ++_editorial.Likes;
                        }
                        break;
                    }

                case (int)Section.PersonalityDevelopment:
                    {
                        PersonalityDevlopment _personality = dbcontext.PersonalityDevlopments.FirstOrDefault(a => a.PDID == PostsID);
                        if (_personality != null)
                        {
                            LikeCount = ++_personality.Likes;
                        }
                        break;
                    }

                default:
                    throw new InvalidOperationException("Couldn't process operation: " + TabID);
            }
            dbcontext.SaveChanges();
            return (int)LikeCount;
        }

        public int unLike(int PostsID, int TabID, int UserID)
        {
            int? LikeCount = 0;
            Like unlike = null;
            switch (TabID)
            {
                case (int)CommentSection.StudyMaterials:
                    {
                        StudyMaterialPost _study = dbcontext.StudyMaterialPosts.FirstOrDefault(a => a.StudyMaterialID == PostsID);
                        unlike = dbcontext.Likes.FirstOrDefault(x => x.PostID == PostsID && x.SectionID == TabID && x.ProfileID == UserID);
                        if (_study != null && unlike != null)
                        {
                            LikeCount = (_study.Likes == 0) ? 0 : --_study.Likes;
                        }
                        break;
                    }

                case (int)CommentSection.ScienceTechnology:
                    {
                        STPost _science = dbcontext.STPosts.FirstOrDefault(a => a.ID == PostsID);
                        unlike = dbcontext.Likes.FirstOrDefault(x => x.PostID == PostsID && x.SectionID == TabID && x.ProfileID == UserID);
                        if (_science != null && unlike != null)
                        {
                            LikeCount = (_science.Likes == 0) ? 0 : --_science.Likes;
                        }
                        break;
                    }

                case (int)CommentSection.CurrentAffairs:
                    {
                        Post _current = dbcontext.Posts.FirstOrDefault(a => a.PostId == PostsID);
                        unlike = dbcontext.Likes.FirstOrDefault(x => x.PostID == PostsID && x.SectionID == TabID && x.ProfileID == UserID);
                        if (_current != null && unlike != null)
                        {
                            LikeCount = (_current.Likes == 0) ? 0 : --_current.Likes;
                        }
                        break;
                    }

                case (int)CommentSection.TalentPlatform:
                    {
                        TalentPost _talent = dbcontext.TalentPosts.FirstOrDefault(a => a.ID == PostsID);
                        unlike = dbcontext.Likes.FirstOrDefault(x => x.PostID == PostsID && x.SectionID == TabID && x.ProfileID == UserID);
                        if (_talent != null && unlike != null)
                        {
                            LikeCount = (_talent.Likes == 0) ? 0 : --_talent.Likes;
                        }
                        break;
                    }

                case (int)CommentSection.LearnMore:
                    {
                        LearnPost _learn = dbcontext.LearnPosts.FirstOrDefault(a => a.ID == PostsID);
                        unlike = dbcontext.Likes.FirstOrDefault(x => x.PostID == PostsID && x.SectionID == TabID && x.ProfileID == UserID);
                        if (_learn != null && unlike != null)
                        {
                            LikeCount = (_learn.Likes == 0) ? 0 : --_learn.Likes;
                        }
                        break;
                    }

                case (int)CommentSection.PersonPersonality:
                    {
                        Person_Personality _person = dbcontext.Person_Personality.FirstOrDefault(a => a.ID == PostsID);
                        unlike = dbcontext.Likes.FirstOrDefault(x => x.PostID == PostsID && x.SectionID == TabID && x.ProfileID == UserID);
                        if (_person != null && unlike != null)
                        {
                            LikeCount = (_person.Likes == 0) ? 0 : --_person.Likes;
                        }
                        break;
                    }


                case (int)CommentSection.Editorial:
                    {
                        Editorial _editorial = dbcontext.Editorials.FirstOrDefault(a => a.EditorialID == PostsID);
                        unlike = dbcontext.Likes.FirstOrDefault(x => x.PostID == PostsID && x.SectionID == TabID && x.ProfileID == UserID);
                        if (_editorial != null && unlike != null)
                        {
                            LikeCount = (_editorial.Likes == 0) ? 0 : --_editorial.Likes;
                        }
                        break;
                    }

                case (int)Section.PersonalityDevelopment:
                    {
                        PersonalityDevlopment _personality = dbcontext.PersonalityDevlopments.FirstOrDefault(a => a.PDID == PostsID);
                        unlike = dbcontext.Likes.FirstOrDefault(x => x.PostID == PostsID && x.SectionID == TabID && x.ProfileID == UserID);
                        if (_personality != null && unlike != null)
                        {
                            LikeCount = (_personality.Likes == 0) ? 0 : --_personality.Likes;
                        }
                        break;
                    }

                default:
                    throw new InvalidOperationException("Couldn't process operation: " + TabID);
            }

            dbcontext.Likes.Remove(unlike);
            dbcontext.SaveChanges();

            return (int)LikeCount;
        }
    }
}