﻿using ShreeyaBlog.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ShreeyaBlog.Classes
{
    public enum Section : int
    {
        StudyMaterials = 1,
        ScienceTechnology,
        CurrentAffairs,
        TalentPlatform,
        LearnMore,
        PersonPersonality,
        Editorial,
        PersonalityDevelopment
    }
    public class UserDetail
    {
        public ShreeyaBlogEntities dbcontext = new ShreeyaBlogEntities();
        public virtual int ProfileID { get; set; }
        public virtual bool Active { get; set; }
        public virtual bool Exist { get; set; }
        public virtual int? NoofLikes { get; set; }
        public virtual int? NoofComments { get; set; }
        public virtual bool IsLiked { get; set; }
        public virtual List<CommentDetails> CommentsList { get; set; }
    }

    public class GetProfileID : UserDetail
    {
        public int _profileID;
        public GetProfileID(string Username)
        {
            var _pid = dbcontext.Profiles.Where(email => email.UserName == Username).Select(id => id.ProfileID).FirstOrDefault();
            this._profileID = _pid;
        }
    }

    public class GetActivity : UserDetail
    {
        public bool isActive;
        public GetActivity(int UID)
        {
            bool Active = dbcontext.Profiles.Where(email => email.ProfileID == UID && email.isActive == true).Any();
            this.isActive = Active;
        }
    }

    public class UserExistence : UserDetail
    {
        public bool isExist;
        public UserExistence(String Username)
        {
            Exist = dbcontext.Profiles.Where(e => e.UserName == Username).Any();
            this.isExist = Exist;
        }
        public UserExistence(int UID)
        {
            Exist = dbcontext.Profiles.Where(e => e.ProfileID == UID).Any();
            this.isExist = Exist;
        }
    }

    public class LikeCommentDetail : UserDetail
    {
        public int _profileID;
        public int? _noofLikes = 0, _noofComments = 0;
        public bool _isLiked, _isCommented;

        public LikeCommentDetail(int SectionID, int PostID, int? ProfileID)
        {
            switch (SectionID)
            {
                case (int)Section.StudyMaterials:
                    var StudyMaterials = dbcontext.StudyMaterialPosts.FirstOrDefault(a => a.StudyMaterialID == PostID);
                    if (StudyMaterials != null)
                    {
                        _noofLikes = (StudyMaterials.Likes == null) ? 0 : StudyMaterials.Likes;
                        _noofComments = (StudyMaterials.Comments == null) ? 0 : StudyMaterials.Comments;
                    }
                    break;

                case (int)Section.ScienceTechnology:
                    var ScienceTechnology = dbcontext.STPosts.FirstOrDefault(a => a.ID == PostID);
                    if (ScienceTechnology != null)
                    {
                        _noofLikes = (ScienceTechnology.Likes == null) ? 0 : ScienceTechnology.Likes;
                        _noofComments = (ScienceTechnology.Comments == null) ? 0 : ScienceTechnology.Comments;
                    }
                    break;

                case (int)Section.CurrentAffairs:
                    var CurrentAffairs = dbcontext.Posts.FirstOrDefault(a => a.PostId == PostID);
                    if (CurrentAffairs != null)
                    {
                        _noofLikes = (CurrentAffairs.Likes == null) ? 0 : CurrentAffairs.Likes;
                        _noofComments = (CurrentAffairs.Comments == null) ? 0 : CurrentAffairs.Comments;
                    }
                    break;

                case (int)Section.TalentPlatform:
                    var TalentPlatform = dbcontext.TalentPosts.FirstOrDefault(a => a.ID == PostID);
                    if (TalentPlatform != null)
                    {
                        _noofLikes = (TalentPlatform.Likes == null) ? 0 : TalentPlatform.Likes;
                        _noofComments = (TalentPlatform.Comments == null) ? 0 : TalentPlatform.Comments;
                    }
                    break;

                case (int)Section.LearnMore:
                    var LearnMore = dbcontext.ExamPosts.FirstOrDefault(a => a.ExamPostID == PostID);
                    if (LearnMore != null)
                    {
                        _noofLikes = (LearnMore.Likes == null) ? 0 : LearnMore.Likes;
                        _noofComments = (LearnMore.Comments == null) ? 0 : LearnMore.Comments;
                    }
                    break;

                case (int)Section.PersonPersonality:
                    var PersonPersonality = dbcontext.Person_Personality.FirstOrDefault(a => a.ID == PostID);
                    if (PersonPersonality != null)
                    {
                        _noofLikes = (PersonPersonality.Likes == null) ? 0 : PersonPersonality.Likes;
                        _noofComments = (PersonPersonality.Comments == null) ? 0 : PersonPersonality.Comments;
                    }
                    break;

                case (int)Section.Editorial:
                    var Editorial = dbcontext.Editorials.FirstOrDefault(a => a.EditorialID == PostID);
                    if (Editorial != null)
                    {
                        _noofLikes = (Editorial.Likes == null) ? 0 : Editorial.Likes;
                        _noofComments = (Editorial.Comments == null) ? 0 : Editorial.Comments;
                    }
                    break;

                case (int)Section.PersonalityDevelopment:
                    var PersonalityDevelopment = dbcontext.PersonalityDevlopments.FirstOrDefault(a => a.PDID == PostID);
                    if (PersonalityDevelopment != null)
                    {
                        _noofLikes = (PersonalityDevelopment.Likes == null) ? 0 : PersonalityDevelopment.Likes;
                        _noofComments = (PersonalityDevelopment.Comments == null) ? 0 : PersonalityDevelopment.Comments;
                    }
                    break;

                default:
                    _noofLikes = 0;
                    _noofComments = 0;
                    break;
            }

            if (ProfileID != null)
            {
                _isLiked = dbcontext.Likes.Where(likes => likes.ProfileID == ProfileID && likes.PostID == PostID && likes.SectionID == (int)SectionID).Any();
                _isCommented = dbcontext.Comments.Where(comments => comments.ProfileID == ProfileID && comments.PostID == PostID && comments.SectionID == (int)SectionID).Any();
            }
        }
    }

    public class SubcriptionDetail 
    {
        public ShreeyaBlogEntities dbcontext = new ShreeyaBlogEntities();
        public int EndDays;

        public SubcriptionDetail(int? Userid)
        {
            var RegUser = dbcontext.Profiles.Where(u => u.ProfileID == Userid).FirstOrDefault();

            DateTime ExpiryDate = Convert.ToDateTime(null);
            DateTime SubscribedDate = Convert.ToDateTime(RegUser.CreatedDate);
            var SubscriptionId = RegUser.SubscribedPlan;

            switch (SubscriptionId)
            {
                case 1:
                    ExpiryDate = SubscribedDate.AddMonths(1);
                    break;
                case 2:
                    ExpiryDate = SubscribedDate.AddMonths(3);
                    break;
                case 3:
                    ExpiryDate = SubscribedDate.AddMonths(6);
                    break;
                case 4:
                    ExpiryDate = SubscribedDate.AddMonths(12);
                    break;
                default:
                    ExpiryDate = SubscribedDate.AddMonths(0);
                    break;
            }
            EndDays = (ExpiryDate - DateTime.Now).Days;
        }
    }
}
