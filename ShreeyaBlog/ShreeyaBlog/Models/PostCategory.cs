﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web;

namespace ShreeyaBlog.Models
{
    public class PostCategory
    {
        public int PostId { get; set; }
        public int StudyMaterialID { get; set; }
        public int EditorialID { get; set; }

        public int CategoryId { get; set; }
        public string CategoryName { get; set; }

        public string Title { get; set; }
        public string ShortTitle
        {
            get
            {
                if (string.IsNullOrWhiteSpace(Title))
                    return string.Empty;

                var _title = Regex.Replace(Title, "<.*?>", String.Empty);

                if (string.IsNullOrWhiteSpace(_title))
                    return string.Empty;

                return _title.Length > 26 ? _title.Substring(0, 25) + "..." : _title;
            }
        }

        public string Slug { get; set; }

        public string Description { get; set; }
        public string LineDescription
        {
            get
            {
                if (string.IsNullOrWhiteSpace(Description))
                    return string.Empty;

                var desc = Regex.Replace(Description, "<.*?>", String.Empty);

                if (string.IsNullOrWhiteSpace(desc))
                    return string.Empty;

                return desc.Length > 31 ? desc.Substring(0, 30) + "..." : desc;
            }
        }
        public string ShortDescription
        {
            get
            {
                if (string.IsNullOrWhiteSpace(Description))
                    return string.Empty;

                var desc = Regex.Replace(Description, "<.*?>", String.Empty);

                if (string.IsNullOrWhiteSpace(desc))
                    return string.Empty;

                return desc.Length > 201 ? desc.Substring(0, 200) + "..." : desc;
            }
        }
        public string IndexDescription
        {
            get
            {
                if (string.IsNullOrWhiteSpace(Description))
                    return string.Empty;

                var desc = Regex.Replace(Description, "<.*?>", String.Empty);

                if (string.IsNullOrWhiteSpace(desc))
                    return string.Empty;

                return desc.Length > 201 ? desc.Substring(0, 260) + "..." : desc;
            }
        }

        public string EditorialDescription
        {
            get
            {
                if (string.IsNullOrWhiteSpace(Description))
                    return string.Empty;

                var desc = Regex.Replace(Description, "<.*?>", String.Empty);

                if (string.IsNullOrWhiteSpace(desc))
                    return string.Empty;

                return desc.Length > 501 ? desc.Substring(0, 610) + "..." : desc;
            }
        }


        public string SmallDescription
        {
            get
            {
                if (string.IsNullOrWhiteSpace(Description))
                    return string.Empty;

                var desc = Regex.Replace(Description, "<.*?>", String.Empty);

                if (string.IsNullOrWhiteSpace(desc))
                    return string.Empty;

                return desc.Length > 101 ? desc.Substring(0, 100) + "..." : desc;
            }
        }
        public string CardDescription
        {
            get
            {
                if (string.IsNullOrWhiteSpace(Description))
                    return string.Empty;

                var desc = Regex.Replace(Description, "<.*?>", String.Empty);

                if (string.IsNullOrWhiteSpace(desc))
                    return string.Empty;

                return desc.Length > 151 ? desc.Substring(0, 150) + "..." : desc;
            }
        }

        public string DefaultImage { get; set; }

        public string Attachment { get; set; }

        public string EditorialImage { get; set; }

        public int? Likes { get; set; }
        public int? Comments { get; set; }

        public bool IsPublished { get; set; }
        public bool IsDeleted { get; set; }

        public bool isLiked { get; set; }
        public Nullable<System.DateTime> PublishedDate { get; set; }
        public Nullable<System.DateTime> CreatedDate { get; set; }
        public Nullable<System.DateTime> ModifyDate { get; set; }
        public virtual Category Category { get; set; }
    }

    public class TitlesModel
    {
        public int PostId { get; set; }
        public int CategoryId { get; set; }
        public string CategoryName { get; set; }
        public string Title { get; set; }
        public string Description { get; set; }
        public string DefaultImage { get; set; }


        //public string ShortTitle
        //{
        //    get
        //    {
        //        if (string.IsNullOrWhiteSpace(Title))
        //            return string.Empty;

        //        var _title = Regex.Replace(Title, "<.*?>", String.Empty);

        //        if (string.IsNullOrWhiteSpace(_title))
        //            return string.Empty;

        //        return _title.Length > 26 ? _title.Substring(0, 25) + "..." : _title;
        //    }
        //}
        public Nullable<System.DateTime> CreatedDate { get; set; }
    }

    public class CommentDetails
    {
        public int? CommentID { get; set; }
        public int? PostId { get; set; }
        public string Image { get; set; }
        public int ProfileID { get; set; }
        public string Comments { get; set; }
        public string UserName { get; set; }
        public string FirstName { get; set; }
        public int ReplyID { get; set; }
        public string Reply { get; set; }
        public Nullable<System.DateTime> CreatedDate { get; set; }
        public Nullable<System.DateTime> ModifiedDate { get; set; }

        public ICollection<CommentDetails> Replies { get; set; }


        public static implicit operator List<object>(CommentDetails v)
        {
            throw new NotImplementedException();
        }
    }

    public class Contact_info
    {
        public string Address { get; set; }
        public string PhoneNo { get; set; }
        public string Email { get; set; }
        public string WebSite { get; set; }
    }

    public class Liked_posts
    {
       public int? ID { get; set; }
        public int?         TabID { get; set; }
        public string      Category { get; set; }
        public string       DefaultImage { get; set; }
        public string     PostTitle { get; set; }
        public int      CategoryID { get; set; }
        public Nullable<System.DateTime> LikedOn { get; set; }
     
    }

    public class StudyMaterialCategory
    {
        public int StudyMaterialID { get; set; }
        public int StudyID { get; set; }
        public string StudyMaterialName { get; set; }
        public string Title { get; set; }
        public string Description { get; set; }
        public string ShortDescription
        {
            get
            {
                if (string.IsNullOrWhiteSpace(Description))
                    return string.Empty;

                var desc = Regex.Replace(Description, "<.*?>", String.Empty);

                if (string.IsNullOrWhiteSpace(desc))
                    return string.Empty;

                return desc.Length > 201 ? desc.Substring(0, 200) + "..." : desc;
            }
        }

        public string SmallDescription
        {
            get
            {
                if (string.IsNullOrWhiteSpace(Description))
                    return string.Empty;

                var desc = Regex.Replace(Description, "<.*?>", String.Empty);

                if (string.IsNullOrWhiteSpace(desc))
                    return string.Empty;

                return desc.Length > 101 ? desc.Substring(0, 100) + "..." : desc;
            }
        }

        public string LineDescription
        {
            get
            {
                if (string.IsNullOrWhiteSpace(Description))
                    return string.Empty;

                var desc = Regex.Replace(Description, "<.*?>", String.Empty);

                if (string.IsNullOrWhiteSpace(desc))
                    return string.Empty;

                return desc.Length > 31 ? desc.Substring(0, 30) + "..." : desc;
            }
        }
        public string DefaultImage { get; set; }
        public int? Likes { get; set; }

        public int? Comments { get; set; }
        public bool IsPublished { get; set; }
        public Nullable<System.DateTime> CreatedDate { get; set; }
        public Nullable<System.DateTime> ModifiedDate { get; set; }
        public bool IsDeleted { get; set; }

        public bool isLiked { get; set; }
        public virtual StudyMaterial StudyMaterial { get; set; }
    }

    public class ExamPostModel
    {
        public int ExamPostID { get; set; }
        public int ExamID { get; set; }
        public string ExamName { get; set; }
        public string Title { get; set; }
        public string Description { get; set; }
        public string ShortDescription
        {
            get
            {
                if (string.IsNullOrWhiteSpace(Description))
                    return string.Empty;

                var desc = Regex.Replace(Description, "<.*?>", String.Empty);

                if (string.IsNullOrWhiteSpace(desc))
                    return string.Empty;

                return desc.Length > 201 ? desc.Substring(0, 200) + "..." : desc;
            }
        }

        public string SmallDescription
        {
            get
            {
                if (string.IsNullOrWhiteSpace(Description))
                    return string.Empty;

                var desc = Regex.Replace(Description, "<.*?>", String.Empty);

                if (string.IsNullOrWhiteSpace(desc))
                    return string.Empty;

                return desc.Length > 101 ? desc.Substring(0, 100) + "..." : desc;
            }
        }

        public string LineDescription
        {
            get
            {
                if (string.IsNullOrWhiteSpace(Description))
                    return string.Empty;

                var desc = Regex.Replace(Description, "<.*?>", String.Empty);

                if (string.IsNullOrWhiteSpace(desc))
                    return string.Empty;

                return desc.Length > 31 ? desc.Substring(0, 30) + "..." : desc;
            }
        }
        public string DefaultImage { get; set; }
        public int? Likes { get; set; }

        public int? Comments { get; set; }
        public bool IsPublished { get; set; }
        public Nullable<System.DateTime> CreatedDate { get; set; }
        public Nullable<System.DateTime> ModifiedDate { get; set; }
        public bool IsDeleted { get; set; }

        public bool isLiked { get; set; }
        public virtual Exam Exam { get; set; }
    }

    public class UserProfile
    {
        public int ProfileID { get; set; }
        public string UserName { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Qualification { get; set; }
        public string About { get; set; }
        public Nullable<System.DateTime> DOB { get; set; }
        public string AspiringExams { get; set; }
        public string Aim { get; set; }
        public string MobileNo { get; set; }
        public string Address { get; set; }
        public int SubscribedPlan { get; set; }
        public string SubscribedTest { get; set; }
        public Nullable<bool> isActive { get; set; }
        public Nullable<System.DateTime> CreatedDate { get; set; }
        public Nullable<System.DateTime> ModifiedDate { get; set; }
        public string Image { get; set; }
        public string dateString { get; set; }
        public Nullable<System.DateTime> SubscribedDate
        {
            get
            {
                DateTime _subscribedDate = Convert.ToDateTime(CreatedDate);
                if (!CreatedDate.HasValue)
                    return DateTime.Now;
                return _subscribedDate;
            }
        }

        public Nullable<System.DateTime> ExpiryDate
        {
            get
            {
                DateTime _subscribedDate = Convert.ToDateTime(CreatedDate);
                DateTime ExpiryDate = Convert.ToDateTime(null);
                var SubscriptionID = SubscribedPlan;
                switch (SubscriptionID)
                {
                    case 1:
                        ExpiryDate = _subscribedDate.AddMonths(1);
                        break;
                    case 2:
                        ExpiryDate = _subscribedDate.AddMonths(3);
                        break;
                    case 3:
                        ExpiryDate = _subscribedDate.AddMonths(6);
                        break;
                    case 4:
                        ExpiryDate = _subscribedDate.AddMonths(12);
                        break;
                    default:
                        ExpiryDate = _subscribedDate.AddMonths(0);
                        break;
                }
                return ExpiryDate;
            }
        }

    }
}