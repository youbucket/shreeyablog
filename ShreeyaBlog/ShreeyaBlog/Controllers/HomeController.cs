﻿using ShreeyaBlog.Classes;
using ShreeyaBlog.Models;
using System;
using System.IO;
using System.Linq;
using System.Net.Mail;
using System.Web;
using System.Web.Mvc;

namespace ShreeyaBlog.Controllers
{
    public class HomeController : Controller
    {
        ShreeyaBlogEntities dbcontext = new ShreeyaBlogEntities();

        public ActionResult Index(Post model)
        {
            DerivedIndex objectindex = new DerivedIndex();

            //Quote
            ViewBag.Greetings = objectindex.greeting();

            //Parallax 
            ViewBag.Parallax = objectindex.parallax();

            //editorial - get latest editorial
            ViewBag.getEditorial = objectindex.editorial();

            //Personality Section
            ViewBag.PersonalDevelopment = objectindex.personalDevelopment();

            //NewNotification Section
            ViewBag.NewNotification = objectindex.notification();

            //Images
            ViewBag.adds = objectindex.advertiseImage();



            //Videos
            ViewBag.videos = objectindex.GetVideos();

            //Latest wrt categories- get latest post(1) from all the categories
            ViewBag.LatestNews = objectindex.latestCategories();

            //Latest - get latest post(9) irrespective of category
            //ViewBag.LatestLiveBag = objectindex.latestPost();

            //Categorywise Posts Begin
            //Category

            ////Listing Category Names
            //var CategoryNames = dbcontext.Categories.Where(cat => cat.isActive == true).Select(category => category.CategoryName).ToArray();
            ////Listing Category IDs
            //var CategoryIDs = dbcontext.Categories.Where(cat => cat.isActive == true).Select(category => category.CategoryId).ToArray();

            ////Category 1 - get posts(6)
            //ViewBag.CategoryHeadingBag1 = CategoryNames.ElementAt(0);
            //ViewBag.category_oneBag = objectindex.categoryListTake6(CategoryIDs.ElementAt(0));

            ////Category 2 - get posts(3) from Category 3 [skip Category 1]
            //ViewBag.CategoryHeadingBag2 = CategoryNames.ElementAt(1);
            //ViewBag.category_twoBag = objectindex.categoryListTake3(CategoryIDs.ElementAt(1));

            ////Category 3 - get posts(3) from Category 3 [skip Category 1,Category 2]           
            //ViewBag.CategoryHeadingBag3 = CategoryNames.ElementAt(2);
            //ViewBag.category_threeBag = objectindex.categoryListTake3(CategoryIDs.ElementAt(2));

            ////Category 4 - get posts(3) from Category 4 [skip Category 1,Category 2,Category 3]
            //ViewBag.CategoryHeadingBag4 = CategoryNames.ElementAt(3);
            //ViewBag.category_fourBag = objectindex.categoryListTake3(CategoryIDs.ElementAt(3));

            ////Category 5 - get posts(3) from Category 5 [skip Category 1,Category 2,Category 3,Category 4]
            //ViewBag.CategoryHeadingBag5 = CategoryNames.ElementAt(4);
            //ViewBag.category_fiveBag = objectindex.categoryListTake3(CategoryIDs.ElementAt(4));

            ////Category 6 - get posts(3) from Category 6 [skip Category 1,Category 2,Category 3,Category 4,Category 5]
            //ViewBag.CategoryHeadingBag6 = CategoryNames.ElementAt(5);
            //ViewBag.category_sixBag = objectindex.categoryListTake3(CategoryIDs.ElementAt(5));

            ////Category 7 - get posts(3) from Category 7 [skip Category 1,Category 2,Category 3,Category 4,Category 5,Category 6]
            //ViewBag.CategoryHeadingBag7 = CategoryNames.ElementAt(6);
            //ViewBag.category_sevenBag = objectindex.categoryListTake3(CategoryIDs.ElementAt(6));

            ////Category 8 - get posts(3) from Category 8 [skip Category 1,Category 2,Category 3,Category 4,Category 5,Category 6,Category 7]
            //ViewBag.CategoryHeadingBag8 = CategoryNames.ElementAt(7);
            //ViewBag.category_eightBag = objectindex.categoryListTake3(CategoryIDs.ElementAt(7));

            ////Category 9 - get posts(2) from Category 9 [skip Category 1,Category 2,Category 3,Category 4,Category 5,Category 6,Category 7,Category 8]           
            //ViewBag.CategoryHeadingBag9 = CategoryNames.ElementAt(8);
            //ViewBag.category_nineBag = objectindex.categoryListTake2(CategoryIDs.ElementAt(8));

            ////Category 10 - get posts(2) from Category 10 [skip Category 1,Category 2,Category 3,Category 4,Category 5,Category 6,Category 7,Category 8,Category 9]
            //ViewBag.CategoryHeadingBag10 = CategoryNames.ElementAt(9);
            //ViewBag.category_tenBag = objectindex.categoryListTake2(CategoryIDs.ElementAt(9));

            ////Category 11 - get posts(6) from Category 11 [skip Category 1,Category 2,Category 3,Category 4,Category 5,Category 6,Category 7,Category 8,Category 9,Category 10]
            //ViewBag.CategoryHeadingBagGK = CategoryNames.ElementAt(10);
            //ViewBag.category_GKBag = objectindex.categoryListTake6(CategoryIDs.ElementAt(10));

            //Categorywise Posts End
            return View();
        }

        public ActionResult AboutUs()
        {
            return View();
        }

        [HttpGet]
        public ActionResult ContactUS()
        {
            return View();
        }

        [HttpPost]
        public ActionResult ContactUS(string firstname, string lastname, string email, string phone, string message)
        {
            SmtpClient smtp1;
            string from = "technical@shlrtechnosoft.in";
            string toEmail = "sjsmag2019@gmail.com";

            using (MailMessage mail = new MailMessage())
            {
                mail.From = new MailAddress(from);
                mail.To.Add(toEmail);

                mail.Subject = "Contact";

                mail.Body = "Hello " + toEmail + Environment.NewLine +
                    "You Got A New Enquiry..!" + Environment.NewLine + Environment.NewLine +
                    "Name : " + firstname + Environment.NewLine +
                    "Last Name : " + lastname + Environment.NewLine +
                    "Email : " + email + Environment.NewLine +
                    "Phone : " + phone + Environment.NewLine +
                    "Message : " + message + Environment.NewLine;

                mail.IsBodyHtml = false;

                smtp1 = new SmtpClient();
                smtp1.Host = "relay-hosting.secureserver.net";
                //smtp.Host = "smtp.gmail.com";
                smtp1.Port = 25;

                smtp1.Credentials = new System.Net.NetworkCredential
                ("technical@shlrtechnosoft.in", "Technical@123");

                smtp1.EnableSsl = false;
                smtp1.Send(mail);
            }
            Response.Write("<script>alert('Data Submitted Successfully...Thank you!')</script>");
            return View();
        }

        [HttpGet]
        public ActionResult Careers()
        {
            var list = dbcontext.Careers.Where(job => job.ID != 0).ToList();
            ViewBag.list = list;
            return View();
        }

        [HttpPost]
        public ActionResult Careers(string firstname, string lastname, string email, string phone, HttpPostedFileBase FileUpload)
        {
            Careers();
            SmtpClient smtp1;
            string from = "technical@shlrtechnosoft.in";
            string toEmail = "ser9414@gmail.com";

            using (MailMessage mail = new MailMessage())
            {
                mail.From = new MailAddress(from);
                mail.To.Add(toEmail);

                mail.Subject = "Career";

                mail.Body = "Hello " + toEmail + Environment.NewLine +
                    "You Got A New Enquiry..!" + Environment.NewLine + Environment.NewLine +
                    "First Name : " + firstname + Environment.NewLine +
                    "Last Name : " + lastname + Environment.NewLine +
                    "Email : " + email + Environment.NewLine +
                    "Phone : " + phone + Environment.NewLine;

                if (FileUpload != null)
                {
                    string fileName = Path.GetFileName(FileUpload.FileName);
                    mail.Attachments.Add(new Attachment(FileUpload.InputStream, fileName));
                }
                mail.IsBodyHtml = false;

                smtp1 = new SmtpClient();
                smtp1.Host = "relay-hosting.secureserver.net";
                //smtp.Host = "smtp.gmail.com";
                smtp1.Port = 25;

                smtp1.Credentials = new System.Net.NetworkCredential
                ("technical@shlrtechnosoft.in", "Technical@123");

                smtp1.EnableSsl = false;
                smtp1.Send(mail);
            }
            TempData["msg"] = "<script>alert('Data Submitted Successfully...Thank you!');</script>";
            return View();
        }

        [HttpGet]
        public ActionResult InActive()
        {
            return View();
        }

        [HttpGet]
        public ActionResult OnlineTest()
        {
            return View();
        }
    }
}

