﻿using ShreeyaBlog.Classes;
using ShreeyaBlog.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity.Migrations;
using System.Data.Entity.Validation;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;

namespace ShreeyaBlog.Controllers
{
    public class AdministratorController : Controller
    {
        public ShreeyaBlogEntities dbcontext = new ShreeyaBlogEntities();

        // GET: Administrator
        public ActionResult Index()
        {
            return View();
        }

        ///////////////////////////////////////////////////////////////////////////////////////////////
        ////////////////////////////                                ///////////////////////////////////
        ///////////////////////////  Parallax Section Start         ///////////////////////////////////
        ///////////////////////////                                 ///////////////////////////////////
        ///////////////////////////////////////////////////////////////////////////////////////////////

        [HttpGet]
        [Authorize(Roles = "Admin")]
        public ActionResult ParallaxAdd()
        {
            return View();
        }

        [HttpPost]
        public ActionResult ParallaxAdd(Parallax parallax, HttpPostedFileBase FileUpload, string CategoryID)
        {
            string filesnames = "";
            if (FileUpload != null)
            {
                string savedFileName = DateTime.Now.ToString("MMddHHmmssfff") + Path.GetFileName(FileUpload.FileName);
                FileUpload.SaveAs(Path.Combine(Server.MapPath("~/Content/Uploads/Parallax"), savedFileName)); // Save the file
                filesnames = savedFileName;

                Parallax insert = new Parallax()
                {
                    Name = parallax.Name,
                    Category = CategoryID,
                    Image = filesnames,
                    Active = parallax.Active,
                    CreatedDate = DateTime.Now
                };
                dbcontext.Parallaxes.Add(insert);
                dbcontext.SaveChanges();
            }
            TempData["msg"] = "<script>alert('Added Successfully');</script>";
            return RedirectToAction("ParallaxList", "Administrator");
        }

        [HttpGet]
        public ActionResult ParallaxList()
        {
            var list = dbcontext.Parallaxes.Where(s => s.ID != 0).OrderBy(c => c.CreatedDate).ToList();
            return View(list);
        }

        [HttpGet]
        public ActionResult ParallaxPreview(int? ParallaxID)
        {
            var Parallax = dbcontext.Parallaxes.Where(i => i.ID == ParallaxID).Select(i => i.Image).FirstOrDefault();
            ViewBag.ParallaxImage = Parallax;
            return View();
        }

        public ActionResult ActivateParallax(int? ParallaxID)
        {
            if (ModelState.IsValid)
            {
                Parallax update = dbcontext.Parallaxes.Find(ParallaxID);
                if (update.ID == ParallaxID && update.Active != true)
                {
                    update.Active = true;
                }
                else
                {
                    update.Active = false;
                }
                dbcontext.Parallaxes.AddOrUpdate(update);
                int c = dbcontext.SaveChanges();
            }
            TempData["msg"] = "<script>alert('Updated Successfully');</script>";
            return RedirectToAction("ParallaxList", "Administrator");
        }

        public async Task<ActionResult> ParallaxDelete(int? ParallaxID)
        {
            if (ParallaxID == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Parallax parallax = await dbcontext.Parallaxes.FindAsync(ParallaxID);
            {
                var filename = dbcontext.Parallaxes.Where(p => p.ID == ParallaxID).Select(i => i.Image).FirstOrDefault();
                var filePath = Server.MapPath("~/Content/Uploads/Parallax/" + filename);
                if (System.IO.File.Exists(filePath))
                {
                    System.IO.File.Delete(filePath);
                }
                dbcontext.Parallaxes.Remove(parallax);
                int c = dbcontext.SaveChanges();
            }
            if (parallax == null)
            {
                return HttpNotFound();
            }
            TempData["msg"] = "<script>alert('Deleted Successfully');</script>";
            return RedirectToAction("ParallaxList", "Administrator");
        }

        ///////////////////////////////////////////////////////////////////////////////////////////////
        ////////////////////////////                                ///////////////////////////////////
        ///////////////////////////  Parallax Section End         /////////////////////////////////////
        ///////////////////////////                                 ///////////////////////////////////
        ///////////////////////////////////////////////////////////////////////////////////////////////



        [HttpGet]
        public ActionResult AddsAdd()
        {
            return View();
        }

        [HttpPost]
        public ActionResult AddsAdd(Add _adds, HttpPostedFileBase FileUpload)
        {
            string filesnames = "";
            if (FileUpload != null)
            {
                string savedFileName = DateTime.Now.ToString("MMddHHmmssfff") + Path.GetFileName(FileUpload.FileName);
                FileUpload.SaveAs(Path.Combine(Server.MapPath("~/Content/Uploads/Adds"), savedFileName)); // Save the file
                filesnames = savedFileName;

                Add insert = new Add()
                {
                    Name = _adds.Name,
                    Image = filesnames,
                    Active = _adds.Active,
                    CreatedDate = DateTime.Now
                };
                dbcontext.Adds.Add(insert);
                dbcontext.SaveChanges();
            }
            TempData["msg"] = "<script>alert('Added Successfully');</script>";
            return RedirectToAction("AddsList", "Administrator");
        }

        [HttpGet]
        [Authorize(Roles = "Admin")]
        public ActionResult AddsList()
        {
            var list = dbcontext.Adds.Where(s => s.ID != 0).OrderBy(c => c.CreatedDate).ToList();
            return View(list);
        }


        public ActionResult AddsActivate(int? addID)
        {
            if (ModelState.IsValid)
            {
                Add update = dbcontext.Adds.Find(addID);
                if (update.ID == addID && update.Active != true)
                {
                    update.Active = true;
                }
                else
                {
                    update.Active = false;
                }
                dbcontext.Adds.AddOrUpdate(update);
                int c = dbcontext.SaveChanges();
            }
            TempData["msg"] = "<script>alert('Updated Successfully');</script>";
            return RedirectToAction("AddsList", "Administrator");
        }

        public async Task<ActionResult> AddsDelete(int? addID)
        {
            if (addID == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Add parallax = await dbcontext.Adds.FindAsync(addID);
            {
                var filename = dbcontext.Adds.Where(p => p.ID == addID).Select(i => i.Image).FirstOrDefault();
                string filePath = Server.MapPath("~/Content/Uploads/Adds/" + filename);
                if (System.IO.File.Exists(filePath))
                {
                    System.IO.File.Delete(filePath);
                }
                dbcontext.Adds.Remove(parallax);
                int c = dbcontext.SaveChanges();
            }
            if (parallax == null)
            {
                return HttpNotFound();
            }
            TempData["msg"] = "<script>alert('Deleted Successfully');</script>";
            return RedirectToAction("AddsList", "Administrator");
        }



        ///////////////////////////////////////////////////////////////////////////////////////////////
        ////////////////////////////                                ///////////////////////////////////
        ///////////////////////////  AdvertiseImage Section Start         ///////////////////////////////////
        ///////////////////////////                                 ///////////////////////////////////
        ///////////////////////////////////////////////////////////////////////////////////////////////

        [HttpGet]
        public ActionResult ImageAdd()
        {
            return View();
        }

        [HttpPost]
        public ActionResult ImageAdd(AdvertiseImage img, HttpPostedFileBase FileUpload)
        {
            string filesnames = "";
            if (FileUpload != null)
            {
                string savedFileName = DateTime.Now.ToString("MMddHHmmssfff") + Path.GetFileName(FileUpload.FileName);
                FileUpload.SaveAs(Path.Combine(Server.MapPath("~/Content/Uploads/Advertise"), savedFileName)); // Save the file
                filesnames = savedFileName;

                AdvertiseImage insert = new AdvertiseImage()
                {
                    Title = img.Title,
                    Image = filesnames,
                    Status = img.Status,
                    CreatedDate = DateTime.Now
                };
                dbcontext.AdvertiseImages.Add(insert);
                dbcontext.SaveChanges();
            }
            TempData["msg"] = "<script>alert('Added Successfully');</script>";
            return RedirectToAction("ImageList", "Administrator");
        }

        [HttpGet]
        public ActionResult ImageList()
        {
            var list = dbcontext.AdvertiseImages.Where(s => s.AddImgId != 0).OrderBy(c => c.CreatedDate).ToList();
            return View(list);
        }

        public ActionResult ImageActivate(int? ImgID)
        {
            if (ModelState.IsValid)
            {
                AdvertiseImage update = dbcontext.AdvertiseImages.Find(ImgID);
                if (update.AddImgId == ImgID && update.Status != true)
                {
                    update.Status = true;
                }
                else
                {
                    update.Status = false;
                }
                dbcontext.AdvertiseImages.AddOrUpdate(update);
                int c = dbcontext.SaveChanges();
            }
            TempData["msg"] = "<script>alert('Updated Successfully');</script>";
            return RedirectToAction("ImageList", "Administrator");
        }

        public async Task<ActionResult> ImageDelete(int? ImgID)
        {
            if (ImgID == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            AdvertiseImage img = await dbcontext.AdvertiseImages.FindAsync(ImgID);
            {
                var filename = dbcontext.AdvertiseImages.Where(p => p.AddImgId == ImgID).Select(i => i.Image).FirstOrDefault();
                var filePath = Server.MapPath("~/Content/Uploads/Advertise/" + filename);
                if (System.IO.File.Exists(filePath))
                {
                    System.IO.File.Delete(filePath);
                }
                dbcontext.AdvertiseImages.Remove(img);
                int c = dbcontext.SaveChanges();
            }
            if (img == null)
            {
                return HttpNotFound();
            }
            TempData["msg"] = "<script>alert('Deleted Successfully');</script>";
            return RedirectToAction("ImageList", "Administrator");
        }


        ///////////////////////////////////////////////////////////////////////////////////////////////
        ////////////////////////////                                ///////////////////////////////////
        ///////////////////////////  AdvertiseImage Section End         /////////////////////////////////////
        ///////////////////////////                                 ///////////////////////////////////
        ///////////////////////////////////////////////////////////////////////////////////////////////





        ///////////////////////////////////////////////////////////////////////////////////////////////
        ////////////////////////////                                ///////////////////////////////////
        ///////////////////////////  Current Affair Category start        /////////////////////////////
        ///////////////////////////                                 ///////////////////////////////////
        ///////////////////////////////////////////////////////////////////////////////////////////////

        public void BindCat()
        {
            List<Category> category = dbcontext.Categories.Where(x => x.CategoryId != 0 && x.isActive == true).ToList();
            Category c = new Category();
            c.CategoryName = "Root";
            c.CategoryId = 0;
            category.Insert(0, c);
            SelectList categorydata = new SelectList(category, "CategoryId", "CategoryName", 0);
            ViewBag.Categories = categorydata;
        }

        [HttpGet]
        [Authorize(Roles = "Admin")]
        public ActionResult CurrentAffair()
        {
            BindCat();
            return View();
        }

        [HttpPost]
        public ActionResult CurrentAffair(Category model, HttpPostedFileBase FileUpload)
        {
            BindCat();
            string filesnames = "test.png";

            if (FileUpload != null)
            {
                if (!System.IO.Directory.Exists(Server.MapPath("\\Content\\Uploads\\Tab\\SubTab" + 1)))
                {
                    string savedFileName = Path.GetFileName(FileUpload.FileName);
                    FileUpload.SaveAs(Path.Combine(Server.MapPath("~/Content/Uploads/Tab/SubTab"), savedFileName)); // Save the file
                    filesnames = savedFileName;
                }
            }
            Category insert = new Category()
            {
                CategoryName = model.CategoryName,
                isActive = model.isActive,
                ParentID = model.ParentID == 0 ? null : model.ParentID,
                IconImage = "SubTab/" + filesnames,
                CreatedDate = DateTime.Now
            };
            dbcontext.Categories.Add(insert);
            dbcontext.SaveChanges();
            TempData["msg"] = "<script>alert('Added Successfully');</script>";
            return RedirectToAction("CurrentAffairList", "Administrator");
        }

        [HttpGet]
        public ActionResult CurrentAffairList(Category model)
        {
            var listCat = dbcontext.Categories.Where(p => p.CategoryId != 0).ToList();
            return View(listCat);
        }

        public async Task<ActionResult> CurrentAffairEdit(int? catID)
        {
            BindCat();
            if (catID == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Category cate = await dbcontext.Categories.FindAsync(catID);
            if (cate == null)
            {
                return HttpNotFound();
            }
            return View(cate);
        }

        [HttpPost]
        public ActionResult CurrentAffairEdit(Category model, HttpPostedFileBase FileUpload)
        {
            BindCat();
            string filesnames = dbcontext.Categories.Where(i => i.CategoryId == model.CategoryId).Select(i => i.IconImage).FirstOrDefault();
            filesnames = filesnames == null ? "test.png" : filesnames;

            if (FileUpload != null)
            {
                if (!System.IO.Directory.Exists(Server.MapPath("\\Content\\Uploads\\Tab\\SubTab" + 1)))
                {
                    string savedFileName = Path.GetFileName(FileUpload.FileName);
                    FileUpload.SaveAs(Path.Combine(Server.MapPath("~/Content/Uploads/Tab/SubTab"), savedFileName)); // Save the file
                    filesnames = savedFileName;
                }
            }
            Category updatecate = dbcontext.Categories.Find(model.CategoryId);
            if (updatecate.CategoryId == model.CategoryId)
            {
                updatecate.CategoryName = model.CategoryName;
                updatecate.ParentID = model.ParentID == 0 ? null : model.ParentID;
                updatecate.isActive = model.isActive;
                updatecate.IconImage = "SubTab/" + filesnames;
                updatecate.ModifiedDate = DateTime.Now;
            }
            dbcontext.Categories.AddOrUpdate(updatecate);
            int c = dbcontext.SaveChanges();
            TempData["msg"] = "<script>alert('Updated Successfully');</script>";
            return RedirectToAction("CurrentAffairList", "Administrator");
        }

        public async Task<ActionResult> CurrentAffairActivate(int? catID)
        {
            if (catID == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Category update = await dbcontext.Categories.FindAsync(catID);
            if (update.CategoryId == catID && update.isActive != true)
            {
                update.isActive = true;
            }
            else
            {
                update.isActive = false;
            }
            dbcontext.Categories.AddOrUpdate(update);
            dbcontext.SaveChanges();
            TempData["msg"] = "<script>alert('Updated Successfully');</script>";
            return RedirectToAction("CurrentAffairList", "Administrator");
        }

        [HttpGet]
        public ActionResult CurrentAffairDelete(Category model, int? catID)
        {
            var associatedPosts = (from cat in dbcontext.Categories
                                   join catpost in dbcontext.Posts on cat.CategoryId equals catpost.CategoryId
                                   where cat.CategoryId == catID
                                   select new PostCategory
                                   {
                                       PostId = catpost.PostId,
                                       CategoryName = cat.CategoryName,
                                       Title = catpost.Title,
                                       DefaultImage = catpost.DefaultImage,
                                       CreatedDate = catpost.PublishedDate,
                                   }).OrderBy(c => c.CreatedDate).ToList();
            return View(associatedPosts);
        }

        [HttpPost]
        public ActionResult CurrentAffairDelete(int? catID)
        {
            Category _parent = dbcontext.Categories.Find(catID);
            var _child = dbcontext.Posts.Where(category => category.CategoryId == catID);
            if (_child != null)
            {
                dbcontext.Posts.RemoveRange(_child);
                dbcontext.Categories.Remove(_parent);
            }
            else
            {
                dbcontext.Categories.Remove(_parent);
            }
            dbcontext.SaveChanges();
            TempData["msg"] = "<script>alert('Deleted Successfully');</script>";
            return RedirectToAction("CurrentAffairList", "Administrator");
        }

        ///////////////////////////////////////////////////////////////////////////////////////////////
        ////////////////////////////                                ///////////////////////////////////
        ///////////////////////////  Current Affair Category end        ///////////////////////////////
        ///////////////////////////                                 ///////////////////////////////////
        ///////////////////////////////////////////////////////////////////////////////////////////////




        ///////////////////////////////////////////////////////////////////////////////////////////////
        ////////////////////////////                                ///////////////////////////////////
        ///////////////////////////  Current Affair Post Start        /////////////////////////////////
        ///////////////////////////                                 ///////////////////////////////////
        ///////////////////////////////////////////////////////////////////////////////////////////////

        public void BindCategory()
        {
            List<Category> category = dbcontext.Categories.Where(x => x.CategoryId != 0 && x.isActive == true).ToList();
            Category c = new Category();
            c.CategoryName = "--Select Category--";
            c.CategoryId = 0;
            category.Insert(0, c);
            SelectList categorydata = new SelectList(category, "CategoryId", "CategoryName", 0);
            ViewBag.Categories = categorydata;
        }

        [HttpGet]
        [Authorize(Roles = "Admin")]
        public ActionResult CurrentAffairPost()
        {
            BindCategory();
            return View();
        }

        [HttpPost, ValidateInput(false)]
        public ActionResult CurrentAffairPost(Post model, HttpPostedFileBase FileUpload)
        {
            BindCategory();
            string filesnames = "default.jpg";

            if (FileUpload != null)
            {
                if (!System.IO.Directory.Exists(Server.MapPath("\\Content\\Uploads\\Posts" + 1)))
                {
                    string savedFileName = DateTime.Now.ToString("MMddHHmmssfff") + Path.GetFileName(FileUpload.FileName);
                    FileUpload.SaveAs(Path.Combine(Server.MapPath("~/Content/Uploads/Posts"), savedFileName)); // Save the file
                    filesnames = savedFileName;
                }
            }
            Post insert = new Post()
            {
                Title = model.Title,
                CategoryId = model.CategoryId,
                //Slug = model.Slug,
                Description = model.Description,
                DefaultImage = filesnames,
                IsPublished = true,
                IsDeleted = false,
                Likes = 0,
                Comments = 0,
                PublishedDate = DateTime.Now,
            };
            dbcontext.Posts.Add(insert);
            int c = dbcontext.SaveChanges();
            TempData["msg"] = "<script>alert('Added Successfully');</script>";
            return RedirectToAction("CurrentAffairPostList", "Administrator");
        }

        //[Authorize(Roles = "Admin")]
        [HttpGet]
        public ActionResult CurrentAffairPostList()
        {
            var list = (from postdata in dbcontext.Posts
                        join catdata in dbcontext.Categories on postdata.CategoryId equals catdata.CategoryId
                        where postdata.IsDeleted != true
                        select new PostCategory
                        {
                            PostId = postdata.PostId,
                            CategoryId = postdata.CategoryId,
                            CategoryName = catdata.CategoryName,
                            Title = postdata.Title,
                            Slug = postdata.Slug,
                            Description = postdata.Description,
                            DefaultImage = postdata.DefaultImage,
                            IsPublished = postdata.IsPublished,
                            PublishedDate = postdata.PublishedDate,
                            ModifyDate = postdata.ModifyDate,
                        }).OrderByDescending(x => x.PublishedDate).ToList<PostCategory>();
            return View(list);
        }

        [HttpGet]
        public async Task<ActionResult> CurrentAffairPostDetail(int? postID)
        {
            BindCategory();
            if (postID == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Post list = await dbcontext.Posts.FindAsync(postID);
            if (list == null)
            {
                return HttpNotFound();
            }
            return View(list);
        }

        public async Task<ActionResult> CurrentAffairPostEdit(int? postID)
        {
            BindCategory();
            if (postID == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Post allpost = await dbcontext.Posts.FindAsync(postID);
            if (allpost == null)
            {
                return HttpNotFound();
            }
            return View(allpost);
        }

        [HttpPost, ValidateInput(false)]
        public ActionResult CurrentAffairPostEdit(Post model, HttpPostedFileBase FileUpload)
        {
            BindCategory();

            string filesnames = dbcontext.Posts.Where(i => i.PostId == model.PostId).Select(i => i.DefaultImage).FirstOrDefault();
            filesnames = filesnames == null ? "default.jpg" : filesnames;

            if (FileUpload != null)
            {
                if (!System.IO.Directory.Exists(Server.MapPath("\\Content\\Uploads\\Posts" + 1)))
                {
                    string savedFileName = DateTime.Now.ToString("MMddHHmmssfff") + Path.GetFileName(FileUpload.FileName);
                    FileUpload.SaveAs(Path.Combine(Server.MapPath("~/Content/Uploads/Posts"), savedFileName)); // Save the file
                    filesnames = savedFileName;
                }
            }

            Post updatePost = dbcontext.Posts.Find(model.PostId);
            if (updatePost.PostId == model.PostId)
            {
                updatePost.CategoryId = model.CategoryId;
                updatePost.Title = model.Title;
                updatePost.Description = model.Description;
                updatePost.DefaultImage = filesnames;
                updatePost.IsPublished = true;
                updatePost.IsDeleted = false;
                updatePost.ModifyDate = DateTime.Now;
            }
            dbcontext.Posts.AddOrUpdate(updatePost);
            dbcontext.SaveChanges();
            TempData["msg"] = "<script>alert('Updated Successfully');</script>";
            return RedirectToAction("CurrentAffairPostList", "Administrator");
        }

        [HttpGet]
        public async Task<ActionResult> CurrentAffairPostDelete(int? postID)
        {
            if (postID == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Post _delete = await dbcontext.Posts.FindAsync(postID);

            var filename = dbcontext.Posts.Where(p => p.PostId == postID).Select(i => i.DefaultImage).FirstOrDefault();
            var filePath = Server.MapPath("~/Content/Uploads/Posts" + filename);
            if (System.IO.File.Exists(filePath))
            {
                System.IO.File.Delete(filePath);
            }

            ///delete data in Likes, Comments, replies table w.r.t Post
            var _likes = dbcontext.Likes.Where(id => id.PostID == postID && id.SectionID == 3);
            var _comments = dbcontext.Comments.Where(id => id.PostID == postID && id.SectionID == 3);
            var _replies = dbcontext.ReplyComments.Where(id => id.PostID == postID && id.SectionID == 3);
            if (_likes != null || _comments != null || _replies != null)
            {
                dbcontext.Likes.RemoveRange(_likes);
                dbcontext.Comments.RemoveRange(_comments);
                dbcontext.ReplyComments.RemoveRange(_replies);
                dbcontext.Posts.Remove(_delete);
            }
            else
            {
                dbcontext.Posts.Remove(_delete);
            }
            dbcontext.SaveChanges();

            if (_delete == null)
            {
                return HttpNotFound();
            }
            TempData["msg"] = "<script>alert('Deleted Successfully');</script>";
            return RedirectToAction("CurrentAffairPostList", "Administrator");
        }


        ///////////////////////////////////////////////////////////////////////////////////////////////
        ////////////////////////////                                ///////////////////////////////////
        ///////////////////////////  Current Affair Post end        ///////////////////////////////////
        ///////////////////////////                                 ///////////////////////////////////
        ///////////////////////////////////////////////////////////////////////////////////////////////






        ///////////////////////////////////////////////////////////////////////////////////////////////
        ////////////////////////////                                ///////////////////////////////////
        ///////////////////////////  StudyMaterial Category Start        //////////////////////////////
        ///////////////////////////                                 ///////////////////////////////////
        ///////////////////////////////////////////////////////////////////////////////////////////////

        public void BindStudyMat()
        {
            List<StudyMaterial> study = dbcontext.StudyMaterials.Where(x => x.StudyID != 0 && x.isActive == true).ToList();
            StudyMaterial c = new StudyMaterial();
            c.StudyMaterialName = "Root";
            c.StudyID = 0;
            study.Insert(0, c);
            SelectList categorydata = new SelectList(study, "StudyID", "StudyMaterialName", 0);
            ViewBag.StudyMaterial = categorydata;
        }

        [HttpGet]
        [Authorize(Roles = "Admin")]
        public ActionResult StudyMaterial()
        {
            BindStudyMat();
            return View();
        }

        [HttpPost, ValidateInput(false)]
        public ActionResult StudyMaterial(StudyMaterial model, HttpPostedFileBase FileUpload)
        {
            BindStudyMat();
            string filesnames = "test.png";

            if (FileUpload != null)
            {
                if (!System.IO.Directory.Exists(Server.MapPath("\\Content\\Uploads\\Tab\\SubTab" + 1)))
                {
                    string savedFileName = Path.GetFileName(FileUpload.FileName);
                    FileUpload.SaveAs(Path.Combine(Server.MapPath("~/Content/Uploads/Tab/SubTab"), savedFileName)); // Save the file
                    filesnames = savedFileName;
                }
            }
            StudyMaterial insert = new StudyMaterial()
            {
                StudyMaterialName = model.StudyMaterialName,
                isActive = model.isActive,
                ParentID = model.ParentID == 0 ? null : model.ParentID,
                Description = model.Description,
                IconImage = "SubTab/" + filesnames,
                CreatedDate = DateTime.Now
            };
            dbcontext.StudyMaterials.Add(insert);
            dbcontext.SaveChanges();
            TempData["msg"] = "<script>alert('Added Successfully');</script>";
            return RedirectToAction("StudyMaterialList", "Administrator");
        }

        [HttpGet]
        public ActionResult StudyMaterialList()
        {
            var list = dbcontext.StudyMaterials.Where(p => p.StudyID != 0).ToList();
            return View(list);
        }

        public async Task<ActionResult> StudyMaterialEdit(int? catID)
        {
            BindStudyMat();
            if (catID == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            StudyMaterial cate = await dbcontext.StudyMaterials.FindAsync(catID);
            if (cate == null)
            {
                return HttpNotFound();
            }
            return View(cate);
        }

        [HttpPost, ValidateInput(false)]
        public ActionResult StudyMaterialEdit(StudyMaterial model, HttpPostedFileBase FileUpload)
        {
            BindStudyMat();

            string filesnames = dbcontext.StudyMaterials.Where(i => i.StudyID == model.StudyID).Select(i => i.IconImage).FirstOrDefault();
            filesnames = filesnames == null ? "test.png" : filesnames;

            if (FileUpload != null)
            {
                if (!System.IO.Directory.Exists(Server.MapPath("\\Content\\Uploads\\Tab\\SubTab" + 1)))
                {
                    string savedFileName = Path.GetFileName(FileUpload.FileName);
                    FileUpload.SaveAs(Path.Combine(Server.MapPath("~/Content/Uploads/Tab/SubTab"), savedFileName)); // Save the file
                    filesnames = savedFileName;
                }
            }
            StudyMaterial updatecate = dbcontext.StudyMaterials.Find(model.StudyID);
            if (updatecate.StudyID == model.StudyID)
            {
                updatecate.StudyMaterialName = model.StudyMaterialName;
                updatecate.ParentID = model.ParentID == 0 ? null : model.ParentID;
                updatecate.isActive = model.isActive;
                updatecate.Description = model.Description;
                updatecate.IconImage = "SubTab/" + filesnames;
                updatecate.ModifiedDate = DateTime.Now;
            }
            dbcontext.StudyMaterials.AddOrUpdate(updatecate);
            int c = dbcontext.SaveChanges();
            TempData["msg"] = "<script>alert('Updated Successfully');</script>";
            return RedirectToAction("StudyMaterialList", "Administrator");
        }

        public async Task<ActionResult> StudyMaterialActivate(int? catID)
        {
            if (catID == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            StudyMaterial update = await dbcontext.StudyMaterials.FindAsync(catID);
            if (update.StudyID == catID && update.isActive != true)
            {
                update.isActive = true;
            }
            else
            {
                update.isActive = false;
            }
            dbcontext.StudyMaterials.AddOrUpdate(update);
            dbcontext.SaveChanges();
            TempData["msg"] = "<script>alert('Updated Successfully');</script>";
            return RedirectToAction("StudyMaterialList", "Administrator");
        }

        //[Authorize(Roles = "Admin")]
        [HttpGet]
        public ActionResult StudyMaterialDelete(Category model, int? catID)
        {
            var associatedPosts = (from cat in dbcontext.StudyMaterials
                                   join catpost in dbcontext.StudyMaterialPosts on cat.StudyID equals catpost.StudyID
                                   where cat.StudyID == catID
                                   select new PostCategory
                                   {
                                       PostId = catpost.StudyMaterialID,
                                       CategoryName = cat.StudyMaterialName,
                                       Title = catpost.Title,
                                       DefaultImage = catpost.DefaultImage,
                                       CreatedDate = catpost.CreatedDate,
                                   }).OrderBy(c => c.CreatedDate).ToList();
            return View(associatedPosts);
        }

        [HttpPost]
        public ActionResult StudyMaterialDelete(int? catID)
        {
            StudyMaterial _parent = dbcontext.StudyMaterials.Find(catID);
            var _child = dbcontext.StudyMaterialPosts.Where(category => category.StudyID == catID);
            if (_child != null)
            {
                dbcontext.StudyMaterialPosts.RemoveRange(_child);
                dbcontext.StudyMaterials.Remove(_parent);
            }
            else
            {
                dbcontext.StudyMaterials.Remove(_parent);
            }
            dbcontext.SaveChanges();
            TempData["msg"] = "<script>alert('Deleted Successfully');</script>";
            return RedirectToAction("StudyMaterialList", "Administrator");
        }



        ///////////////////////////////////////////////////////////////////////////////////////////////
        ////////////////////////////                                ///////////////////////////////////
        ///////////////////////////  StudyMaterial Category End        ////////////////////////////////
        ///////////////////////////                                 ///////////////////////////////////
        ///////////////////////////////////////////////////////////////////////////////////////////////






        ///////////////////////////////////////////////////////////////////////////////////////////////
        ////////////////////////////                                ///////////////////////////////////
        ///////////////////////////  StudyMaterial Post Start        /////////////////////////////////
        ///////////////////////////                                 ///////////////////////////////////
        ///////////////////////////////////////////////////////////////////////////////////////////////

        public void BindStudyMaterial()
        {
            List<StudyMaterial> study = dbcontext.StudyMaterials.Where(x => x.StudyID != 0 && x.isActive == true).ToList();
            StudyMaterial c = new StudyMaterial();
            c.StudyMaterialName = "--Select Category--";
            c.StudyID = 0;
            study.Insert(0, c);
            SelectList categorydata = new SelectList(study, "StudyID", "StudyMaterialName", 0);
            ViewBag.StudyMaterial = categorydata;
        }

        [HttpGet]
        public ActionResult StudyMaterialPost()
        {
            BindStudyMaterial();
            return View();
        }

        [HttpPost, ValidateInput(false)]
        public ActionResult StudyMaterialPost(StudyMaterialPost model, HttpPostedFileBase FileUpload)
        {
            BindStudyMaterial();
            string filesnames = "default.jpg";

            if (FileUpload != null)
            {
                if (!System.IO.Directory.Exists(Server.MapPath("\\Content\\Uploads\\StudyMaterial" + 1)))
                {
                    string savedFileName = DateTime.Now.ToString("MMddHHmmssfff") + Path.GetFileName(FileUpload.FileName);
                    FileUpload.SaveAs(Path.Combine(Server.MapPath("~/Content/Uploads/StudyMaterial"), savedFileName)); // Save the file
                    filesnames = savedFileName;
                }
            }
            StudyMaterialPost insert = new StudyMaterialPost()
            {
                Title = model.Title,
                StudyID = model.StudyID,
                Description = model.Description,
                DefaultImage = filesnames,
                IsPublished = true,
                IsDeleted = false,
                Likes = 0,
                Comments = 0,
                CreatedDate = DateTime.Now,
            };
            dbcontext.StudyMaterialPosts.Add(insert);
            int c = dbcontext.SaveChanges();
            TempData["msg"] = "<script>alert('Added Successfully');</script>";
            return RedirectToAction("StudyMaterialPostList", "Administrator");
        }

        //[Authorize(Roles = "Admin")]
        [HttpGet]
        public ActionResult StudyMaterialPostList()
        {
            var list = (from postdata in dbcontext.StudyMaterialPosts
                        join catdata in dbcontext.StudyMaterials on postdata.StudyID equals catdata.StudyID
                        where postdata.IsDeleted != true
                        select new PostCategory
                        {
                            PostId = postdata.StudyMaterialID,
                            CategoryId = (int)postdata.StudyID,
                            CategoryName = catdata.StudyMaterialName,
                            Title = postdata.Title,
                            Description = postdata.Description,
                            DefaultImage = postdata.DefaultImage,
                            IsPublished = (bool)postdata.IsPublished,
                            PublishedDate = postdata.CreatedDate,
                            ModifyDate = postdata.ModifiedDate,
                        }).OrderByDescending(x => x.PublishedDate).ToList<PostCategory>();
            return View(list);
        }

        [HttpGet]
        public async Task<ActionResult> StudyMaterialPostDetail(int? postID)
        {
            BindStudyMaterial();
            if (postID == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            StudyMaterialPost list = await dbcontext.StudyMaterialPosts.FindAsync(postID);
            if (list == null)
            {
                return HttpNotFound();
            }
            return View(list);
        }

        public async Task<ActionResult> StudyMaterialPostEdit(int? postID)
        {
            BindStudyMaterial();
            if (postID == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            StudyMaterialPost allpost = await dbcontext.StudyMaterialPosts.FindAsync(postID);
            if (allpost == null)
            {
                return HttpNotFound();
            }
            return View(allpost);
        }

        [HttpPost, ValidateInput(false)]
        public ActionResult StudyMaterialPostEdit(StudyMaterialPost model, HttpPostedFileBase FileUpload)
        {
            BindStudyMaterial();

            string filesnames = dbcontext.Posts.Where(i => i.PostId == model.StudyMaterialID).Select(i => i.DefaultImage).FirstOrDefault();
            filesnames = filesnames == null ? "default.jpg" : filesnames;

            if (FileUpload != null)
            {
                if (!System.IO.Directory.Exists(Server.MapPath("\\Content\\Uploads\\StudyMaterial" + 1)))
                {
                    string savedFileName = DateTime.Now.ToString("MMddHHmmssfff") + Path.GetFileName(FileUpload.FileName);
                    FileUpload.SaveAs(Path.Combine(Server.MapPath("~/Content/Uploads/StudyMaterial"), savedFileName)); // Save the file
                    filesnames = savedFileName;
                }
            }
            StudyMaterialPost updatePost = dbcontext.StudyMaterialPosts.Find(model.StudyMaterialID);
            if (updatePost.StudyMaterialID == model.StudyMaterialID)
            {
                updatePost.StudyID = model.StudyID;
                updatePost.Title = model.Title;
                updatePost.Description = model.Description;
                updatePost.DefaultImage = filesnames;
                updatePost.IsPublished = true;
                updatePost.IsDeleted = false;
                updatePost.ModifiedDate = DateTime.Now;
            }
            dbcontext.StudyMaterialPosts.AddOrUpdate(updatePost);
            dbcontext.SaveChanges();
            TempData["msg"] = "<script>alert('Updated Successfully');</script>";
            return RedirectToAction("StudyMaterialPostList", "Administrator");
        }

        [HttpGet]
        public async Task<ActionResult> StudyMaterialPostDelete(int? postID)
        {
            if (postID == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            StudyMaterialPost _delete = await dbcontext.StudyMaterialPosts.FindAsync(postID);

            ///delete data in Likes, Comments, replies table w.r.t Post

            var filename = dbcontext.StudyMaterialPosts.Where(p => p.StudyMaterialID == postID).Select(i => i.DefaultImage).FirstOrDefault();
            var filePath = Server.MapPath("~/Content/Uploads/StudyMaterial/" + filename);
            if (System.IO.File.Exists(filePath))
            {
                System.IO.File.Delete(filePath);
            }

            var _likes = dbcontext.Likes.Where(id => id.PostID == postID && id.SectionID == 1);
            var _comments = dbcontext.Comments.Where(id => id.PostID == postID && id.SectionID == 1);
            var _replies = dbcontext.ReplyComments.Where(id => id.PostID == postID && id.SectionID == 1);
            if (_likes != null || _comments != null || _replies != null)
            {
                dbcontext.Likes.RemoveRange(_likes);
                dbcontext.Comments.RemoveRange(_comments);
                dbcontext.ReplyComments.RemoveRange(_replies);
                dbcontext.StudyMaterialPosts.Remove(_delete);
            }
            else
            {
                dbcontext.StudyMaterialPosts.Remove(_delete);
            }
            dbcontext.SaveChanges();

            if (_delete == null)
            {
                return HttpNotFound();
            }
            TempData["msg"] = "<script>alert('Deleted Successfully');</script>";
            return RedirectToAction("StudyMaterialPostList", "Administrator");
        }


        ///////////////////////////////////////////////////////////////////////////////////////////////
        ////////////////////////////                                ///////////////////////////////////
        ///////////////////////////  StudyMaterial Post end        ///////////////////////////////////
        ///////////////////////////                                 ///////////////////////////////////
        ///////////////////////////////////////////////////////////////////////////////////////////////






        ///////////////////////////////////////////////////////////////////////////////////////////////
        ////////////////////////////                                ///////////////////////////////////
        ///////////////////////////  ScienceTechnology Category Start        //////////////////////////////
        ///////////////////////////                                 ///////////////////////////////////
        ///////////////////////////////////////////////////////////////////////////////////////////////

        public void bindScienceTechnology()
        {
            List<ScienceTechnology> list = dbcontext.ScienceTechnologies.Where(active => active.Active == true).ToList();
            ScienceTechnology st = new ScienceTechnology();
            st.Name = "Root";
            st.ID = 0;
            list.Insert(0, st);
            SelectList newlist = new SelectList(list, "ID", "Name", 0);
            ViewBag.bindScienceTechnology = newlist;
        }

        [HttpGet]
        [Authorize(Roles = "Admin")]
        public ActionResult ScienceTechnology()
        {
            bindScienceTechnology();
            return View();
        }

        [HttpPost]
        public ActionResult ScienceTechnology(ScienceTechnology model, HttpPostedFileBase FileUpload)
        {
            bindScienceTechnology();
            string filesnames = "test.png";

            if (FileUpload != null)
            {
                if (!System.IO.Directory.Exists(Server.MapPath("\\Content\\Uploads\\Tab\\SubTab" + 1)))
                {
                    string savedFileName = Path.GetFileName(FileUpload.FileName);
                    FileUpload.SaveAs(Path.Combine(Server.MapPath("~/Content/Uploads/Tab/SubTab"), savedFileName)); // Save the file
                    filesnames = savedFileName;
                }
            }
            ScienceTechnology insert = new ScienceTechnology()
            {
                Name = model.Name,
                Active = model.Active,
                Description = model.Description,
                ParentID = model.ParentID == 0 ? null : model.ParentID,
                IconImage = "SubTab/" + filesnames,
                CreatedDate = DateTime.Now
            };
            dbcontext.ScienceTechnologies.Add(insert);
            dbcontext.SaveChanges();
            TempData["msg"] = "<script>alert('Added Successfully');</script>";
            return RedirectToAction("ScienceTechnologyList", "Administrator");
        }

        [HttpGet]
        public ActionResult ScienceTechnologyList()
        {
            var list = dbcontext.ScienceTechnologies.Where(p => p.ID != 0).ToList();
            return View(list);
        }

        public async Task<ActionResult> ScienceTechnologyEdit(int? catID)
        {
            bindScienceTechnology();
            if (catID == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            ScienceTechnology cate = await dbcontext.ScienceTechnologies.FindAsync(catID);
            if (cate == null)
            {
                return HttpNotFound();
            }
            return View(cate);
        }

        [HttpPost, ValidateInput(false)]
        public ActionResult ScienceTechnologyEdit(ScienceTechnology model, HttpPostedFileBase FileUpload)
        {
            bindScienceTechnology();
            string filesnames = dbcontext.ScienceTechnologies.Where(i => i.ID == model.ID).Select(i => i.IconImage).FirstOrDefault();
            filesnames = filesnames == null ? "test.png" : filesnames;

            if (FileUpload != null)
            {
                if (!System.IO.Directory.Exists(Server.MapPath("\\Content\\Uploads\\Tab\\SubTab" + 1)))
                {
                    string savedFileName = Path.GetFileName(FileUpload.FileName);
                    FileUpload.SaveAs(Path.Combine(Server.MapPath("~/Content/Uploads/Tab/SubTab"), savedFileName)); // Save the file
                    filesnames = savedFileName;
                }
            }
            ScienceTechnology updatecate = dbcontext.ScienceTechnologies.Find(model.ID);
            if (updatecate.ID == model.ID)
            {
                updatecate.Name = model.Name;
                updatecate.ParentID = model.ParentID == 0 ? null : model.ParentID;
                updatecate.Description = model.Description;
                updatecate.IconImage = "SubTab/" + filesnames;
                updatecate.Active = model.Active;
                updatecate.ModifiedDate = DateTime.Now;
            }
            dbcontext.ScienceTechnologies.AddOrUpdate(updatecate);
            int c = dbcontext.SaveChanges();
            TempData["msg"] = "<script>alert('Updated Successfully');</script>";
            return RedirectToAction("ScienceTechnologyList", "Administrator");
        }

        public async Task<ActionResult> ScienceTechnologyActivate(int? catID)
        {
            if (catID == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            ScienceTechnology update = await dbcontext.ScienceTechnologies.FindAsync(catID);
            if (update.ID == catID && update.Active != true)
            {
                update.Active = true;
            }
            else
            {
                update.Active = false;
            }
            dbcontext.ScienceTechnologies.AddOrUpdate(update);
            dbcontext.SaveChanges();
            TempData["msg"] = "<script>alert('Updated Successfully');</script>";
            return RedirectToAction("ScienceTechnologyList", "Administrator");
        }

        [HttpGet]
        public ActionResult ScienceTechnologyDelete(ScienceTechnology model, int? catID)
        {
            var associatedPosts = (from cat in dbcontext.ScienceTechnologies
                                   join catpost in dbcontext.STPosts on cat.ID equals catpost.SubID
                                   where cat.ID == catID
                                   select new PostCategory
                                   {
                                       PostId = catpost.ID,
                                       CategoryName = cat.Name,
                                       Title = catpost.Title,
                                       DefaultImage = catpost.DefaultImage,
                                       CreatedDate = catpost.CreatedDate,
                                   }).OrderBy(c => c.CreatedDate).ToList();
            return View(associatedPosts);
        }

        [HttpPost]
        public ActionResult ScienceTechnologyDelete(int? catID)
        {
            ScienceTechnology _parent = dbcontext.ScienceTechnologies.Find(catID);
            var _child = dbcontext.STPosts.Where(category => category.SubID == catID);
            if (_child != null)
            {
                dbcontext.STPosts.RemoveRange(_child);
                dbcontext.ScienceTechnologies.Remove(_parent);
            }
            else
            {
                dbcontext.ScienceTechnologies.Remove(_parent);
            }
            dbcontext.SaveChanges();
            TempData["msg"] = "<script>alert('Deleted Successfully');</script>";
            return RedirectToAction("ScienceTechnologyList", "Administrator");
        }

        ///////////////////////////////////////////////////////////////////////////////////////////////
        ////////////////////////////                                ///////////////////////////////////
        ///////////////////////////  ScienceTechnology Category End        ////////////////////////////////
        ///////////////////////////                                 ///////////////////////////////////
        ///////////////////////////////////////////////////////////////////////////////////////////////




        ///////////////////////////////////////////////////////////////////////////////////////////////
        ////////////////////////////                                ///////////////////////////////////
        ///////////////////////////  ScienceTechnology Post Start        //////////////////////////////
        ///////////////////////////                                 ///////////////////////////////////
        ///////////////////////////////////////////////////////////////////////////////////////////////

        public void bindCategoryST()
        {
            List<ScienceTechnology> list = dbcontext.ScienceTechnologies.Where(active => active.Active == true).ToList();
            ScienceTechnology st = new ScienceTechnology();
            st.Name = "Select Category";
            st.ID = 0;
            list.Insert(0, st);
            SelectList newlist = new SelectList(list, "ID", "Name", 0);
            ViewBag.bindCategoryST = newlist;
        }

        [HttpGet]
        //[Authorize(Roles = "Admin")]
        public ActionResult ScienceTechnologyPost()
        {
            bindCategoryST();
            return View();
        }

        [HttpPost, ValidateInput(false)]
        public ActionResult ScienceTechnologyPost(STPost model, HttpPostedFileBase FileUpload)
        {
            bindCategoryST();
            string filesnames = "default.jpg";

            if (FileUpload != null)
            {
                if (!System.IO.Directory.Exists(Server.MapPath("\\Content\\Uploads\\ScienceTechnology" + 1)))
                {
                    string savedFileName = DateTime.Now.ToString("MMddHHmmssfff") + Path.GetFileName(FileUpload.FileName);
                    FileUpload.SaveAs(Path.Combine(Server.MapPath("~/Content/Uploads/ScienceTechnology"), savedFileName)); // Save the file
                    filesnames = savedFileName;
                }
            }
            STPost insert = new STPost()
            {
                Title = model.Title,
                SubID = model.SubID,
                Description = model.Description,
                DefaultImage = filesnames,
                Published = true,
                Deleted = false,
                Likes = 0,
                Comments = 0,
                CreatedDate = DateTime.Now,
            };
            dbcontext.STPosts.Add(insert);
            int c = dbcontext.SaveChanges();
            TempData["msg"] = "<script>alert('Added Successfully');</script>";
            return RedirectToAction("ScienceTechnologyPostList", "Administrator");
        }

        [HttpGet]
        public ActionResult ScienceTechnologyPostList()
        {
            var list = (from postdata in dbcontext.STPosts
                        join catdata in dbcontext.ScienceTechnologies on postdata.SubID equals catdata.ID
                        where postdata.Deleted != true
                        select new PostCategory
                        {
                            PostId = postdata.ID,
                            CategoryId = (int)postdata.SubID,
                            CategoryName = catdata.Name,
                            Title = postdata.Title,
                            Description = postdata.Description,
                            DefaultImage = postdata.DefaultImage,
                            IsPublished = (bool)postdata.Published,
                            PublishedDate = postdata.CreatedDate,
                            ModifyDate = postdata.ModifiedDate,
                        }).OrderByDescending(x => x.PublishedDate).ToList<PostCategory>();
            return View(list);
        }

        [HttpGet]
        public async Task<ActionResult> ScienceTechnologyPostDetail(int? postID)
        {
            bindCategoryST();
            if (postID == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            STPost list = await dbcontext.STPosts.FindAsync(postID);
            if (list == null)
            {
                return HttpNotFound();
            }
            return View(list);
        }

        public async Task<ActionResult> ScienceTechnologyPostEdit(int? postID)
        {
            bindCategoryST();
            if (postID == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            STPost allpost = await dbcontext.STPosts.FindAsync(postID);
            if (allpost == null)
            {
                return HttpNotFound();
            }
            return View(allpost);
        }

        [HttpPost, ValidateInput(false)]
        public ActionResult ScienceTechnologyPostEdit(STPost model, HttpPostedFileBase FileUpload)
        {
            bindCategoryST();

            string filesnames = dbcontext.STPosts.Where(i => i.ID == model.ID).Select(i => i.DefaultImage).FirstOrDefault();
            filesnames = filesnames == null ? "default.jpg" : filesnames;

            if (FileUpload != null)
            {
                if (!System.IO.Directory.Exists(Server.MapPath("\\Content\\Uploads\\ScienceTechnology" + 1)))
                {
                    string savedFileName = DateTime.Now.ToString("MMddHHmmssfff") + Path.GetFileName(FileUpload.FileName);
                    FileUpload.SaveAs(Path.Combine(Server.MapPath("~/Content/Uploads/ScienceTechnology"), savedFileName)); // Save the file
                    filesnames = savedFileName;
                }
            }
            STPost updatePost = dbcontext.STPosts.Find(model.ID);
            if (updatePost.ID == model.ID)
            {
                updatePost.SubID = model.SubID;
                updatePost.Title = model.Title;
                updatePost.Description = model.Description;
                updatePost.DefaultImage = filesnames;
                updatePost.Published = true;
                updatePost.Deleted = false;
                updatePost.ModifiedDate = DateTime.Now;
            }
            dbcontext.STPosts.AddOrUpdate(updatePost);
            dbcontext.SaveChanges();
            TempData["msg"] = "<script>alert('Updated Successfully');</script>";
            return RedirectToAction("ScienceTechnologyPostList", "Administrator");
        }

        [HttpGet]
        public async Task<ActionResult> ScienceTechnologyPostDelete(int? postID)
        {
            if (postID == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            STPost _delete = await dbcontext.STPosts.FindAsync(postID);

            var filename = dbcontext.STPosts.Where(p => p.ID == postID).Select(i => i.DefaultImage).FirstOrDefault();
            var filePath = Server.MapPath("~/Content/Uploads/ScienceTechnology/" + filename);
            if (System.IO.File.Exists(filePath))
            {
                System.IO.File.Delete(filePath);
            }

            ///delete data in Likes, Comments, replies table w.r.t Post
            var _likes = dbcontext.Likes.Where(id => id.PostID == postID && id.SectionID == 2);
            var _comments = dbcontext.Comments.Where(id => id.PostID == postID && id.SectionID == 2);
            var _replies = dbcontext.ReplyComments.Where(id => id.PostID == postID && id.SectionID == 2);
            if (_likes != null || _comments != null || _replies != null)
            {
                dbcontext.Likes.RemoveRange(_likes);
                dbcontext.Comments.RemoveRange(_comments);
                dbcontext.ReplyComments.RemoveRange(_replies);
                dbcontext.STPosts.Remove(_delete);
            }
            else
            {
                dbcontext.STPosts.Remove(_delete);
            }
            dbcontext.SaveChanges();

            if (_delete == null)
            {
                return HttpNotFound();
            }
            TempData["msg"] = "<script>alert('Deleted Successfully');</script>";
            return RedirectToAction("ScienceTechnologyPostList", "Administrator");
        }


        ///////////////////////////////////////////////////////////////////////////////////////////////
        ////////////////////////////                                ///////////////////////////////////
        ///////////////////////////  ScienceTechnology Post end        ////////////////////////////////
        ///////////////////////////                                 ///////////////////////////////////
        ///////////////////////////////////////////////////////////////////////////////////////////////






        ///////////////////////////////////////////////////////////////////////////////////////////////
        ////////////////////////////                                ///////////////////////////////////
        ///////////////////////////  TalentPlatform Category Start        //////////////////////////////
        ///////////////////////////                                 ///////////////////////////////////
        ///////////////////////////////////////////////////////////////////////////////////////////////

        public void BindTalent()
        {
            List<Talent> talent = dbcontext.Talents.Where(x => x.ID != 0 && x.isActive == true).ToList();
            Talent h = new Talent();
            h.Name = "Root";
            h.ID = 0;
            talent.Insert(0, h);
            SelectList hdata = new SelectList(talent, "ID", "Name", 0);
            ViewBag.talent = hdata;
        }

        [HttpGet]
        [Authorize(Roles = "Admin")]
        public ActionResult TalentPlatform()
        {
            BindTalent();
            return View();
        }

        [HttpPost]
        public ActionResult TalentPlatform(Talent model, HttpPostedFileBase FileUpload)
        {
            BindTalent();
            string filesnames = "test.png";
            if (FileUpload != null)
            {
                if (!System.IO.Directory.Exists(Server.MapPath("\\Content\\Uploads\\Tab\\SubTab" + 1)))
                {
                    string savedFileName = Path.GetFileName(FileUpload.FileName);
                    FileUpload.SaveAs(Path.Combine(Server.MapPath("~/Content/Uploads/Tab/SubTab"), savedFileName)); // Save the file
                    filesnames = savedFileName;
                }
            }
            Talent insert = new Talent()
            {
                Name = model.Name,
                isActive = model.isActive,
                ParentID = model.ParentID == 0 ? null : model.ParentID,
                IconImage = "SubTab/" + filesnames,
                CreatedDate = DateTime.Now
            };
            dbcontext.Talents.Add(insert);
            dbcontext.SaveChanges();
            TempData["msg"] = "<script>alert('Added Successfully');</script>";
            return RedirectToAction("TalentPlatformList", "Administrator");
        }

        [HttpGet]
        public ActionResult TalentPlatformList()
        {
            var list = dbcontext.Talents.Where(p => p.ID != 0).ToList();
            return View(list);
        }

        public async Task<ActionResult> TalentPlatformEdit(int? catID)
        {
            BindTalent();
            if (catID == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Talent cate = await dbcontext.Talents.FindAsync(catID);
            if (cate == null)
            {
                return HttpNotFound();
            }
            return View(cate);
        }

        [HttpPost, ValidateInput(false)]
        public ActionResult TalentPlatformEdit(Talent model, HttpPostedFileBase FileUpload)
        {
            BindTalent();

            string filesnames = dbcontext.Talents.Where(i => i.ID == model.ID).Select(i => i.IconImage).FirstOrDefault();
            filesnames = filesnames == null ? "test.png" : filesnames;

            if (FileUpload != null)
            {
                if (!System.IO.Directory.Exists(Server.MapPath("\\Content\\Uploads\\Tab\\SubTab" + 1)))
                {
                    string savedFileName = Path.GetFileName(FileUpload.FileName);
                    FileUpload.SaveAs(Path.Combine(Server.MapPath("~/Content/Uploads/Tab/SubTab"), savedFileName)); // Save the file
                    filesnames = savedFileName;
                }
            }

            Talent updatecate = dbcontext.Talents.Find(model.ID);
            if (updatecate.ID == model.ID)
            {
                updatecate.Name = model.Name;
                updatecate.ParentID = model.ParentID == 0 ? null : model.ParentID;
                updatecate.isActive = model.isActive;
                updatecate.IconImage = "SubTab/" + filesnames;
                updatecate.ModifiedDate = DateTime.Now;
            }
            dbcontext.Talents.AddOrUpdate(updatecate);
            int c = dbcontext.SaveChanges();
            TempData["msg"] = "<script>alert('Updated Successfully');</script>";
            return RedirectToAction("TalentPlatformList", "Administrator");
        }

        public async Task<ActionResult> TalentPlatformActivate(int? catID)
        {
            if (catID == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Talent update = await dbcontext.Talents.FindAsync(catID);
            if (update.ID == catID && update.isActive != true)
            {
                update.isActive = true;
            }
            else
            {
                update.isActive = false;
            }
            dbcontext.Talents.AddOrUpdate(update);
            dbcontext.SaveChanges();
            TempData["msg"] = "<script>alert('Updated Successfully');</script>";
            return RedirectToAction("TalentPlatformList", "Administrator");
        }

        [HttpGet]
        public ActionResult TalentPlatformDelete(Talent model, int? catID)
        {
            var associatedPosts = (from cat in dbcontext.Talents
                                   join catpost in dbcontext.TalentPosts on cat.ID equals catpost.SubID
                                   where cat.ID == catID
                                   select new PostCategory
                                   {
                                       PostId = catpost.ID,
                                       CategoryName = cat.Name,
                                       Title = catpost.Title,
                                       DefaultImage = catpost.DefaultImage,
                                       CreatedDate = catpost.CreatedDate,
                                   }).OrderBy(c => c.CreatedDate).ToList();
            return View(associatedPosts);
        }

        [HttpPost]
        public ActionResult TalentPlatformDelete(int? catID)
        {
            Talent _parent = dbcontext.Talents.Find(catID);
            var _child = dbcontext.TalentPosts.Where(category => category.SubID == catID);
            if (_child != null)
            {
                dbcontext.TalentPosts.RemoveRange(_child);
                dbcontext.Talents.Remove(_parent);
            }
            else
            {
                dbcontext.Talents.Remove(_parent);
            }
            dbcontext.SaveChanges();
            TempData["msg"] = "<script>alert('Deleted Successfully');</script>";
            return RedirectToAction("TalentPlatformList", "Administrator");
        }

        ///////////////////////////////////////////////////////////////////////////////////////////////
        ////////////////////////////                                ///////////////////////////////////
        ///////////////////////////  TalentPlatform Category End        ////////////////////////////////
        ///////////////////////////                                 ///////////////////////////////////
        ///////////////////////////////////////////////////////////////////////////////////////////////





        ///////////////////////////////////////////////////////////////////////////////////////////////
        ////////////////////////////                                ///////////////////////////////////
        ///////////////////////////  TalentPlatform Post Start        //////////////////////////////
        ///////////////////////////                                 ///////////////////////////////////
        ///////////////////////////////////////////////////////////////////////////////////////////////

        public void BindTalentPost()
        {
            List<Talent> Talent = dbcontext.Talents.Where(x => x.ID != 0 && x.isActive == true).ToList();
            Talent h = new Talent();
            h.Name = "--Select Talent--";
            h.ID = 0;
            Talent.Insert(0, h);
            SelectList hdata = new SelectList(Talent, "ID", "Name", 0);
            ViewBag.TalentPost1 = hdata;
        }

        [HttpGet]
        [Authorize(Roles = "Admin")]
        public ActionResult TalentPlatformPost()
        {
            BindTalentPost();
            return View();
        }

        [HttpPost, ValidateInput(false)]
        public ActionResult TalentPlatformPost(TalentPost model, HttpPostedFileBase FileUpload)
        {
            BindTalentPost();
            string filesnames = "default.jpg";

            if (FileUpload != null)
            {
                if (!System.IO.Directory.Exists(Server.MapPath("\\Content\\Uploads\\TalentPlatform" + 1)))
                {
                    string savedFileName = DateTime.Now.ToString("MMddHHmmssfff") + Path.GetFileName(FileUpload.FileName);
                    FileUpload.SaveAs(Path.Combine(Server.MapPath("~/Content/Uploads/TalentPlatform"), savedFileName)); // Save the file
                    filesnames = savedFileName;
                }
            }
            TalentPost insert = new TalentPost()
            {
                Title = model.Title,
                SubID = model.SubID,
                Description = model.Description,
                DefaultImage = filesnames,
                IsPublished = true,
                IsDeleted = false,
                Likes = 0,
                Comments = 0,
                CreatedDate = DateTime.Now,
            };
            dbcontext.TalentPosts.Add(insert);
            int c = dbcontext.SaveChanges();
            TempData["msg"] = "<script>alert('Added Successfully');</script>";
            return RedirectToAction("TalentPlatformPostList", "Administrator");
        }

        [HttpGet]
        public ActionResult TalentPlatformPostList()
        {
            var list = (from postdata in dbcontext.TalentPosts
                        join catdata in dbcontext.Talents on postdata.SubID equals catdata.ID
                        where postdata.IsDeleted != true
                        select new PostCategory
                        {
                            PostId = postdata.ID,
                            CategoryId = (int)postdata.SubID,
                            CategoryName = catdata.Name,
                            Title = postdata.Title,
                            Description = postdata.Description,
                            DefaultImage = postdata.DefaultImage,
                            IsPublished = (bool)postdata.IsPublished,
                            PublishedDate = postdata.CreatedDate,
                            ModifyDate = postdata.ModifiedDate,
                        }).OrderByDescending(x => x.PublishedDate).ToList<PostCategory>();
            return View(list);
        }

        [HttpGet]
        public async Task<ActionResult> TalentPlatformPostDetail(int? postID)
        {
            BindTalentPost();
            if (postID == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            TalentPost list = await dbcontext.TalentPosts.FindAsync(postID);
            if (list == null)
            {
                return HttpNotFound();
            }
            return View(list);
        }

        public async Task<ActionResult> TalentPlatformPostEdit(int? postID)
        {
            BindTalentPost();
            if (postID == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            TalentPost allpost = await dbcontext.TalentPosts.FindAsync(postID);
            if (allpost == null)
            {
                return HttpNotFound();
            }
            return View(allpost);
        }

        [HttpPost, ValidateInput(false)]
        public ActionResult TalentPlatformPostEdit(TalentPost model, HttpPostedFileBase FileUpload)
        {
            BindTalentPost();

            string filesnames = dbcontext.STPosts.Where(i => i.ID == model.ID).Select(i => i.DefaultImage).FirstOrDefault();
            filesnames = filesnames == null ? "default.jpg" : filesnames;

            if (FileUpload != null)
            {
                if (!System.IO.Directory.Exists(Server.MapPath("\\Content\\Uploads\\TalentPlatform" + 1)))
                {
                    string savedFileName = DateTime.Now.ToString("MMddHHmmssfff") + Path.GetFileName(FileUpload.FileName);
                    FileUpload.SaveAs(Path.Combine(Server.MapPath("~/Content/Uploads/TalentPlatform"), savedFileName)); // Save the file
                    filesnames = savedFileName;
                }
            }
            TalentPost updatePost = dbcontext.TalentPosts.Find(model.ID);
            if (updatePost.ID == model.ID)
            {
                updatePost.SubID = model.SubID;
                updatePost.Title = model.Title;
                updatePost.Description = model.Description;
                updatePost.DefaultImage = filesnames;
                updatePost.IsPublished = true;
                updatePost.IsDeleted = false;
                updatePost.ModifiedDate = DateTime.Now;
            }
            dbcontext.TalentPosts.AddOrUpdate(updatePost);
            dbcontext.SaveChanges();
            TempData["msg"] = "<script>alert('Updated Successfully');</script>";
            return RedirectToAction("TalentPlatformPostList", "Administrator");
        }

        [HttpGet]
        public async Task<ActionResult> TalentPlatformPostDelete(int? postID)
        {
            if (postID == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            TalentPost _delete = await dbcontext.TalentPosts.FindAsync(postID);

            var filename = dbcontext.TalentPosts.Where(p => p.ID == postID).Select(i => i.DefaultImage).FirstOrDefault();
            var filePath = Server.MapPath("~/Content/Uploads/TalentPlatform/" + filename);
            if (System.IO.File.Exists(filePath))
            {
                System.IO.File.Delete(filePath);
            }

            ///delete data in Likes, Comments, replies table w.r.t Post
            var _likes = dbcontext.Likes.Where(id => id.PostID == postID && id.SectionID == 4);
            var _comments = dbcontext.Comments.Where(id => id.PostID == postID && id.SectionID == 4);
            var _replies = dbcontext.ReplyComments.Where(id => id.PostID == postID && id.SectionID == 4);
            if (_likes != null || _comments != null || _replies != null)
            {
                dbcontext.Likes.RemoveRange(_likes);
                dbcontext.Comments.RemoveRange(_comments);
                dbcontext.ReplyComments.RemoveRange(_replies);
                dbcontext.TalentPosts.Remove(_delete);
            }
            else
            {
                dbcontext.TalentPosts.Remove(_delete);
            }
            dbcontext.SaveChanges();

            if (_delete == null)
            {
                return HttpNotFound();
            }
            TempData["msg"] = "<script>alert('Deleted Successfully');</script>";
            return RedirectToAction("TalentPlatformPostList", "Administrator");
        }


        ///////////////////////////////////////////////////////////////////////////////////////////////
        ////////////////////////////                                ///////////////////////////////////
        ///////////////////////////  TalentPlatform Post end        ////////////////////////////////
        ///////////////////////////                                 ///////////////////////////////////
        ///////////////////////////////////////////////////////////////////////////////////////////////
        ///



        ///////////////////////////////////////////////////////////////////////////////////////////////
        ////////////////////////////                                ///////////////////////////////////
        ///////////////////////////  PersonPersonality Post Start        //////////////////////////////
        ///////////////////////////                                 ///////////////////////////////////
        ///////////////////////////////////////////////////////////////////////////////////////////////

        [HttpGet]
        public ActionResult PersonPersonalityPost()
        {
            return View();
        }

        [HttpPost, ValidateInput(false)]
        public ActionResult PersonPersonalityPost(Person_Personality model, HttpPostedFileBase FileUpload)
        {
            string filesnames = "default.jpg";

            if (FileUpload != null)
            {
                if (!System.IO.Directory.Exists(Server.MapPath("\\Content\\Uploads\\PersonPersonality" + 1)))
                {
                    string savedFileName = DateTime.Now.ToString("MMddHHmmssfff") + Path.GetFileName(FileUpload.FileName);
                    FileUpload.SaveAs(Path.Combine(Server.MapPath("~/Content/Uploads/PersonPersonality"), savedFileName)); // Save the file
                    filesnames = savedFileName;
                }
            }
            Person_Personality insert = new Person_Personality()
            {
                Title = model.Title,
                Description = model.Description,
                DefaultImage = filesnames,
                Published = true,
                Deleted = false,
                Likes = 0,
                Comments = 0,
                CreatedDate = DateTime.Now,
            };
            dbcontext.Person_Personality.Add(insert);
            int c = dbcontext.SaveChanges();
            TempData["msg"] = "<script>alert('Added Successfully');</script>";
            return RedirectToAction("PersonPersonalityPostList", "Administrator");
        }

        //[Authorize(Roles = "Admin")]
        [HttpGet]
        public ActionResult PersonPersonalityPostList()
        {
            var list = dbcontext.Person_Personality.Where(p => p.Published == true).ToList();
            return View(list);
        }

        [HttpGet]
        public async Task<ActionResult> PersonPersonalityPostDetail(int? postID)
        {
            if (postID == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Person_Personality list = await dbcontext.Person_Personality.FindAsync(postID);
            if (list == null)
            {
                return HttpNotFound();
            }
            return View(list);
        }

        public async Task<ActionResult> PersonPersonalityPostEdit(int? postID)
        {
            if (postID == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Person_Personality allpost = await dbcontext.Person_Personality.FindAsync(postID);
            if (allpost == null)
            {
                return HttpNotFound();
            }
            return View(allpost);
        }

        [HttpPost, ValidateInput(false)]
        public ActionResult PersonPersonalityPostEdit(Person_Personality model, HttpPostedFileBase FileUpload)
        {
            string filesnames = dbcontext.STPosts.Where(i => i.ID == model.ID).Select(i => i.DefaultImage).FirstOrDefault();
            filesnames = filesnames == null ? "default.jpg" : filesnames;

            if (FileUpload != null)
            {
                if (!System.IO.Directory.Exists(Server.MapPath("\\Content\\Uploads\\PersonPersonality" + 1)))
                {
                    string savedFileName = DateTime.Now.ToString("MMddHHmmssfff") + Path.GetFileName(FileUpload.FileName);
                    FileUpload.SaveAs(Path.Combine(Server.MapPath("~/Content/Uploads/PersonPersonality"), savedFileName)); // Save the file
                    filesnames = savedFileName;
                }
            }
            Person_Personality updatePost = dbcontext.Person_Personality.Find(model.ID);
            if (updatePost.ID == model.ID)
            {
                updatePost.Title = model.Title;
                updatePost.Description = model.Description;
                updatePost.DefaultImage = filesnames;
                updatePost.Published = true;
                updatePost.Deleted = false;
                updatePost.ModifiedDate = DateTime.Now;
            }
            dbcontext.Person_Personality.AddOrUpdate(updatePost);
            dbcontext.SaveChanges();
            TempData["msg"] = "<script>alert('Updated Successfully');</script>";
            return RedirectToAction("PersonPersonalityPostList", "Administrator");
        }

        [HttpGet]
        public async Task<ActionResult> PersonPersonalityPostDelete(int? postID)
        {
            if (postID == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Person_Personality _delete = await dbcontext.Person_Personality.FindAsync(postID);

            var filename = dbcontext.Person_Personality.Where(p => p.ID == postID).Select(i => i.DefaultImage).FirstOrDefault();
            var filePath = Server.MapPath("~/Content/Uploads/PersonPersonality/" + filename);
            if (System.IO.File.Exists(filePath))
            {
                System.IO.File.Delete(filePath);
            }

            ///delete data in Likes, Comments, replies table w.r.t Post
            var _likes = dbcontext.Likes.Where(id => id.PostID == postID && id.SectionID == 6);
            var _comments = dbcontext.Comments.Where(id => id.PostID == postID && id.SectionID == 6);
            var _replies = dbcontext.ReplyComments.Where(id => id.PostID == postID && id.SectionID == 6);
            if (_likes != null || _comments != null || _replies != null)
            {
                dbcontext.Likes.RemoveRange(_likes);
                dbcontext.Comments.RemoveRange(_comments);
                dbcontext.ReplyComments.RemoveRange(_replies);
                dbcontext.Person_Personality.Remove(_delete);
            }
            else
            {
                dbcontext.Person_Personality.Remove(_delete);
            }
            dbcontext.SaveChanges();
            if (_delete == null)
            {
                return HttpNotFound();
            }
            TempData["msg"] = "<script>alert('Deleted Successfully');</script>";
            return RedirectToAction("PersonPersonalityPostList", "Administrator");
        }


        ///////////////////////////////////////////////////////////////////////////////////////////////
        ////////////////////////////                                ///////////////////////////////////
        ///////////////////////////  PersonPersonality Post end        ////////////////////////////////
        ///////////////////////////                                 ///////////////////////////////////
        ///////////////////////////////////////////////////////////////////////////////////////////////






        ///////////////////////////////////////////////////////////////////////////////////////////////
        ////////////////////////////                                ///////////////////////////////////
        ///////////////////////////  PersonalityDevlopment Post Start        //////////////////////////////
        ///////////////////////////                                 ///////////////////////////////////
        ///////////////////////////////////////////////////////////////////////////////////////////////

        [HttpGet]
        public ActionResult PersonalityDevlopmentPost()
        {
            return View();
        }

        [HttpPost, ValidateInput(false)]
        public ActionResult PersonalityDevlopmentPost(PersonalityDevlopment model, HttpPostedFileBase FileUpload)
        {
            string filesnames = "default.jpg";

            if (FileUpload != null)
            {
                if (!System.IO.Directory.Exists(Server.MapPath("\\Content\\Uploads\\PersonalityDevlopment" + 1)))
                {
                    string savedFileName = DateTime.Now.ToString("MMddHHmmssfff") + Path.GetFileName(FileUpload.FileName);
                    FileUpload.SaveAs(Path.Combine(Server.MapPath("~/Content/Uploads/PersonalityDevlopment"), savedFileName)); // Save the file
                    filesnames = savedFileName;
                }
            }
            PersonalityDevlopment insert = new PersonalityDevlopment()
            {
                Title = model.Title,
                Description = model.Description,
                DefaultImage = filesnames,
                Published = true,
                Deleted = false,
                Likes = 0,
                Comments = 0,
                CreatedDate = DateTime.Now,
            };
            dbcontext.PersonalityDevlopments.Add(insert);
            int c = dbcontext.SaveChanges();
            TempData["msg"] = "<script>alert('Added Successfully');</script>";
            return RedirectToAction("PersonalityDevlopmentPostList", "Administrator");
        }

        //[Authorize(Roles = "Admin")]
        [HttpGet]
        public ActionResult PersonalityDevlopmentPostList()
        {
            var list = dbcontext.PersonalityDevlopments.Where(p => p.Published == true).ToList();
            return View(list);
        }

        [HttpGet]
        public async Task<ActionResult> PersonalityDevlopmentPostDetail(int? postID)
        {
            if (postID == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            PersonalityDevlopment list = await dbcontext.PersonalityDevlopments.FindAsync(postID);
            if (list == null)
            {
                return HttpNotFound();
            }
            return View(list);
        }

        public async Task<ActionResult> PersonalityDevlopmentPostEdit(int? postID)
        {
            if (postID == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            PersonalityDevlopment allpost = await dbcontext.PersonalityDevlopments.FindAsync(postID);
            if (allpost == null)
            {
                return HttpNotFound();
            }
            return View(allpost);
        }

        [HttpPost, ValidateInput(false)]
        public ActionResult PersonalityDevlopmentPostEdit(PersonalityDevlopment model, HttpPostedFileBase FileUpload)
        {
            string filesnames = dbcontext.PersonalityDevlopments.Where(i => i.PDID == model.PDID).Select(i => i.DefaultImage).FirstOrDefault();
            filesnames = filesnames == null ? "default.jpg" : filesnames;

            if (FileUpload != null)
            {
                if (!System.IO.Directory.Exists(Server.MapPath("\\Content\\Uploads\\PersonalityDevlopment" + 1)))
                {
                    string savedFileName = DateTime.Now.ToString("MMddHHmmssfff") + Path.GetFileName(FileUpload.FileName);
                    FileUpload.SaveAs(Path.Combine(Server.MapPath("~/Content/Uploads/PersonalityDevlopment"), savedFileName)); // Save the file
                    filesnames = savedFileName;
                }
            }
            PersonalityDevlopment updatePost = dbcontext.PersonalityDevlopments.Find(model.PDID);
            if (updatePost.PDID == model.PDID)
            {
                updatePost.Title = model.Title;
                updatePost.Description = model.Description;
                updatePost.DefaultImage = filesnames;
                updatePost.Published = true;
                updatePost.Deleted = false;
                updatePost.ModifiedDate = DateTime.Now;
            }
            dbcontext.PersonalityDevlopments.AddOrUpdate(updatePost);
            dbcontext.SaveChanges();
            TempData["msg"] = "<script>alert('Updated Successfully');</script>";
            return RedirectToAction("PersonalityDevlopmentPostList", "Administrator");
        }

        [HttpGet]
        public async Task<ActionResult> PersonalityDevlopmentPostDelete(int? postID)
        {
            if (postID == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            PersonalityDevlopment _delete = await dbcontext.PersonalityDevlopments.FindAsync(postID);

            var filename = dbcontext.PersonalityDevlopments.Where(p => p.PDID == postID).Select(i => i.DefaultImage).FirstOrDefault();
            var filePath = Server.MapPath("~/Content/Uploads/PersonalityDevlopment/" + filename);
            if (System.IO.File.Exists(filePath))
            {
                System.IO.File.Delete(filePath);
            }

            ///delete data in Likes, Comments, replies table w.r.t Post
            var _likes = dbcontext.Likes.Where(id => id.PostID == postID && id.SectionID == 8);
            var _comments = dbcontext.Comments.Where(id => id.PostID == postID && id.SectionID == 8);
            var _replies = dbcontext.ReplyComments.Where(id => id.PostID == postID && id.SectionID == 8);
            if (_likes != null || _comments != null || _replies != null)
            {
                dbcontext.Likes.RemoveRange(_likes);
                dbcontext.Comments.RemoveRange(_comments);
                dbcontext.ReplyComments.RemoveRange(_replies);
                dbcontext.PersonalityDevlopments.Remove(_delete);
            }
            else
            {
                dbcontext.PersonalityDevlopments.Remove(_delete);
            }
            dbcontext.SaveChanges();
            if (_delete == null)
            {
                return HttpNotFound();
            }
            TempData["msg"] = "<script>alert('Deleted Successfully');</script>";
            return RedirectToAction("PersonalityDevlopmentPostList", "Administrator");
        }


        ///////////////////////////////////////////////////////////////////////////////////////////////
        ////////////////////////////                                ///////////////////////////////////
        ///////////////////////////  PersonalityDevlopment Post end        ////////////////////////////////
        ///////////////////////////                                 ///////////////////////////////////
        ///////////////////////////////////////////////////////////////////////////////////////////////




        ///////////////////////////////////////////////////////////////////////////////////////////////
        ////////////////////////////                                ///////////////////////////////////
        ///////////////////////////  Editorial Post Start        //////////////////////////////
        ///////////////////////////                                 ///////////////////////////////////
        ///////////////////////////////////////////////////////////////////////////////////////////////

        [HttpGet]
        public ActionResult EditorialPost()
        {
            return View();
        }

        [HttpPost, ValidateInput(false)]
        public ActionResult EditorialPost(Editorial model, HttpPostedFileBase FileUpload)
        {
            string filesnames = "default.jpg";

            if (FileUpload != null)
            {
                if (!System.IO.Directory.Exists(Server.MapPath("\\Content\\Uploads\\Editorial" + 1)))
                {
                    string savedFileName = DateTime.Now.ToString("MMddHHmmssfff") + Path.GetFileName(FileUpload.FileName);
                    FileUpload.SaveAs(Path.Combine(Server.MapPath("~/Content/Uploads/Editorial"), savedFileName)); // Save the file
                    filesnames = savedFileName;
                }
            }
            Editorial insert = new Editorial()
            {
                Title = model.Title,
                Description = model.Description,
                EditorialImage = filesnames,
                isPublished = true,
                isDeleted = false,
                Likes = 0,
                Comments = 0,
                CreatedDate = DateTime.Now,
            };
            dbcontext.Editorials.Add(insert);
            int c = dbcontext.SaveChanges();
            TempData["msg"] = "<script>alert('Added Successfully');</script>";
            return RedirectToAction("EditorialPostList", "Administrator");
        }

        //[Authorize(Roles = "Admin")]
        [HttpGet]
        public ActionResult EditorialPostList()
        {
            var list = dbcontext.Editorials.Where(p => p.isDeleted != true).OrderByDescending(x => x.CreatedDate).ToList();
            return View(list);
        }

        [HttpGet]
        public async Task<ActionResult> EditorialPostDetail(int? postID)
        {
            if (postID == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Editorial list = await dbcontext.Editorials.FindAsync(postID);
            if (list == null)
            {
                return HttpNotFound();
            }
            return View(list);
        }

        public async Task<ActionResult> EditorialPostEdit(int? postID)
        {
            if (postID == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Editorial allpost = await dbcontext.Editorials.FindAsync(postID);
            if (allpost == null)
            {
                return HttpNotFound();
            }
            return View(allpost);
        }

        [HttpPost, ValidateInput(false)]
        public ActionResult EditorialPostEdit(Editorial model, HttpPostedFileBase FileUpload)
        {
            string filesnames = dbcontext.Editorials.Where(i => i.EditorialID == model.EditorialID).Select(i => i.EditorialImage).FirstOrDefault();
            filesnames = filesnames == null ? "default.jpg" : filesnames;

            if (FileUpload != null)
            {
                if (!System.IO.Directory.Exists(Server.MapPath("\\Content\\Uploads\\Editorial" + 1)))
                {
                    string savedFileName = DateTime.Now.ToString("MMddHHmmssfff") + Path.GetFileName(FileUpload.FileName);
                    FileUpload.SaveAs(Path.Combine(Server.MapPath("~/Content/Uploads/Editorial"), savedFileName)); // Save the file
                    filesnames = savedFileName;
                }
            }
            Editorial updatePost = dbcontext.Editorials.Find(model.EditorialID);
            if (updatePost.EditorialID == model.EditorialID)
            {
                updatePost.Title = model.Title;
                updatePost.Description = model.Description;
                updatePost.EditorialImage = filesnames;
                updatePost.isPublished = true;
                updatePost.isDeleted = false;
                updatePost.ModifiedDate = DateTime.Now;
            }
            dbcontext.Editorials.AddOrUpdate(updatePost);
            dbcontext.SaveChanges();
            TempData["msg"] = "<script>alert('Updated Successfully');</script>";
            return RedirectToAction("EditorialPostList", "Administrator");
        }

        [HttpGet]
        public async Task<ActionResult> EditorialPostDelete(int? postID)
        {
            if (postID == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Editorial _delete = await dbcontext.Editorials.FindAsync(postID);

            var filename = dbcontext.Editorials.Where(p => p.EditorialID == postID).Select(i => i.EditorialImage).FirstOrDefault();
            var filePath = Server.MapPath("~/Content/Uploads/Editorial/" + filename);
            if (System.IO.File.Exists(filePath))
            {
                System.IO.File.Delete(filePath);
            }

            ///delete data in Likes, Comments, replies table w.r.t Post
            var _likes = dbcontext.Likes.Where(id => id.PostID == postID && id.SectionID == 7);
            var _comments = dbcontext.Comments.Where(id => id.PostID == postID && id.SectionID == 7);
            var _replies = dbcontext.ReplyComments.Where(id => id.PostID == postID && id.SectionID == 7);
            if (_likes != null || _comments != null || _replies != null)
            {
                dbcontext.Likes.RemoveRange(_likes);
                dbcontext.Comments.RemoveRange(_comments);
                dbcontext.ReplyComments.RemoveRange(_replies);
                dbcontext.Editorials.Remove(_delete);
            }
            else
            {
                dbcontext.Editorials.Remove(_delete);
            }
            dbcontext.SaveChanges();
            if (_delete == null)
            {
                return HttpNotFound();
            }
            TempData["msg"] = "<script>alert('Deleted Successfully');</script>";
            return RedirectToAction("EditorialPostList", "Administrator");
        }


        ///////////////////////////////////////////////////////////////////////////////////////////////
        ////////////////////////////                                ///////////////////////////////////
        ///////////////////////////  Editorial Post end        ////////////////////////////////
        ///////////////////////////                                 ///////////////////////////////////
        ///////////////////////////////////////////////////////////////////////////////////////////////


        ///////////////////////////////////////////////////////////////////////////////////////////////
        ////////////////////////////                                ///////////////////////////////////
        ///////////////////////////  New Notification Post Start        ///////////////////////////////
        ///////////////////////////                                 ///////////////////////////////////
        ///////////////////////////////////////////////////////////////////////////////////////////////

        [HttpGet]
        public ActionResult NotificationPost()
        {
            return View();
        }

        [HttpPost, ValidateInput(false)]
        public ActionResult NotificationPost(Notification model, HttpPostedFileBase FileUpload)
        {
            string filesnames = "default.pdf";

            if (FileUpload != null)
            {
                if (!System.IO.Directory.Exists(Server.MapPath("\\Content\\Uploads\\Notification" + 1)))
                {
                    string savedFileName = DateTime.Now.ToString("MMddHHmmssfff") + Path.GetFileName(FileUpload.FileName);
                    FileUpload.SaveAs(Path.Combine(Server.MapPath("~/Content/Uploads/Notification"), savedFileName)); // Save the file
                    filesnames = savedFileName;
                }
            }
            Notification insert = new Notification()
            {
                Title = model.Title,
                Description = model.Description,
                Attachment = filesnames,
                Published = true,
                Deleted = false,
                Likes = 0,
                Comments = 0,
                CreatedDate = DateTime.Now,
            };
            dbcontext.Notifications.Add(insert);
            int c = dbcontext.SaveChanges();
            TempData["msg"] = "<script>alert('Added Successfully');</script>";
            return RedirectToAction("NotificationPostList", "Administrator");
        }

        //[Authorize(Roles = "Admin")]
        [HttpGet]
        public ActionResult NotificationPostList()
        {
            var list = dbcontext.Notifications.Where(p => p.Published == true).OrderBy(c => c.CreatedDate).ToList();
            return View(list);
        }

        public async Task<ActionResult> NotificationPostEdit(int? postID)
        {
            if (postID == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Notification allpost = await dbcontext.Notifications.FindAsync(postID);
            if (allpost == null)
            {
                return HttpNotFound();
            }
            return View(allpost);
        }

        [HttpPost, ValidateInput(false)]
        public ActionResult NotificationPostEdit(Notification model, HttpPostedFileBase FileUpload)
        {
            string filesnames = dbcontext.Notifications.Where(i => i.ID == model.ID).Select(i => i.Attachment).FirstOrDefault();
            filesnames = filesnames == null ? "default.pdf" : filesnames;

            if (FileUpload != null)
            {
                if (!System.IO.Directory.Exists(Server.MapPath("\\Content\\Uploads\\PersonalityDevlopment" + 1)))
                {
                    string savedFileName = DateTime.Now.ToString("MMddHHmmssfff") + Path.GetFileName(FileUpload.FileName);
                    FileUpload.SaveAs(Path.Combine(Server.MapPath("~/Content/Uploads/PersonalityDevlopment"), savedFileName)); // Save the file
                    filesnames = savedFileName;
                }
            }
            Notification updatePost = dbcontext.Notifications.Find(model.ID);
            if (updatePost.ID == model.ID)
            {
                updatePost.Title = model.Title;
                updatePost.Description = model.Description;
                updatePost.Attachment = filesnames;
                updatePost.Published = true;
                updatePost.Deleted = false;
                updatePost.ModifiedDate = DateTime.Now;
            }
            dbcontext.Notifications.AddOrUpdate(updatePost);
            dbcontext.SaveChanges();
            TempData["msg"] = "<script>alert('Updated Successfully');</script>";
            return RedirectToAction("NotificationPostList", "Administrator");
        }

        public ActionResult NotificationPostDelete(int? postID)
        {
            if (postID == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Notification delete = dbcontext.Notifications.Find(postID);
            if (delete == null)
            {
                return HttpNotFound();
            }
            return View(delete);
        }

        [HttpPost]
        public ActionResult NotificationPostDelete(int postID)
        {
            Notification delete = dbcontext.Notifications.Find(postID);

            var filename = dbcontext.Notifications.Where(p => p.ID == postID).Select(i => i.Attachment).FirstOrDefault();
            var filePath = Server.MapPath("~/Content/Uploads/Notification/" + filename);
            if (System.IO.File.Exists(filePath))
            {
                System.IO.File.Delete(filePath);
            }

            dbcontext.Notifications.Remove(delete);
            dbcontext.SaveChanges();
            TempData["msg"] = "<script>alert('Deleted Successfully');</script>";
            return RedirectToAction("NotificationPostList", "Administrator");
        }


        ///////////////////////////////////////////////////////////////////////////////////////////////
        ////////////////////////////                                ///////////////////////////////////
        ///////////////////////////  New Notification Post End        /////////////////////////////////
        ///////////////////////////                                 ///////////////////////////////////
        ///////////////////////////////////////////////////////////////////////////////////////////////





        ///////////////////////////////////////////////////////////////////////////////////////////////
        ////////////////////////////                                ///////////////////////////////////
        ///////////////////////////  Learn Category Begin                 /////////////////////////////
        ///////////////////////////                                 ///////////////////////////////////
        ///////////////////////////////////////////////////////////////////////////////////////////////

        public void BindLearn()
        {
            List<Learn> category = dbcontext.Learns.Where(x => x.ID != 0 && x.Active == true).ToList();
            Learn c = new Learn();
            c.Name = "Root";
            c.ID = 0;
            category.Insert(0, c);
            SelectList categorydata = new SelectList(category, "Id", "Name", 0);
            ViewBag.Categories = categorydata;
        }

        [HttpGet]
        public ActionResult Learn()
        {
            BindLearn();
            return View();
        }

        [HttpPost]
        public ActionResult Learn(Learn model, HttpPostedFileBase FileUpload)
        {
            BindLearn();
            string filesnames = "test.png";

            if (FileUpload != null)
            {
                if (!System.IO.Directory.Exists(Server.MapPath("\\Content\\Uploads\\Tab\\SubTab" + 1)))
                {
                    string savedFileName = Path.GetFileName(FileUpload.FileName);
                    FileUpload.SaveAs(Path.Combine(Server.MapPath("~/Content/Uploads/Tab/SubTab"), savedFileName)); // Save the file
                    filesnames = savedFileName;
                }
            }
            Learn insert = new Learn()
            {
                Name = model.Name,
                Active = model.Active,
                ParentID = model.ParentID == 0 ? null : model.ParentID,
                IconImage = "SubTab/" + filesnames,
                CreatedDate = DateTime.Now
            };
            try
            {
                dbcontext.Learns.Add(insert);
                dbcontext.SaveChanges();

            }
            catch (DbEntityValidationException dbEx)
            {
                foreach (var validationErrors in dbEx.EntityValidationErrors)
                {
                    foreach (var validationError in validationErrors.ValidationErrors)
                    {
                        System.Console.WriteLine("Property: {0} Error: {1}", validationError.PropertyName, validationError.ErrorMessage);
                    }
                }
            }
            return RedirectToAction("LearnList", "Administrator");
        }

        [HttpGet]
        public ActionResult LearnList()
        {
            var listCat = dbcontext.Learns.Where(p => p.ID != 0).ToList();
            return View(listCat);
        }

        //[Authorize(Roles = "Admin")]
        public async Task<ActionResult> LearnEdit(int? catID)
        {
            BindCat();
            if (catID == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Learn cate = await dbcontext.Learns.FindAsync(catID);
            if (cate == null)
            {
                return HttpNotFound();
            }
            return View(cate);
        }

        [HttpPost]
        public ActionResult LearnEdit(Learn model, HttpPostedFileBase FileUpload)
        {
            BindCat();
            string filesnames = dbcontext.Learns.Where(i => i.ID == model.ID).Select(i => i.IconImage).FirstOrDefault();
            filesnames = filesnames == null ? "test.png" : filesnames;

            if (FileUpload != null)
            {
                if (!System.IO.Directory.Exists(Server.MapPath("\\Content\\Uploads\\Tab\\SubTab" + 1)))
                {
                    string savedFileName = Path.GetFileName(FileUpload.FileName);
                    FileUpload.SaveAs(Path.Combine(Server.MapPath("~/Content/Uploads/Tab/SubTab"), savedFileName)); // Save the file
                    filesnames = savedFileName;
                }
            }
            Learn updatecate = dbcontext.Learns.Find(model.ID);
            if (updatecate.ID == model.ID)
            {
                updatecate.Name = model.Name;
                updatecate.ParentID = model.ParentID == 0 ? null : model.ParentID;
                updatecate.Active = model.Active;
                updatecate.IconImage = "SubTab/" + filesnames;
                updatecate.ModifiedDate = DateTime.Now;
            }
            dbcontext.Learns.AddOrUpdate(updatecate);
            int c = dbcontext.SaveChanges();
            TempData["msg"] = "<script>alert('Updated Successfully');</script>";
            return RedirectToAction("LearnList", "Administrator");
        }

        public async Task<ActionResult> LearnActivate(int? catID)
        {
            if (catID == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Learn update = await dbcontext.Learns.FindAsync(catID);
            if (update.ID == catID && update.Active != true)
            {
                update.Active = true;
            }
            else
            {
                update.Active = false;
            }
            dbcontext.Learns.AddOrUpdate(update);
            dbcontext.SaveChanges();
            TempData["msg"] = "<script>alert('Updated Successfully');</script>";
            return RedirectToAction("LearnList", "Administrator");
        }

        //[Authorize(Roles = "Admin")]
        [HttpGet]
        public ActionResult LearnDelete(Learn model, int? catID)
        {
            var associatedPosts = (from cat in dbcontext.Learns
                                   join catpost in dbcontext.LearnPosts on cat.ID equals catpost.SubID
                                   where cat.ID == catID
                                   select new PostCategory
                                   {
                                       PostId = catpost.ID,
                                       CategoryName = cat.Name,
                                       Title = catpost.Title,
                                       DefaultImage = catpost.DefaultImage,
                                       CreatedDate = catpost.CreatedDate,
                                   }).OrderBy(c => c.CreatedDate).ToList();
            return View(associatedPosts);
        }

        [HttpPost]
        public ActionResult LearnDelete(int? catID)
        {
            Learn _parent = dbcontext.Learns.Find(catID);
            var _child = dbcontext.LearnPosts.Where(category => category.SubID == catID);
            if (_child != null)
            {
                dbcontext.LearnPosts.RemoveRange(_child);
                dbcontext.Learns.Remove(_parent);
            }
            else
            {
                dbcontext.Learns.Remove(_parent);
            }
            dbcontext.SaveChanges();
            TempData["msg"] = "<script>alert('Deleted Successfully');</script>";
            return RedirectToAction("LearnList", "Administrator");
        }

        ///////////////////////////////////////////////////////////////////////////////////////////////
        ////////////////////////////                                ///////////////////////////////////
        ///////////////////////////  Learn Category End                 ///////////////////////////////
        ///////////////////////////                                 ///////////////////////////////////
        ///////////////////////////////////////////////////////////////////////////////////////////////





        ///////////////////////////////////////////////////////////////////////////////////////////////
        ////////////////////////////                                ///////////////////////////////////
        ///////////////////////////  Learn Post Begin                 /////////////////////////////////
        ///////////////////////////                                 ///////////////////////////////////
        ///////////////////////////////////////////////////////////////////////////////////////////////

        public void BindLearnPost()
        {
            List<Learn> Talent = dbcontext.Learns.Where(x => x.ID != 0 && x.Active == true).ToList();
            Learn h = new Learn();
            h.Name = "--Select Category--";
            h.ID = 0;
            Talent.Insert(0, h);
            SelectList hdata = new SelectList(Talent, "ID", "Name", 0);
            ViewBag.LearnPost = hdata;
        }


        [HttpGet]
        public ActionResult LearnPost()
        {
            BindLearnPost();
            return View();
        }

        [HttpPost, ValidateInput(false)]
        public ActionResult LearnPost(LearnPost model, HttpPostedFileBase FileUpload)
        {
            BindTalentPost();
            string filesnames = "default.jpg";

            if (FileUpload != null)
            {
                if (!System.IO.Directory.Exists(Server.MapPath("\\Content\\Uploads\\Learn" + 1)))
                {
                    string savedFileName = DateTime.Now.ToString("MMddHHmmssfff") + Path.GetFileName(FileUpload.FileName);
                    FileUpload.SaveAs(Path.Combine(Server.MapPath("~/Content/Uploads/Learn"), savedFileName)); // Save the file
                    filesnames = savedFileName;
                }
            }
            LearnPost insert = new LearnPost()
            {
                Title = model.Title,
                SubID = model.SubID,
                Description = model.Description,
                DefaultImage = filesnames,
                Published = true,
                Deleted = false,
                Likes = 0,
                Comments = 0,
                CreatedDate = DateTime.Now,
            };
            dbcontext.LearnPosts.Add(insert);
            int c = dbcontext.SaveChanges();
            TempData["msg"] = "<script>alert('Added Successfully');</script>";
            return RedirectToAction("LearnPostList", "Administrator");
        }

        //[Authorize(Roles = "Admin")]
        [HttpGet]
        public ActionResult LearnPostList()
        {
            var list = (from postdata in dbcontext.LearnPosts
                        join catdata in dbcontext.Learns on postdata.SubID equals catdata.ID
                        where postdata.Deleted != true
                        select new PostCategory
                        {
                            PostId = postdata.ID,
                            CategoryId = (int)postdata.SubID,
                            CategoryName = catdata.Name,
                            Title = postdata.Title,
                            Description = postdata.Description,
                            DefaultImage = postdata.DefaultImage,
                            IsPublished = (bool)postdata.Published,
                            PublishedDate = postdata.CreatedDate,
                            ModifyDate = postdata.ModifiedDate,
                        }).OrderByDescending(x => x.PublishedDate).ToList<PostCategory>();
            return View(list);
        }

        public async Task<ActionResult> LearnPostEdit(int? postID)
        {
            BindLearnPost();
            if (postID == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            LearnPost allpost = await dbcontext.LearnPosts.FindAsync(postID);
            if (allpost == null)
            {
                return HttpNotFound();
            }
            return View(allpost);
        }

        [HttpPost, ValidateInput(false)]
        public ActionResult LearnPostEdit(LearnPost model, HttpPostedFileBase FileUpload)
        {
            BindLearnPost();

            string filesnames = dbcontext.LearnPosts.Where(i => i.ID == model.ID).Select(i => i.DefaultImage).FirstOrDefault();
            filesnames = filesnames == null ? "default.jpg" : filesnames;

            if (FileUpload != null)
            {
                if (!System.IO.Directory.Exists(Server.MapPath("\\Content\\Uploads\\Learn" + 1)))
                {
                    string savedFileName = DateTime.Now.ToString("MMddHHmmssfff") + Path.GetFileName(FileUpload.FileName);
                    FileUpload.SaveAs(Path.Combine(Server.MapPath("~/Content/Uploads/Learn"), savedFileName)); // Save the file
                    filesnames = savedFileName;
                }
            }
            LearnPost updatePost = dbcontext.LearnPosts.Find(model.ID);
            if (updatePost.ID == model.ID)
            {
                updatePost.SubID = model.SubID;
                updatePost.Title = model.Title;
                updatePost.Description = model.Description;
                updatePost.DefaultImage = filesnames;
                updatePost.Published = true;
                updatePost.Deleted = false;
                updatePost.ModifiedDate = DateTime.Now;
            }
            dbcontext.LearnPosts.AddOrUpdate(updatePost);
            dbcontext.SaveChanges();
            TempData["msg"] = "<script>alert('Updated Successfully');</script>";
            return RedirectToAction("LearnPostList", "Administrator");
        }

        [HttpGet]
        public ActionResult LearnPostDelete(int? postID)
        {
            BindLearnPost();
            if (postID == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            LearnPost delete = dbcontext.LearnPosts.Find(postID);
            if (delete == null)
            {
                return HttpNotFound();
            }
            return View(delete);
        }

        [HttpPost]
        public ActionResult LearnPostDelete(int postID)
        {
            LearnPost delete = dbcontext.LearnPosts.Find(postID);

            var filename = dbcontext.LearnPosts.Where(p => p.ID == postID).Select(i => i.DefaultImage).FirstOrDefault();
            var filePath = Server.MapPath("~/Content/Uploads/Learn/" + filename);
            if (System.IO.File.Exists(filePath))
            {
                System.IO.File.Delete(filePath);
            }

            ///delete data in Likes, Comments, replies table w.r.t Post
            var _likes = dbcontext.Likes.Where(id => id.PostID == postID && id.SectionID == 5);
            var _comments = dbcontext.Comments.Where(id => id.PostID == postID && id.SectionID == 5);
            var _replies = dbcontext.ReplyComments.Where(id => id.PostID == postID && id.SectionID == 5);
            if (_likes != null || _comments != null || _replies != null)
            {
                dbcontext.Likes.RemoveRange(_likes);
                dbcontext.Comments.RemoveRange(_comments);
                dbcontext.ReplyComments.RemoveRange(_replies);
                dbcontext.LearnPosts.Remove(delete);
            }
            else
            {
                dbcontext.LearnPosts.Remove(delete);
            }
            dbcontext.SaveChanges();
            TempData["msg"] = "<script>alert('Deleted Successfully');</script>";
            return RedirectToAction("LearnPostList", "Administrator");
        }

        ///////////////////////////////////////////////////////////////////////////////////////////////
        ////////////////////////////                                ///////////////////////////////////
        ///////////////////////////  Learn Post End                 ///////////////////////////////////
        ///////////////////////////                                 ///////////////////////////////////
        ///////////////////////////////////////////////////////////////////////////////////////////////




        ///////////////////////////////////////////////////////////////////////////////////////////////
        ////////////////////////////                                ///////////////////////////////////
        ///////////////////////////  Career Post Begin                 ////////////////////////////////
        ///////////////////////////                                 ///////////////////////////////////
        ///////////////////////////////////////////////////////////////////////////////////////////////


        [HttpGet]
        public ActionResult CareerPost()
        {
            return View();
        }

        [HttpPost]
        public ActionResult CareerPost(Career _career)
        {
            Career insert = new Career()
            {
                JobName = _career.JobName,
                Active = _career.Active,
                CreatedDate = DateTime.Now
            };
            dbcontext.Careers.Add(insert);
            dbcontext.SaveChanges();
            TempData["msg"] = "<script>alert('Succesfully Added');</script>";
            return RedirectToAction("CareerPostList", "Administrator");
        }

        public ActionResult CareerPostList()
        {
            var list = dbcontext.Careers.Where(job => job.ID != 0).ToList();
            return View(list);
        }

        public async Task<ActionResult> CareerActivate(int? careerID)
        {
            if (ModelState.IsValid)
            {
                Career update = await dbcontext.Careers.FindAsync(careerID);
                if (update.ID == careerID && update.Active != true)
                {
                    update.Active = true;
                }
                else
                {
                    update.Active = false;
                }
                dbcontext.Careers.AddOrUpdate(update);
                int c = dbcontext.SaveChanges();
            }
            TempData["msg"] = "<script>alert('Updated Successfully');</script>";
            return RedirectToAction("CareerPostList", "Administrator");
        }

        public async Task<ActionResult> CareerDelete(int? careerID)
        {
            if (careerID == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Career allpost = await dbcontext.Careers.FindAsync(careerID);
            if (allpost != null)
            {
                dbcontext.Careers.Remove(allpost);
                dbcontext.SaveChanges();
            }
            if (allpost == null)
            {
                return HttpNotFound();
            }
            TempData["msg"] = "<script>alert('Deleted Successfully');</script>";
            return RedirectToAction("CareerPostList", "Administrator");
        }


        ///////////////////////////////////////////////////////////////////////////////////////////////
        ////////////////////////////                                ///////////////////////////////////
        ///////////////////////////  Career Post Begin                 ////////////////////////////////
        ///////////////////////////                                 ///////////////////////////////////
        ///////////////////////////////////////////////////////////////////////////////////////////////





        ///////////////////////////////////////////////////////////////////////////////////////////////
        ////////////////////////////                                ///////////////////////////////////
        ///////////////////////////  Greetings Post Begin                 ////////////////////////////////
        ///////////////////////////                                 ///////////////////////////////////
        ///////////////////////////////////////////////////////////////////////////////////////////////


        [HttpGet]
        public ActionResult Greetings()
        {
            return View();
        }

        [HttpPost]
        public ActionResult Greetings(Greeting _greet)
        {
            Greeting insert = new Greeting()
            {
                Quote = _greet.Quote,
                Active = _greet.Active,
                CreatedDate = DateTime.Now
            };
            dbcontext.Greetings.Add(insert);
            dbcontext.SaveChanges();
            TempData["msg"] = "<script>alert('Succesfully Added');</script>";
            return RedirectToAction("GreetingsList", "Administrator");
        }

        public ActionResult GreetingsList()
        {
            var list = dbcontext.Greetings.Where(greet => greet.ID != 0).ToList();
            return View(list);
        }

        public async Task<ActionResult> GreetingsActivate(int? _greetID)
        {
            if (ModelState.IsValid)
            {
                Greeting update = await dbcontext.Greetings.FindAsync(_greetID);
                if (update.ID == _greetID && update.Active != true)
                {
                    update.Active = true;
                }
                else
                {
                    update.Active = false;
                }
                dbcontext.Greetings.AddOrUpdate(update);
                int c = dbcontext.SaveChanges();
            }
            TempData["msg"] = "<script>alert('Updated Successfully');</script>";
            return RedirectToAction("GreetingsList", "Administrator");
        }

        public async Task<ActionResult> GreetingsDelete(int? _greetID)
        {
            if (_greetID == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Greeting allpost = await dbcontext.Greetings.FindAsync(_greetID);
            if (allpost != null)
            {
                dbcontext.Greetings.Remove(allpost);
                dbcontext.SaveChanges();
            }
            if (allpost == null)
            {
                return HttpNotFound();
            }
            TempData["msg"] = "<script>alert('Deleted Successfully');</script>";
            return RedirectToAction("GreetingsList", "Administrator");
        }


        ///////////////////////////////////////////////////////////////////////////////////////////////
        ////////////////////////////                                ///////////////////////////////////
        ///////////////////////////  Career Post Begin                 ////////////////////////////////
        ///////////////////////////                                 ///////////////////////////////////
        ///////////////////////////////////////////////////////////////////////////////////////////////




        ///////////////////////////////////////////////////////////////////////////////////////////////
        ///////////////////////////////                                ////////////////////////////////
        ///////////////////////////  User Profile Start        ////////////////////////////////////////
        ///////////////////////////                                 ///////////////////////////////////
        ///////////////////////////////////////////////////////////////////////////////////////////////

        //OnlineTest
        [HttpGet]
        public ActionResult AdminOnlineTest()
        {
            return View();
        }


        [HttpGet]
        public ActionResult RegisteredUsers()
        {
            //var list = dbcontext.Profiles.Where(id => id.ProfileID != 0).ToList();
            //var list = (from profile in dbcontext.Profiles
            //            where profile.ProfileID != 0
            //            select new UserProfile
            //            {
            //                ProfileID = profile.ProfileID,
            //                FirstName = profile.FirstName,
            //                MobileNo = profile.MobileNo,
            //                SubscribedPlan = (int)profile.SubscribedPlan,
            //                isActive = profile.isActive,
            //                CreatedDate = profile.CreatedDate
            //            }).OrderByDescending(i => i.ProfileID).ToList();
            var list=(from a in dbcontext.AspNetUsers
                      join b in dbcontext.Profiles on a.Email equals b.UserName
                      select new UserProfile
                      {
                          ProfileID = b.ProfileID,
                          FirstName = b.FirstName,
                          MobileNo = b.MobileNo,
                          SubscribedPlan = (int)b.SubscribedPlan,
                          isActive = b.isActive,
                         CreatedDate = b.CreatedDate
                      }).OrderByDescending(i => i.ProfileID).ToList();



            TempData["RegisteredUsers"] = list;
            return View(list);
        }

        [HttpPost]
        public ActionResult RegisteredUsers(string Message)
        {
            var ActiveUsersList = dbcontext.Profiles.Where(p => p.isActive == true).Select(u => u.UserName).ToList();

            SmtpClient smtp;

            string from = "technical@shlrtechnosoft.in";

            using (MailMessage mail = new MailMessage())
            {
                try
                {
                    mail.From = new MailAddress(from);
                    //mail.To.Add(toEmail);
                    for (int i = 0; i < ActiveUsersList.Count(); i++)
                    {
                        mail.To.Add(new MailAddress((string)ActiveUsersList[i]));
                        mail.Subject = "SJSMAG";
                        mail.Body = "Message : " + Message + Environment.NewLine;

                        mail.IsBodyHtml = false;
                    }
                    smtp = new SmtpClient();

                    //smtp.Host = "relay-hosting.secureserver.net";
                    smtp.Host = "dedrelay.secureserver.net";
                    smtp.Port = 25;

                    smtp.Credentials = new System.Net.NetworkCredential
                    ("technical@shlrtechnosoft.in", "Technical@123");

                    smtp.EnableSsl = false;
                    //smtp.EnableSsl = true;

                    smtp.Send(mail);
                    TempData["msg"] = "<script>alert('Mail Sent Successfully... :)');</script>";
                    Response.Write("<script>alert('Mail Sent Successfully... :)')</script>");
                }
                catch (Exception ex)
                {
                    var Error = ex;
                    TempData["msg"] = "<script>alert('Email Sending Failed :(');</script>";
                    Response.Write("<script>alert('Email Sending Failed :(')</script>");
                }
            }

            var list = TempData["RegisteredUsers"] as List<UserProfile>;
            TempData.Keep();

            return View(list);
        }

        [HttpGet]
        public async Task<ActionResult> UserActivate(int? Userid)
        {
            if (Userid == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Profile update = await dbcontext.Profiles.FindAsync(Userid);
            if (update.ProfileID == Userid && update.isActive != true)
            {
                update.isActive = true;
                update.CreatedDate = DateTime.Now;
            }
            else
            {
                update.isActive = false;
            }
            dbcontext.Profiles.AddOrUpdate(update);
            dbcontext.SaveChanges();
            TempData["msg"] = "<script>alert('Updated Successfully');</script>";
            return RedirectToAction("RegisteredUsers", "Administrator");
        }

        public void bindSubscription()
        {
            List<Subscription> subscription = dbcontext.Subscriptions.Where(active => active.Active == true).ToList();
            Subscription s = new Subscription();
            s.SubscriptionID = 0;
            s.Name = "--Choose You Subscription";
            subscription.Insert(0, s);
            SelectList SubBag = new SelectList(subscription, "SubscriptionID", "Name", 0);
            ViewBag.SubBag = SubBag;
        }


        [HttpGet]
        public ActionResult UserDetail(int? Userid)
        {
            bindSubscription();
            var RegUser = dbcontext.Profiles.Where(u => u.ProfileID == Userid).FirstOrDefault();

            int EndDays = new SubcriptionDetail(Userid).EndDays;
            ViewBag.data = EndDays;

            return View(RegUser);
        }

        [HttpPost]
        public ActionResult UserDetail(Profile _model)
        {
            bindSubscription();

            Profile PlanUpdate = dbcontext.Profiles.Find(_model.ProfileID);
            if (PlanUpdate != null)
            {
                PlanUpdate.SubscribedPlan = _model.SubscribedPlan;
            }
            dbcontext.Profiles.AddOrUpdate(PlanUpdate);
            dbcontext.SaveChanges();

            var RegUser = dbcontext.Profiles.Where(u => u.ProfileID == _model.ProfileID).FirstOrDefault();
            int EndDays = new SubcriptionDetail(_model.ProfileID).EndDays;
            ViewBag.data = EndDays;
            TempData["msg"] = "<script>alert('Succesfully Updated');</script>";
            return View(RegUser);
        }

        public ActionResult AlertUser(int? Userid)
        {
            var User = dbcontext.Profiles.Where(i => i.ProfileID == Userid).SingleOrDefault();
            int EndDays = new SubcriptionDetail(Userid).EndDays;

            SmtpClient smtp;
            string from = "technical@shlrtechnosoft.in";

            using (MailMessage mail = new MailMessage())
            {
                try
                {
                    mail.From = new MailAddress(from);
                    mail.To.Add(User.UserName);

                    mail.Subject = "SJSMAG - Subscription Alert";
                    mail.Body = "Dear " + User.FirstName + "," + Environment.NewLine +
                                "This mail is to inform you regarding your Subscribed Plan Ending." + Environment.NewLine +
                                "Your Subscription will end in " + EndDays + " Days." + Environment.NewLine +
                                "Please Buy the Package within the due date.";

                    mail.IsBodyHtml = false;

                    smtp = new SmtpClient();

                    //smtp.Host = "relay-hosting.secureserver.net";
                    smtp.Host = "dedrelay.secureserver.net";
                    //smtp.Host = "smtp.gmail.com";

                    smtp.Port = 25;

                    smtp.Credentials = new System.Net.NetworkCredential
                    ("technical@shlrtechnosoft.in", "Technical@123");

                    smtp.EnableSsl = false;
                    //smtp.EnableSsl = true;

                    smtp.Send(mail);
                    TempData["msg"] = "<script>alert('Alert Email Sent Successfully');</script>";
                    Response.Write("<script>alert('Alert Email Sent Successfully!')</script>");
                }
                catch (Exception ex)
                {
                    var Error = ex;
                    TempData["msg"] = "<script>alert('Email Sending Failed :(')</script>";
                    Response.Write("<script>alert('Email Sending Failed !')</script>");
                }

            }

            var list = TempData["RegisteredUsers"] as List<UserProfile>;
            return RedirectToAction("RegisteredUsers", "Administrator", list);
        }

        ///////////////////////////////////////////////////////////////////////////////////////////////
        ///////////////////////////////                                ////////////////////////////////
        ///////////////////////////    User Profile End                ////////////////////////////////
        ///////////////////////////                                 ///////////////////////////////////
        ///////////////////////////////////////////////////////////////////////////////////////////////



        ////////////////////////////Subsription
        ///
        [HttpGet]
        public ActionResult SubsriptionAdd()
        {
            var list = dbcontext.Subscriptions.Where(s => s.SubscriptionID != 0).OrderBy(c => c.CreatedDate).ToList();
            ViewBag.SubsriptionList = list;

            return View();
        }

        [HttpPost]
        public ActionResult SubsriptionAdd(Subscription _subscription)
        {
            Subscription insert = new Subscription()
            {
                Name = _subscription.Name,
                Months = _subscription.Months,
                Active = _subscription.Active,
                CreatedDate = DateTime.Now
            };
            dbcontext.Subscriptions.Add(insert);
            dbcontext.SaveChanges();
            TempData["msg"] = "<script>alert('Added Successfully');</script>";

            var list = dbcontext.Subscriptions.Where(s => s.SubscriptionID != 0).OrderBy(c => c.CreatedDate).ToList();
            ViewBag.SubsriptionList = list;

            return View();
            //return RedirectToAction("SubsriptionList", "Administrator");
        }

        [HttpGet]
        [Authorize(Roles = "Admin")]
        public ActionResult SubsriptionList()
        {
            var list = dbcontext.Subscriptions.Where(s => s.SubscriptionID != 0).OrderBy(c => c.CreatedDate).ToList();
            return View(list);
        }


        //-----------------------------------------------Video Section Starts--------------------------------------------------------------------

        [HttpGet]
        public ActionResult AddVideo()
        {
            return View();
        }

        [HttpPost]
        public ActionResult AddVideo(Video model, HttpPostedFileBase FileUpload)
        {
            string filesnames = "";
            var Stringimages = new List<string>();
            String id = model.Name;
            if (FileUpload != null)
            {
                if (!System.IO.Directory.Exists(Server.MapPath("\\Content\\Uploads\\Videos" + id)))
                {
                    string savedFileName = DateTime.Now.ToString("MMddHHmmssfff") + Path.GetFileName(FileUpload.FileName);
                    FileUpload.SaveAs(Path.Combine(Server.MapPath("~/Content/Uploads/Videos"), savedFileName)); // Save the file
                    Stringimages.Add(savedFileName);

                    filesnames = string.Join("|||", Stringimages.ToArray());
                }
                else
                {
                    ViewBag.msg = "Following File  Already Exists " + FileUpload.FileName + ". ";
                }

            }
            else
            {
                ViewBag.msg = "File not uploaded";
            }

            Video insert = new Video()
            {
                Name = model.Name,
                Description = model.Description,
                VideoPath = filesnames,
                IsPublished = true,
                CreatedDate = DateTime.Now
            };
            dbcontext.Videos.Add(insert);
            int c = dbcontext.SaveChanges();
            if (c > 0)
            {
                ViewBag.msg = "Video Sucessfully Saved";
            }
            else
            {
                ViewBag.msg = "Oops.. ! try again";
            }
            //return RedirectToAction("ListPost", "Admin");
            return View();
        }

        ///List Videos
        public ActionResult ListVideo()
        {
            var listAll = dbcontext.Videos.OrderByDescending(x => x.CreatedDate).ToList();
            return View(listAll);
        }

        public async Task<ActionResult> EditVideo(int? videoID)
        {
            var VideoImg = (from videodata in dbcontext.Videos where videodata.ID == videoID select videodata.VideoPath).FirstOrDefault();
            ViewBag.VideoImgages = VideoImg;

            if (videoID == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            Video allvideo = await dbcontext.Videos.FindAsync(videoID);
            if (allvideo == null)
            {
                return HttpNotFound();
            }
            return View(allvideo);
        }

        [HttpPost]
        public ActionResult EditVideo(Video model, HttpPostedFileBase FileUpload)
        {
            var VideoImg = (from videodata in dbcontext.Videos where videodata.ID == model.ID select videodata.VideoPath).FirstOrDefault();
            ViewBag.VideoImgages = VideoImg;

            string filesnames = "";
            var Stringimages = new List<string>();

            String id = model.Name;
            if (FileUpload != null)
            {
                if (!System.IO.Directory.Exists(Server.MapPath("\\Content\\Uploads\\Videos" + id)))
                {
                    string savedFileName = DateTime.Now.ToString("MMddHHmmssfff") + Path.GetFileName(FileUpload.FileName);
                    FileUpload.SaveAs(Path.Combine(Server.MapPath("~/Content/Uploads/Videos"), savedFileName)); // Save the file
                    Stringimages.Add(savedFileName);

                    filesnames = string.Join("|||", Stringimages.ToArray());
                }
                else
                {
                    ViewBag.msg = "Following File is Already Exists " + FileUpload.FileName + ". ";
                }

            }
            else
            {
                ViewBag.msg = "File not uploaded";
            }

            var VideoImg1 = (from videodata in dbcontext.Videos where videodata.ID == model.ID select videodata.VideoPath).FirstOrDefault();
            ViewBag.VideoImgages = VideoImg1;

            Video updateVideo = dbcontext.Videos.Find(model.ID);
            if (updateVideo.ID == model.ID)
            {
                updateVideo.Name = model.Name;
                updateVideo.Description = model.Description;
                if (filesnames.Length > 0)
                {
                    updateVideo.VideoPath = filesnames;
                }
                else
                {
                    updateVideo.VideoPath = VideoImg;
                }
                updateVideo.IsPublished = true;
                updateVideo.ModifiedDate = DateTime.Now;
                updateVideo.IsPublished = false;
            }
            dbcontext.Videos.AddOrUpdate(updateVideo);
            int c = dbcontext.SaveChanges();
            if (c > 0)
            {
                ViewBag.msg = "Updated Sucessfully";
            }
            else
            {
                ViewBag.msg = "Oops.. ! try again";
            }
            int? vid = updateVideo.ID;
            //return View(updateVideo);
            return RedirectToAction("ListVideo", "Administrator");
        }

        [HttpGet]
        public async Task<ActionResult> DetailsVideo(int? videoID)
        {
            var VideoImg = (from videodata in dbcontext.Videos where videodata.ID == videoID select videodata.VideoPath).FirstOrDefault();
            ViewBag.VideoImages = VideoImg;

            if (videoID == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            Video allvideo = await dbcontext.Videos.FindAsync(videoID);
            if (allvideo == null)
            {
                return HttpNotFound();
            }
            return View(allvideo);
        }

        public async Task<ActionResult> DeleteVideo(int? videoID)
        {
            var VideoImg = (from videodata in dbcontext.Videos where videodata.ID == videoID select videodata.VideoPath).FirstOrDefault();
            ViewBag.VideoImages = VideoImg;

            if (videoID == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            Video allvideo = await dbcontext.Videos.FindAsync(videoID);
            if (allvideo.ID == videoID)
            {
                dbcontext.Videos.Remove(allvideo);
                int st = dbcontext.SaveChanges();
                if(st > 0)
                {
                    TempData["Msg"] = "Item Deleted Successfully..!";
                    return RedirectToAction("ListVideo", "Administrator");
                }
                else
                {
                    TempData["Msg"] = "Something Wrong..!";
                }
            }

            if (allvideo == null)
            {
                return HttpNotFound();
            }
            return View(allvideo);
        }

        //-----------------------------------------------Video Section Ends--------------------------------------------------------------------



        //-----------------------------------------------Delete Registered Users--------------------------------------------------------------------

        [HttpGet]
        public ActionResult Delete(int? id)
        {
            var data = dbcontext.Profiles.Where(a => a.ProfileID == id).Select(a => a).FirstOrDefault();
            var aspdata = dbcontext.AspNetUsers.Where(a => a.Email == data.UserName).FirstOrDefault();
            if (data != null)
            {
                dbcontext.Profiles.Remove(data);
                dbcontext.AspNetUsers.Remove(aspdata);
                int check = dbcontext.SaveChanges();
                if (check > 0)
                {
                    TempData["Msg"] = "Record Deleted Successfully..!";
                }
            }
            else
            {
                TempData["Msg"] = "Something Wrong.. Please Contact Admin..!";
            }
            return RedirectToAction("RegisteredUsers", "Administrator");
        }
        //-----------------------------------------------Delete Registered Users Ends--------------------------------------------------------------------

    }
}