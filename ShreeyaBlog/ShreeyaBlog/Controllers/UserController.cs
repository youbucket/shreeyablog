﻿using PagedList;
using ShreeyaBlog.Classes;
using ShreeyaBlog.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity.Migrations;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;

namespace ShreeyaBlog.Controllers
{
    public class UserController : Controller
    {
        ShreeyaBlogEntities dbcontext = new ShreeyaBlogEntities();
        // GET: User
        public ActionResult Index(Post model)
        {
            //int? data;
            if (TempData["ReamainingDays"] != null)
                ViewBag.data = (int)TempData["ReamainingDays"];
            TempData.Keep("ReamainingDays");

            DerivedIndex objectindex = new DerivedIndex();

            //Quote
            ViewBag.Greetings = objectindex.greeting();

            //Parallax 
            ViewBag.Parallax = objectindex.parallax();

            //editorial - get latest editorial
            ViewBag.getEditorial = objectindex.editorial();

            //Personality Section
            ViewBag.PersonalDevelopment = objectindex.personalDevelopment();

            //NewNotification Section
            ViewBag.NewNotification = objectindex.notification();

            //Images
            ViewBag.adds = objectindex.advertiseImage();

            //Latest wrt categories- get latest post(1) from all the categories
            ViewBag.LatestNews = objectindex.latestCategories();

            //Latest - get latest post(9) irrespective of category
            //ViewBag.LatestLiveBag = objectindex.latestPost();

            ////Categorywise Posts Begin
            ////Category

            ////Listing Category Names
            //var CategoryNames = dbcontext.Categories.Where(cat => cat.isActive == true).Select(category => category.CategoryName).ToArray();
            ////Listing Category IDs
            //var CategoryIDs = dbcontext.Categories.Where(cat => cat.isActive == true).Select(category => category.CategoryId).ToArray();

            ////Category 1 - get posts(6)
            //ViewBag.CategoryHeadingBag1 = CategoryNames.ElementAt(0);
            //ViewBag.category_oneBag = objectindex.categoryListTake6(CategoryIDs.ElementAt(0));

            ////Category 2 - get posts(3) from Category 3 [skip Category 1]
            //ViewBag.CategoryHeadingBag2 = CategoryNames.ElementAt(1);
            //ViewBag.category_twoBag = objectindex.categoryListTake3(CategoryIDs.ElementAt(1));

            ////Category 3 - get posts(3) from Category 3 [skip Category 1,Category 2]           
            //ViewBag.CategoryHeadingBag3 = CategoryNames.ElementAt(2);
            //ViewBag.category_threeBag = objectindex.categoryListTake3(CategoryIDs.ElementAt(2));

            ////Category 4 - get posts(3) from Category 4 [skip Category 1,Category 2,Category 3]
            //ViewBag.CategoryHeadingBag4 = CategoryNames.ElementAt(3);
            //ViewBag.category_fourBag = objectindex.categoryListTake3(CategoryIDs.ElementAt(3));

            ////Category 5 - get posts(3) from Category 5 [skip Category 1,Category 2,Category 3,Category 4]
            //ViewBag.CategoryHeadingBag5 = CategoryNames.ElementAt(4);
            //ViewBag.category_fiveBag = objectindex.categoryListTake3(CategoryIDs.ElementAt(4));

            ////Category 6 - get posts(3) from Category 6 [skip Category 1,Category 2,Category 3,Category 4,Category 5]
            //ViewBag.CategoryHeadingBag6 = CategoryNames.ElementAt(5);
            //ViewBag.category_sixBag = objectindex.categoryListTake3(CategoryIDs.ElementAt(5));

            ////Category 7 - get posts(3) from Category 7 [skip Category 1,Category 2,Category 3,Category 4,Category 5,Category 6]
            //ViewBag.CategoryHeadingBag7 = CategoryNames.ElementAt(6);
            //ViewBag.category_sevenBag = objectindex.categoryListTake3(CategoryIDs.ElementAt(6));

            ////Category 8 - get posts(3) from Category 8 [skip Category 1,Category 2,Category 3,Category 4,Category 5,Category 6,Category 7]
            //ViewBag.CategoryHeadingBag8 = CategoryNames.ElementAt(7);
            //ViewBag.category_eightBag = objectindex.categoryListTake3(CategoryIDs.ElementAt(7));

            ////Category 9 - get posts(2) from Category 9 [skip Category 1,Category 2,Category 3,Category 4,Category 5,Category 6,Category 7,Category 8]           
            //ViewBag.CategoryHeadingBag9 = CategoryNames.ElementAt(8);
            //ViewBag.category_nineBag = objectindex.categoryListTake2(CategoryIDs.ElementAt(8));

            ////Category 10 - get posts(2) from Category 10 [skip Category 1,Category 2,Category 3,Category 4,Category 5,Category 6,Category 7,Category 8,Category 9]
            //ViewBag.CategoryHeadingBag10 = CategoryNames.ElementAt(9);
            //ViewBag.category_tenBag = objectindex.categoryListTake2(CategoryIDs.ElementAt(9));

            ////Category 11 - get posts(6) from Category 11 [skip Category 1,Category 2,Category 3,Category 4,Category 5,Category 6,Category 7,Category 8,Category 9,Category 10]
            //ViewBag.CategoryHeadingBagGK = CategoryNames.ElementAt(10);
            //ViewBag.category_GKBag = objectindex.categoryListTake6(CategoryIDs.ElementAt(10));

            //Categorywise Posts End
            return View();
        }

        /// <summary>
        /// Current Affairs
        /// </summary>
        /// <returns></returns>
        public ActionResult Category(int catID, int? page)
        {
            ViewBag.CatHeading = (from cat in dbcontext.Categories where cat.CategoryId == catID select cat.CategoryName).FirstOrDefault();

            var CatPosts = (from postdata in dbcontext.Posts
                            join catdata in dbcontext.Categories on postdata.CategoryId equals catdata.CategoryId
                            where postdata.IsDeleted != true && catdata.CategoryId == catID
                            select new PostCategory
                            {
                                PostId = postdata.PostId,
                                CategoryId = postdata.CategoryId,
                                CategoryName = catdata.CategoryName,
                                Title = postdata.Title,
                                Slug = postdata.Slug,
                                Description = postdata.Description,
                                DefaultImage = postdata.DefaultImage,
                                IsPublished = postdata.IsPublished,
                                PublishedDate = postdata.PublishedDate,
                                ModifyDate = postdata.ModifyDate,
                            }).OrderByDescending(x => x.PublishedDate).ToList<PostCategory>();

            var CatePostBag = (from posts in dbcontext.Posts
                               join cat in dbcontext.Categories on posts.CategoryId equals cat.CategoryId
                               where posts.IsDeleted != true && posts.IsPublished == true
                               select posts).OrderByDescending(x => x.PublishedDate).ToList().Take(10);

            ViewBag.CatePostBag = CatePostBag;

            int pageSize = 5;
            int pageNumber = (page ?? 1);
            return View(CatPosts.ToPagedList(pageNumber, pageSize));
        }

        /// <summary>
        /// Current Affair Posts
        /// </summary>
        /// <returns></returns>
        [Authorize(Roles = "User")]
        public ActionResult Posts(Post model, int postid)
        {
            int TabID = 3;
            ////////////////////////////// Like and Comment /////////////////////////////
            ///
            var userProfileID = new GetProfileID(User.Identity.Name)._profileID;
            bool UserActivity = new GetActivity(userProfileID).isActive;
            if (!UserActivity)
            {
                return RedirectToAction("InActive", "Home");
            }
            else
            {
                ViewBag.userProfileID = userProfileID;
                //to check if the post is liked by logged in user
                ViewBag.Liked = new LikeCommentDetail(TabID, postid, userProfileID)._isLiked;

                //to check if the post is Commented by logged in user
                ViewBag.Commented = new LikeCommentDetail(TabID, postid, userProfileID)._isCommented;

                //get likes count and comment count
                ViewBag.LikesCount = new LikeCommentDetail(TabID, postid, userProfileID)._noofLikes;
                ViewBag.CommentsCount = new LikeCommentDetail(TabID, postid, userProfileID)._noofComments;

                //to list all the comments and replies
                CommentAndReply obj = new CommentAndReply();
                ViewBag.listComments = obj.getComments(TabID, postid);
                ViewBag.load_replies = obj.getReplies(TabID, postid);

                /////////////////////////////  Like and Comment ///////////////////////////////

                //to get post details
                var SinglePostCat = (from postdata1 in dbcontext.Posts
                                     join catdata1 in dbcontext.Categories on postdata1.CategoryId equals catdata1.CategoryId
                                     where postdata1.IsDeleted != true && postdata1.PostId == postid
                                     select new PostCategory
                                     {
                                         PostId = postdata1.PostId,
                                         CategoryId = postdata1.CategoryId,
                                         CategoryName = catdata1.CategoryName,
                                         Title = postdata1.Title,
                                         Slug = postdata1.Slug,
                                         Description = postdata1.Description,
                                         Likes = postdata1.Likes,
                                         Comments = postdata1.Comments,
                                         DefaultImage = postdata1.DefaultImage,
                                         IsPublished = postdata1.IsPublished,
                                         PublishedDate = postdata1.PublishedDate,
                                         ModifyDate = postdata1.ModifyDate,
                                     }).OrderByDescending(x => x.PublishedDate).ToList();
                ViewBag.SinglePost = SinglePostCat;

                //to get recent post list [new section]
                var LatestLive = (from postdata1 in dbcontext.Posts
                                  join catdata1 in dbcontext.Categories on postdata1.CategoryId equals catdata1.CategoryId
                                  where postdata1.IsDeleted != true && postdata1.IsPublished == true
                                  select new PostCategory
                                  {
                                      PostId = postdata1.PostId,
                                      CategoryId = postdata1.CategoryId,
                                      CategoryName = catdata1.CategoryName,
                                      Title = postdata1.Title,
                                      Slug = postdata1.Slug,
                                      Description = postdata1.Description,
                                      Likes = postdata1.Likes,
                                      DefaultImage = postdata1.DefaultImage,
                                      IsPublished = postdata1.IsPublished,
                                      PublishedDate = postdata1.PublishedDate,
                                      ModifyDate = postdata1.ModifyDate,
                                  }).OrderByDescending(x => x.PublishedDate).ToList<PostCategory>().Take(10);
                ViewBag.LatestLiveBag = LatestLive;
            }
            return View();
        }

        /// <summary>
        /// Study Material Category
        /// </summary>
        /// <returns></returns>
        [Authorize(Roles = "User")]
        public ActionResult StudyMaterials(int _subID)
        {
            //Description
            var StudyMaterial = dbcontext.StudyMaterials.Where(id => id.StudyID == _subID).FirstOrDefault();
            ViewBag.Category = StudyMaterial.StudyMaterialName;
            ViewBag.Description = StudyMaterial.Description;

            //var Notes = (from _notes in dbcontext.StudyMaterialPosts
            //             where _notes.StudyMaterialID == _subID
            //             select new PostCategory
            //             {
            //                 PostId = _notes.StudyMaterialID,
            //                 CategoryId = (int)_notes.StudyID,
            //                 Title = _notes.Title,
            //                 Description = _notes.Description,
            //                 DefaultImage = _notes.DefaultImage,
            //                 CreatedDate = _notes.CreatedDate
            //             }).ToList();
            //ViewBag.StudyMaterial = Notes;

            ViewBag.StudyMaterial = (from a in dbcontext.StudyMaterialPosts.Where(a => a.StudyID == _subID) select a).ToList();

            //load subjects
            var Subjects = dbcontext.StudyMaterials.Where(parent => parent.ParentID == _subID).Where(active => active.isActive == true).OrderByDescending(x => x.CreatedDate).ToList();
            return View(Subjects);

           
        }

        /// <summary>
        /// Sub Study Material Category
        /// </summary>
        /// <returns></returns>
        public ActionResult Subjects(int? _subjectID, int? _titleID)
        {
            int TabID = 1, TempTitleID = 0;

            var userProfileID = new GetProfileID(User.Identity.Name)._profileID;
            bool UserActivity = new GetActivity(userProfileID).isActive;
            if (!UserActivity)
            {
                return RedirectToAction("InActive", "Home");
            }
            else
            {
                ViewBag.Category = dbcontext.StudyMaterials.Where(studyID => studyID.StudyID == _subjectID).Select(name => name.StudyMaterialName).FirstOrDefault();

                //load Titles
                var Titles = dbcontext.StudyMaterialPosts.Where(subject => subject.StudyID == _subjectID).OrderByDescending(x => x.CreatedDate).ToList();

                if (_titleID != 0 && _titleID != null)
                {
                    //load Post
                    var Notes = (from _notes in dbcontext.StudyMaterialPosts
                                 where _notes.StudyMaterialID == _titleID
                                 select new PostCategory
                                 {
                                     PostId = _notes.StudyMaterialID,
                                     CategoryId = (int)_notes.StudyID,
                                     Title = _notes.Title,
                                     Description = _notes.Description,
                                     DefaultImage = _notes.DefaultImage,
                                     CreatedDate = _notes.CreatedDate
                                 }).OrderByDescending(x => x.CreatedDate).ToList();
                    ViewBag.Notes = Notes;
                }
                else
                {
                    //First Post
                    var Notes = (from _notes in dbcontext.StudyMaterialPosts
                                 where _notes.StudyID == _subjectID
                                 select new PostCategory
                                 {
                                     PostId = _notes.StudyMaterialID,
                                     CategoryId = (int)_notes.StudyID,
                                     Title = _notes.Title,
                                     Description = _notes.Description,
                                     DefaultImage = _notes.DefaultImage,
                                     CreatedDate = _notes.CreatedDate
                                 }).ToList().Take(1);

                    TempTitleID = dbcontext.StudyMaterialPosts.Where(s => s.StudyID == _subjectID).Select(id => id.StudyMaterialID).FirstOrDefault();
                    _titleID = (_titleID == null) ? TempTitleID : _titleID;
                    ViewBag.Notes = Notes;
                }

                ////////////////////////////// Like and Comment /////////////////////////////

                ViewBag.userProfileID = userProfileID;

                //to check if the post is liked by logged in user
                ViewBag.Liked = new LikeCommentDetail(TabID, (int)_titleID, userProfileID)._isLiked;

                //to check if the post is Commented by logged in user
                ViewBag.Commented = new LikeCommentDetail(TabID, (int)_titleID, userProfileID)._isCommented;

                //get likes count and comment count
                ViewBag.LikesCount = new LikeCommentDetail(TabID, (int)_titleID, userProfileID)._noofLikes;
                ViewBag.CommentsCount = new LikeCommentDetail(TabID, (int)_titleID, userProfileID)._noofComments;

                //to list all the comments and replies
                CommentAndReply obj = new CommentAndReply();
                ViewBag.listComments = obj.getComments(TabID, (int)_titleID);
                ViewBag.load_replies = obj.getReplies(TabID, (int)_titleID);

                /////////////////////////////  Like and Comment ///////////////////////////////

                return View(Titles);

            }
        }

        /// <summary>
        /// ScienceTechnology Categories
        /// </summary>
        /// <returns></returns>       
        public ActionResult ScienceTechnology()
        {
            //Load LatestPost
            //Load LatestPost
            var Latest = (from stPost in dbcontext.STPosts
                          join category in dbcontext.ScienceTechnologies on stPost.SubID equals category.ID
                          where stPost.Published == true
                          select new PostCategory
                          {
                              PostId = stPost.ID,
                              CategoryId = (int)stPost.SubID,
                              //CategoryName = category.Name,
                              Title = stPost.Title,
                              Description = stPost.Description,
                              DefaultImage = stPost.DefaultImage,
                              CreatedDate = stPost.CreatedDate
                          }).ToList().OrderByDescending(c => c.CreatedDate).Take(1);
            ViewBag.Latest = Latest;

            //group by Category, Take 1 from all
            //Latest wrt categories- get latest post(1) from all the categories
            var _category = (from d in dbcontext.ScienceTechnologies select d).ToList();
            List<PostCategory> polist = new List<PostCategory>();
            foreach (var ca in _category)
            {

                var p1 = (from postData in dbcontext.STPosts
                          where postData.SubID == ca.ID && postData.Published == true
                          orderby postData.CreatedDate descending
                          select new PostCategory
                          {
                              PostId = postData.ID,
                              CategoryId = (int)postData.SubID,
                              CategoryName = ca.Name,
                              Title = postData.Title,
                              Description = postData.Description,
                              DefaultImage = postData.DefaultImage,
                              IsPublished = (bool)postData.Published,
                              PublishedDate = postData.CreatedDate,
                              ModifyDate = postData.ModifiedDate,
                          }).FirstOrDefault();
                if (p1 != null)
                {
                    polist.Add(p1);
                }
            }
            List<PostCategory> p2 = polist;
            ViewBag.Categories = polist;

            //Most Popular
            var popular = (from stPost in dbcontext.STPosts
                           join category in dbcontext.ScienceTechnologies on stPost.SubID equals category.ID
                           where stPost.Deleted != true && stPost.Published == true
                           select new PostCategory
                           {
                               PostId = stPost.ID,
                               CategoryId = (int)stPost.SubID,
                               CategoryName = category.Name,
                               Title = stPost.Title,
                               Description = stPost.Description,
                               DefaultImage = stPost.DefaultImage,
                               IsPublished = (bool)stPost.Published,
                               CreatedDate = stPost.CreatedDate
                           }).OrderByDescending(x => x.CreatedDate).ToList<PostCategory>().Take(8);
            return View(popular);
        }

        /// <summary>
        /// /Sub Science & Technology
        /// </summary>
        /// <returns></returns> 
        public ActionResult SubST(int? _subSTID, int? _titleID)
        {
            int TabID = 2, TempTitleID = 0;

            var userProfileID = new GetProfileID(User.Identity.Name)._profileID;
            bool UserActivity = new GetActivity(userProfileID).isActive;
            if (!UserActivity)
            {
                return RedirectToAction("InActive", "Home");
            }
            else
            {
                ViewBag.Category = dbcontext.ScienceTechnologies.Where(priID => priID.ID == _subSTID).Select(name => name.Name).FirstOrDefault();

                //load Titles
                var Titles = dbcontext.STPosts.Where(subject => subject.SubID == _subSTID).ToList();

                if (_titleID != 0 && _titleID != null)
                {

                    //load Post
                    var Notes = (from _notes in dbcontext.STPosts
                                 where _notes.ID == _titleID
                                 select new PostCategory
                                 {
                                     PostId = _notes.ID,
                                     CategoryId = (int)_notes.SubID,
                                     Title = _notes.Title,
                                     Description = _notes.Description,
                                     DefaultImage = _notes.DefaultImage,
                                     CreatedDate = _notes.CreatedDate
                                 }).ToList();
                    ViewBag.Notes = Notes;
                }
                else
                {
                    //First Post
                    var Notes = (from _notes in dbcontext.STPosts
                                 where _notes.SubID == _subSTID
                                 select new PostCategory
                                 {
                                     PostId = _notes.ID,
                                     CategoryId = (int)_notes.SubID,
                                     Title = _notes.Title,
                                     Description = _notes.Description,
                                     DefaultImage = _notes.DefaultImage,
                                     CreatedDate = _notes.CreatedDate
                                 }).ToList().Take(1);

                    TempTitleID = dbcontext.STPosts.Where(s => s.SubID == _subSTID).Select(id => id.ID).FirstOrDefault();
                    _titleID = (_titleID == null) ? TempTitleID : _titleID;
                    ViewBag.Notes = Notes;
                }
                ////////////////////////////// Like and Comment /////////////////////////////

                ViewBag.userProfileID = userProfileID;

                //to check if the post is liked by logged in user
                ViewBag.Liked = new LikeCommentDetail(TabID, (int)_titleID, userProfileID)._isLiked;

                //to check if the post is Commented by logged in user
                ViewBag.Commented = new LikeCommentDetail(TabID, (int)_titleID, userProfileID)._isCommented;

                //get likes count and comment count
                ViewBag.LikesCount = new LikeCommentDetail(TabID, (int)_titleID, userProfileID)._noofLikes;
                ViewBag.CommentsCount = new LikeCommentDetail(TabID, (int)_titleID, userProfileID)._noofComments;

                //to list all the comments and replies
                CommentAndReply obj = new CommentAndReply();
                ViewBag.listComments = obj.getComments(TabID, (int)_titleID);
                ViewBag.load_replies = obj.getReplies(TabID, (int)_titleID);

                /////////////////////////////  Like and Comment ///////////////////////////////
                return View(Titles);
            }

        }

        /// <summary>
        /// TalentPlatform Category
        /// </summary>
        /// <returns></returns>
        public ActionResult TalentPlatform()
        {
            var Parallax = dbcontext.Parallaxes.Where(a => a.Active == true && a.Category == "TalentPlatform").OrderBy(cr => cr.CreatedDate).ToList();
            ViewBag.Parallax = Parallax;

            //Load LatestPost
            var Latest = (from taPost in dbcontext.TalentPosts
                          join category in dbcontext.Talents on taPost.SubID equals category.ID
                          where taPost.IsPublished == true
                          select new PostCategory
                          {
                              PostId = taPost.ID,
                              CategoryId = (int)taPost.SubID,
                              //CategoryName = category.Name,
                              Title = taPost.Title,
                              Description = taPost.Description,
                              DefaultImage = taPost.DefaultImage,
                              CreatedDate = taPost.CreatedDate
                          }).ToList().OrderByDescending(c => c.CreatedDate).Take(1);
            ViewBag.Latest = Latest;

            //group by Category, Take 1 from all
            //Latest wrt categories- get latest post(1) from all the categories
            var _category = (from d in dbcontext.Talents select d).ToList();
            List<PostCategory> polist = new List<PostCategory>();
            foreach (var ca in _category)
            {

                var p1 = (from postData in dbcontext.TalentPosts
                          where postData.SubID == ca.ID && postData.IsPublished == true
                          orderby postData.CreatedDate descending
                          select new PostCategory
                          {
                              PostId = postData.ID,
                              CategoryId = (int)postData.SubID,
                              CategoryName = ca.Name,
                              Title = postData.Title,
                              Description = postData.Description,
                              DefaultImage = postData.DefaultImage,
                              IsPublished = (bool)postData.IsPublished,
                              PublishedDate = postData.CreatedDate,
                              ModifyDate = postData.ModifiedDate,
                          }).FirstOrDefault();
                if (p1 != null)
                {
                    polist.Add(p1);
                }
            }
            List<PostCategory> p2 = polist;
            ViewBag.LatestNews = polist;
            return View();
        }

        /// <summary>
        /// Sub TalentPlatform
        /// </summary>
        /// <param name="_subSTID"></param>
        /// <param name="_titleID"></param>
        /// <returns></returns>
        [Authorize(Roles = "User")]
        public ActionResult SubTalPla(int? _subSTID, int? _titleID)
        {
            int TabID = 4, TempTitleID = 0;

            var userProfileID = new GetProfileID(User.Identity.Name)._profileID;
            bool UserActivity = new GetActivity(userProfileID).isActive;
            if (!UserActivity)
            {
                return RedirectToAction("InActive", "Home");
            }
            else
            {
                var Parallax = dbcontext.Parallaxes.Where(a => a.Active == true && a.Category == "TalentPlatform").OrderBy(cr => cr.CreatedDate).ToList();
                ViewBag.Parallax = Parallax;

                ViewBag.Category = dbcontext.Talents.Where(priID => priID.ID == _subSTID).Select(name => name.Name).FirstOrDefault();

                //load Titles
                var Titles = dbcontext.TalentPosts.Where(subject => subject.SubID == _subSTID).ToList();

                if (_titleID != 0 && _titleID != null)
                {
                    //load Post
                    var Notes = (from _notes in dbcontext.TalentPosts
                                 where _notes.ID == _titleID
                                 select new PostCategory
                                 {
                                     PostId = _notes.ID,
                                     CategoryId = _notes.SubID,
                                     Title = _notes.Title,
                                     Description = _notes.Description,
                                     DefaultImage = _notes.DefaultImage,
                                     CreatedDate = _notes.CreatedDate
                                 }).ToList();
                    ViewBag.Notes = Notes;
                }
                else
                {
                    //First Post
                    var Notes = (from _notes in dbcontext.TalentPosts
                                 where _notes.SubID == _subSTID
                                 select new PostCategory
                                 {
                                     PostId = _notes.ID,
                                     CategoryId = _notes.SubID,
                                     Title = _notes.Title,
                                     Description = _notes.Description,
                                     DefaultImage = _notes.DefaultImage,
                                     CreatedDate = _notes.CreatedDate
                                 }).ToList().Take(1);
                    TempTitleID = dbcontext.STPosts.Where(s => s.SubID == _subSTID).Select(id => id.ID).FirstOrDefault();
                    _titleID = (_titleID == null) ? TempTitleID : _titleID;
                    ViewBag.Notes = Notes;
                }
                ////////////////////////////// Like and Comment /////////////////////////////

                ViewBag.userProfileID = userProfileID;

                //to check if the post is liked by logged in user
                ViewBag.Liked = new LikeCommentDetail(TabID, (int)_titleID, userProfileID)._isLiked;

                //to check if the post is Commented by logged in user
                ViewBag.Commented = new LikeCommentDetail(TabID, (int)_titleID, userProfileID)._isCommented;

                //get likes count and comment count
                ViewBag.LikesCount = new LikeCommentDetail(TabID, (int)_titleID, userProfileID)._noofLikes;
                ViewBag.CommentsCount = new LikeCommentDetail(TabID, (int)_titleID, userProfileID)._noofComments;

                //to list all the comments and replies
                CommentAndReply obj = new CommentAndReply();
                ViewBag.listComments = obj.getComments(TabID, (int)_titleID);
                ViewBag.load_replies = obj.getReplies(TabID, (int)_titleID);

                /////////////////////////////  Like and Comment ///////////////////////////////
                return View(Titles);
            }
        }

        /// <summary>
        /// PersonPersonality
        /// </summary>
        /// <param name="page"></param>
        /// <returns></returns>
        public ActionResult PersonPersonality(int? page)
        {
            var PPList1 = (from _person in dbcontext.Person_Personality
                           where _person.Published == true
                           select new PostCategory
                           {
                               PostId = _person.ID,
                               Title = _person.Title,
                               DefaultImage = _person.DefaultImage,
                               Description = _person.Description,
                               IsPublished = (bool)_person.Published,
                               CreatedDate = _person.CreatedDate
                           }).OrderByDescending(c => c.CreatedDate).ToList();
            ViewBag.LatestNews = PPList1;

            int pageSize = 9;
            int pageNumber = (page ?? 1);
            return View(PPList1.ToPagedList(pageNumber, pageSize));
        }


        /// <summary>
        /// PersonPersonality Posts
        /// </summary>
        /// <param name="_subPPID"></param>
        /// <returns></returns>
        [Authorize(Roles = "User")]
        public ActionResult SubPerson(int _subPPID)
        {
            int TabID = 6;

            var userProfileID = new GetProfileID(User.Identity.Name)._profileID;
            bool UserActivity = new GetActivity(userProfileID).isActive;
            if (!UserActivity)
            {
                return RedirectToAction("InActive", "Home");
            }
            else
            {
                var _subPerson = (from _notes in dbcontext.Person_Personality
                                  where _notes.ID == _subPPID
                                  select new PostCategory
                                  {
                                      PostId = _notes.ID,
                                      Title = _notes.Title,
                                      Description = _notes.Description,
                                      DefaultImage = _notes.DefaultImage,
                                      CreatedDate = _notes.CreatedDate
                                  }).ToList();
                ViewBag.Notes = _subPerson;

                ////////////////////////////// Like and Comment /////////////////////////////

                ViewBag.userProfileID = userProfileID;

                //to check if the post is liked by logged in user
                ViewBag.Liked = new LikeCommentDetail(TabID, (int)_subPPID, userProfileID)._isLiked;

                //to check if the post is Commented by logged in user
                ViewBag.Commented = new LikeCommentDetail(TabID, (int)_subPPID, userProfileID)._isCommented;

                //get likes count and comment count
                ViewBag.LikesCount = new LikeCommentDetail(TabID, (int)_subPPID, userProfileID)._noofLikes;
                ViewBag.CommentsCount = new LikeCommentDetail(TabID, (int)_subPPID, userProfileID)._noofComments;

                //to list all the comments and replies
                CommentAndReply obj = new CommentAndReply();
                ViewBag.listComments = obj.getComments(TabID, (int)_subPPID);
                ViewBag.load_replies = obj.getReplies(TabID, (int)_subPPID);

                /////////////////////////////  Like and Comment ///////////////////////////////

                return View();
            }
        }

        /// <summary>
        /// Editorial
        /// </summary>
        /// <param name="model"></param>
        /// <param name="editorialId"></param>
        /// <returns></returns>
        [Authorize(Roles = "User")]
        public ActionResult Editorial(Editorial model, int editorialId)
        {
            int TabID = 7;

            var userProfileID = new GetProfileID(User.Identity.Name)._profileID;
            bool UserActivity = new GetActivity(userProfileID).isActive;
            if (!UserActivity)
            {
                return RedirectToAction("InActive", "Home");
            }
            else
            {
                var listEditorial = (from edito in dbcontext.Editorials
                                     where edito.isDeleted != true /*&& edito.isPublished == true*/
                                     select new PostCategory
                                     {
                                         EditorialID = edito.EditorialID,
                                         Title = edito.Title,
                                         Description = edito.Description,
                                         EditorialImage = edito.EditorialImage,
                                         IsPublished = (bool)edito.isPublished,
                                         CreatedDate = edito.CreatedDate,
                                         ModifyDate = edito.ModifiedDate,
                                     }).OrderByDescending(x => x.CreatedDate).ToList();
                ViewBag.listEditorial = listEditorial;

                //to get Editorial details
                var singleEditorial = (from edito in dbcontext.Editorials
                                       where edito.isDeleted != true && edito.EditorialID == editorialId
                                       select new PostCategory
                                       {
                                           EditorialID = edito.EditorialID,
                                           Title = edito.Title,
                                           Description = edito.Description,
                                           EditorialImage = edito.EditorialImage,
                                           IsPublished = (bool)edito.isPublished,
                                           CreatedDate = edito.CreatedDate,
                                           ModifyDate = edito.ModifiedDate,
                                       }).OrderByDescending(x => x.CreatedDate).ToList();
                ViewBag.singleEditorial = singleEditorial;

                ////////////////////////////// Like and Comment /////////////////////////////

                ViewBag.userProfileID = userProfileID;

                //to check if the post is liked by logged in user
                ViewBag.Liked = new LikeCommentDetail(TabID, editorialId, userProfileID)._isLiked;

                //to check if the post is Commented by logged in user
                ViewBag.Commented = new LikeCommentDetail(TabID, editorialId, userProfileID)._isCommented;

                //get likes count and comment count
                ViewBag.LikesCount = new LikeCommentDetail(TabID, editorialId, userProfileID)._noofLikes;
                ViewBag.CommentsCount = new LikeCommentDetail(TabID, editorialId, userProfileID)._noofComments;

                //to list all the comments and replies
                CommentAndReply obj = new CommentAndReply();
                ViewBag.listComments = obj.getComments(TabID, editorialId);
                ViewBag.load_replies = obj.getReplies(TabID, editorialId);

                /////////////////////////////  Like and Comment ///////////////////////////////
                return View();
            }
        }

        /// <summary>
        /// PersonalityDevelopment
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public ActionResult PersonalityDevelopment()
        {
            ViewBag.Titles = dbcontext.PersonalityDevlopments.Where(p => p.Published == true).ToList();
            var PDList1 = (from _pd in dbcontext.PersonalityDevlopments
                           where _pd.Published == true
                           select new PostCategory
                           {
                               PostId = _pd.PDID,
                               Title = _pd.Title,
                               Description = _pd.Description,
                               DefaultImage = _pd.DefaultImage,
                               CreatedDate = _pd.CreatedDate
                           }).OrderByDescending(date => date.CreatedDate).ToList().Take(1);
            return View(PDList1);
        }

        /// <summary>
        /// PersonalityDevelopment Post
        /// </summary>
        /// <param name="_personalityID"></param>
        /// <returns></returns>
        [Authorize(Roles = "User")]
        public ActionResult PersonalityDevelopmentSub(int? _personalityID)
        {
            int TabID = 8;
            var userProfileID = new GetProfileID(User.Identity.Name)._profileID;
            bool UserActivity = new GetActivity(userProfileID).isActive;
            if (!UserActivity)
            {
                return RedirectToAction("InActive", "Home");
            }
            else
            {
                var Post = (from _pd in dbcontext.PersonalityDevlopments
                            where _pd.PDID == _personalityID
                            select new PostCategory
                            {
                                PostId = _pd.PDID,
                                Title = _pd.Title,
                                Description = _pd.Description,
                                DefaultImage = _pd.DefaultImage,
                                CreatedDate = _pd.CreatedDate
                            }).ToList();

                var Latest = (from _pd in dbcontext.PersonalityDevlopments
                              where _pd.Published == true
                              select new PostCategory
                              {
                                  PostId = _pd.PDID,
                                  Title = _pd.Title,
                                  Description = _pd.Description,
                                  DefaultImage = _pd.DefaultImage,
                                  CreatedDate = _pd.CreatedDate
                              }).OrderByDescending(date => date.CreatedDate).ToList();
                ViewBag.Latest = Latest;

                ////////////////////////////// Like and Comment /////////////////////////////

                ViewBag.userProfileID = userProfileID;

                //to check if the post is liked by logged in user
                ViewBag.Liked = new LikeCommentDetail(TabID, (int)_personalityID, userProfileID)._isLiked;

                //to check if the post is Commented by logged in user
                ViewBag.Commented = new LikeCommentDetail(TabID, (int)_personalityID, userProfileID)._isCommented;

                //get likes count and comment count
                ViewBag.LikesCount = new LikeCommentDetail(TabID, (int)_personalityID, userProfileID)._noofLikes;
                ViewBag.CommentsCount = new LikeCommentDetail(TabID, (int)_personalityID, userProfileID)._noofComments;

                //to list all the comments and replies
                CommentAndReply obj = new CommentAndReply();
                ViewBag.listComments = obj.getComments(TabID, (int)_personalityID);
                ViewBag.load_replies = obj.getReplies(TabID, (int)_personalityID);

                /////////////////////////////  Like and Comment ///////////////////////////////
                return View(Post);
            }
        }

        /// <summary>
        /// Notification start 
        /// </summary>
        /// <returns></returns>
        public ActionResult Notification(int? page)
        {
            var userProfileID = new GetProfileID(User.Identity.Name)._profileID;
            bool UserActivity = new GetActivity(userProfileID).isActive;
            if (!UserActivity)
            {
                return RedirectToAction("InActive", "Home");
            }
            else
            {
                var Latest3 = (from notify in dbcontext.Notifications
                               where notify.Deleted != true
                               select new PostCategory
                               {
                                   PostId = notify.ID,
                                   Title = notify.Title,
                                   Description = notify.Description,
                                   Attachment = notify.Attachment,
                                   IsPublished = (bool)notify.Published,
                                   CreatedDate = notify.CreatedDate
                               }).OrderByDescending(d => d.CreatedDate).ToList();
                ViewBag.Latest3 = Latest3;

                var Recent = (from notify in dbcontext.Notifications
                              where notify.Deleted != true
                              select new PostCategory
                              {
                                  PostId = notify.ID,
                                  Title = notify.Title,
                                  Description = notify.Description,
                                  Attachment = notify.Attachment,
                                  IsPublished = (bool)notify.Published,
                                  CreatedDate = notify.CreatedDate
                              }).OrderByDescending(d => d.CreatedDate).Take(10).ToList();
                ViewBag.Recent = Recent;

                int pageSize = 3;
                int pageNumber = (page ?? 1);
                return View(Latest3.ToPagedList(pageNumber, pageSize));
            }

        }


        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public ActionResult LearnMore(int? _categoryID, int? _titleID)
        {
            int TabID = 5, TempTitleID = 0;

            var userProfileID = new GetProfileID(User.Identity.Name)._profileID;
            bool UserActivity = new GetActivity(userProfileID).isActive;
            if (!UserActivity)
            {
                return RedirectToAction("InActive", "Home");
            }
            else
            {
                ViewBag.Category = dbcontext.Learns.Where(learn => learn.ID == _categoryID).Select(name => name.Name).FirstOrDefault();

                //load Titles
                var Titles = dbcontext.LearnPosts.Where(learn => learn.SubID == _categoryID).ToList();

                if (_titleID != 0 && _titleID != null)
                {
                    //load Post
                    var Notes = (from _notes in dbcontext.LearnPosts
                                 where _notes.ID == _titleID
                                 select new PostCategory
                                 {
                                     PostId = _notes.ID,
                                     CategoryId = (int)_notes.SubID,
                                     Title = _notes.Title,
                                     Description = _notes.Description,
                                     DefaultImage = _notes.DefaultImage,
                                     CreatedDate = _notes.CreatedDate
                                 }).ToList();
                    ViewBag.Notes = Notes;
                }
                else
                {
                    //First Post
                    var Notes = (from _notes in dbcontext.LearnPosts
                                 where _notes.SubID == _categoryID
                                 select new PostCategory
                                 {
                                     PostId = _notes.ID,
                                     CategoryId = (int)_notes.SubID,
                                     Title = _notes.Title,
                                     Description = _notes.Description,
                                     DefaultImage = _notes.DefaultImage,
                                     CreatedDate = _notes.CreatedDate
                                 }).ToList().Take(1);

                    TempTitleID = dbcontext.LearnPosts.Where(l => l.SubID == _categoryID).Select(id => id.ID).FirstOrDefault();
                    _titleID = (_titleID == null) ? TempTitleID : _titleID;

                    ViewBag.Notes = Notes;
                }
                ////////////////////////////// Like and Comment /////////////////////////////

                ViewBag.userProfileID = userProfileID;

                //to check if the post is liked by logged in user
                ViewBag.Liked = new LikeCommentDetail(TabID, (int)_titleID, userProfileID)._isLiked;

                //to check if the post is Commented by logged in user
                ViewBag.Commented = new LikeCommentDetail(TabID, (int)_titleID, userProfileID)._isCommented;

                //get likes count and comment count
                ViewBag.LikesCount = new LikeCommentDetail(TabID, (int)_titleID, userProfileID)._noofLikes;
                ViewBag.CommentsCount = new LikeCommentDetail(TabID, (int)_titleID, userProfileID)._noofComments;

                //to list all the comments and replies
                CommentAndReply obj = new CommentAndReply();
                ViewBag.listComments = obj.getComments(TabID, (int)_titleID);
                ViewBag.load_replies = obj.getReplies(TabID, (int)_titleID);

                /////////////////////////////  Like and Comment ///////////////////////////////
                return View(Titles);
            }            
        }


        /// <summary>
        /// ////////////////////
        /// ////////////////////User Profile Start
        /// ////////////////////
        /// </summary>
        /// <returns></returns>

        public void bindSubscription()
        {
            List<Subscription> subscription = dbcontext.Subscriptions.Where(active => active.Active == true).ToList();
            Subscription s = new Subscription();
            s.SubscriptionID = 0;
            s.Name = "--Choose Your Subscription";
            subscription.Insert(0, s);
            SelectList SubBag = new SelectList(subscription, "SubscriptionID", "Name", 0);
            ViewBag.SubBag = SubBag;
        }

        [Authorize(Roles = "User")]
        [HttpGet]
        public ActionResult CreateProfile()
        {
            bindSubscription();

            string tempUsername = User.Identity.Name.ToString();
            var pdata = (from p in dbcontext.AspNetUsers
                         where p.UserName == tempUsername
                         select new { UserName = tempUsername }).FirstOrDefault();

            Profile ptemp = new Profile();
            ptemp.UserName = pdata.UserName;
            return View(ptemp);
        }

        [HttpPost]
        public ActionResult CreateProfile(Profile profiledata, /*string date,*/ HttpPostedFileBase FileUpload)
        {
            //CreateProfile();
            //var dob = DateTime.Parse(date);

            string filesnames = "default.jpg";
            if (FileUpload != null)
            {
                if (!System.IO.Directory.Exists(Server.MapPath("\\Content\\Uploads\\Profile" + 1)))
                {
                    string savedFileName = DateTime.Now.ToString("MMddHHmmssfff") + Path.GetFileName(FileUpload.FileName);
                    FileUpload.SaveAs(Path.Combine(Server.MapPath("~/Content/Uploads/Profile"), savedFileName));
                    filesnames = savedFileName;
                }
            }

            Profile pfdata = new Profile()
            {
                Image = filesnames,
                FirstName = profiledata.FirstName,
                LastName = profiledata.LastName,
                UserName = User.Identity.Name,
                //Qualification = profiledata.Qualification,
                //About = profiledata.About,
                //DOB = dob,
                //Aim = profiledata.Aim,
                //AspiringExams = profiledata.AspiringExams,
                //Address = profiledata.Address,
                MobileNo = profiledata.MobileNo,
                SubscribedPlan = profiledata.SubscribedPlan,
                SubscribedTest = profiledata.SubscribedTest,
                isActive = false,
                CreatedDate = DateTime.Now,
            };
            dbcontext.Profiles.Add(pfdata);
            int save = dbcontext.SaveChanges();
            return RedirectToAction("ViewProfile", "User");

            //if (profiledata.UserName != null && profiledata.UserName.Length > 0)
            //{
            //    if (Exists(profiledata.FirstName))
            //    {
            //        ModelState.AddModelError("username", "Username exists");
            //    }
            //}
        }

        private bool Exists(string username)
        {
            return dbcontext.Profiles.Count(a => a.UserName.Equals(username)) > 0;
        }

        [Authorize(Roles = "User")]
        public ActionResult ViewProfile()
        {
            var RegUser = dbcontext.Profiles.Where(u => u.UserName == User.Identity.Name.ToString()).FirstOrDefault();
            if (RegUser != null)
            {
                bindSubscription();
                int Userid = new GetProfileID(User.Identity.Name)._profileID;
                ViewBag.data = new SubcriptionDetail(Userid).EndDays;
                return View(RegUser);
            }
            else
            {
                return RedirectToAction("CreateProfile", "User");
            }
        }

        [HttpPost]
        public ActionResult ViewProfile(Profile model/*, string date*/, HttpPostedFileBase FileUpload)
        {
            bindSubscription();
            int Userid = new GetProfileID(User.Identity.Name)._profileID;
            ViewBag.data = new SubcriptionDetail(Userid).EndDays;

            string filesnames = dbcontext.Profiles.Where(i => i.ProfileID == model.ProfileID).Select(i => i.Image).FirstOrDefault();
            filesnames = filesnames == null ? "default.jpg" : filesnames;

            if (FileUpload != null)
            {
                if (!System.IO.Directory.Exists(Server.MapPath("\\Content\\Uploads\\Profile" + 1)))
                {
                    string savedFileName = DateTime.Now.ToString("MMddHHmmssfff") + Path.GetFileName(FileUpload.FileName);
                    FileUpload.SaveAs(Path.Combine(Server.MapPath("~/Content/Uploads/Profile"), savedFileName));
                    filesnames = savedFileName;
                }
            }
            //var dob = DateTime.Parse(date);
            Profile UserDetail = dbcontext.Profiles.Where(u => u.UserName == User.Identity.Name).FirstOrDefault();

            Profile profEdit = dbcontext.Profiles.Find(model.ProfileID);
            if (profEdit.ProfileID == model.ProfileID)
            {
                profEdit.Image = filesnames;
                profEdit.FirstName = model.FirstName;
                profEdit.LastName = model.LastName;
                //profEdit.DOB = dob;
                //profEdit.Qualification = model.Qualification;
                //profEdit.About = model.About;
                //profEdit.Aim = model.Aim;
                profEdit.MobileNo = model.MobileNo;
                //profEdit.AspiringExams = model.AspiringExams;
                //profEdit.Address = model.Address;
                profEdit.SubscribedPlan = UserDetail.SubscribedPlan;
                profEdit.SubscribedTest = model.SubscribedTest;
                profEdit.ModifiedDate = DateTime.Now;
            }
            dbcontext.Profiles.AddOrUpdate(profEdit);
            int c = dbcontext.SaveChanges();
            if (c > 0)
            {
                ViewBag.Sucess = "Sucessfully Updated";
            }
            TempData["msg"] = "<script>alert('Updated Succesfully');</script>";
            return View(profEdit);
        }

        /// <summary>
        /// Liked Posts by Logged in User
        /// </summary>
        /// <returns></returns>
        public ActionResult LikedPost()
        {
            int profile = new GetProfileID(User.Identity.Name)._profileID;
            var Likes = dbcontext.Likes.Where(user => user.ProfileID == profile).ToList();
            return View(Likes);
        }

        /// <summary>
        /// ////////////////////
        /// ////////////////////User Profile Start End
        /// ////////////////////
        /// </summary>
        /// <returns></returns>


        /// <summary>
        /// ///////////////
        /// ///////////////
        /// Like , UnLike , Comment , Reply start
        /// </summary>
        /// <param name="PostsID"></param>
        /// <param name="TabID"></param>
        /// <returns></returns>

        [Authorize(Roles = "User")]
        //[HttpPost]
        public JsonResult LikePost(int PostsID, int TabID)
        {
            int? likeCount = 0; string status = "";
            int getUserID = new GetProfileID(User.Identity.Name)._profileID;

            bool isnull = string.IsNullOrEmpty(User.Identity.Name);
            if (isnull)
            {
                RedirectToAction("Login", "Account");
                return Json(new { code = 1 });
            }
            else
            {
                try
                {
                    CommentAndReply obj = new CommentAndReply();
                    likeCount = obj.addLike(PostsID, TabID);

                    Like like = new Like();
                    {
                        like.PostID = PostsID;
                        like.ProfileID = getUserID;
                        like.Liked = true;
                        like.SectionID = TabID;
                        like.CreatedDate = DateTime.Now;
                        dbcontext.Likes.Add(like);
                        int save = dbcontext.SaveChanges();
                        if (save > 0)
                        {
                            ViewBag.Save = "sucessfull";
                        }
                    }
                    status = "updated Successfully";
                }
                catch (Exception)
                {
                    status = "Please Try again";
                }
                return Json(new { lcount = likeCount, responstext = status }, JsonRequestBehavior.AllowGet);
            }
        }

        [Authorize(Roles = "User")]
        public JsonResult UnlikePost(int PostsID, int TabID)
        {
            int? likeCount = 0; string status = "";
            int getUserID = new GetProfileID(User.Identity.Name)._profileID;

            bool isnull = string.IsNullOrEmpty(User.Identity.Name);
            if (isnull)
            {
                RedirectToAction("Login", "Account");
                return Json(new { code = 1 });
            }
            else
            {
                try
                {
                    CommentAndReply obj = new CommentAndReply();
                    likeCount = obj.unLike(PostsID, TabID, getUserID);

                    ViewBag.LikesCount = likeCount;
                    status = "updated Successfully";
                }
                catch (Exception)
                {
                    status = "Please Try again";
                }
                return Json(new { lcount = likeCount, responstext = status }, JsonRequestBehavior.AllowGet);
            }
        }

        [Authorize(Roles = "User")]
        public JsonResult PostComment(string Comment, int PostID, int UserID, int TabID)
        {
            int? CommentCount = 0; string status = "";
            int getUserID = new GetProfileID(User.Identity.Name)._profileID;

            bool isnull = string.IsNullOrEmpty(User.Identity.Name);
            if (isnull)
            {
                RedirectToAction("Login", "Account");
                return Json(new { code = 1 });
            }
            else
            {
                try
                {
                    CommentAndReply obj = new CommentAndReply();
                    CommentCount = obj.addComment(UserID, TabID, PostID, Comment);

                    Comment _insert = new Comment();
                    {
                        _insert.PostID = PostID;
                        _insert.ProfileID = UserID;
                        _insert.Comments = Comment;
                        _insert.ReplyCount = 0;
                        _insert.SectionID = TabID;
                        _insert.CreatedDate = DateTime.Now;
                    }
                    dbcontext.Comments.Add(_insert);
                    int save = dbcontext.SaveChanges();
                    if (save > 0)
                    {
                        status = "commented Successfully";
                    }
                }
                catch (Exception)
                {
                    status = "Please Try again";
                }
                return Json(new { lcomcount = CommentCount, responstext = status }, JsonRequestBehavior.AllowGet);
            }
        }

        [Authorize(Roles = "User")]
        public JsonResult ReplyComment(string reply, int PostID, int CommentId, int UserID, int TabID)
        {
            int? ReplyCount = 0; string status = "";

            int getUserID = new GetProfileID(User.Identity.Name)._profileID;

            bool isnull = string.IsNullOrEmpty(User.Identity.Name);
            if (isnull)
            {
                RedirectToAction("Login", "Account");
                return Json(new { code = 1 });
            }
            else
            {
                try
                {
                    CommentAndReply obj = new CommentAndReply();
                    ReplyCount = obj.addReply(reply, PostID, CommentId, UserID, TabID);
                    status = "Replied Successfully";
                }
                catch (Exception)
                {
                    status = "Please Try again";
                }
                return Json(new { lcomcount = ReplyCount, responstext = status }, JsonRequestBehavior.AllowGet);
            }
        }

        /// <summary>
        /// ////////////////////
        /// ////////////////////Like , UnLike , Comment , Reply
        /// ////////////////////
        /// </summary>
        /// <returns></returns>
        /// 

        public ActionResult Test()
        {
            return View();
        }

    }
}