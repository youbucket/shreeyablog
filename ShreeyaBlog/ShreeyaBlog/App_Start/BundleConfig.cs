﻿using System.Web;
using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;

namespace ShreeyaBlog
{
    public class BundleConfig
    {
        // For more information on bundling, visit https://go.microsoft.com/fwlink/?LinkId=301862
        public static void RegisterBundles(BundleCollection bundles)
        {
            bundles.Add(new ScriptBundle("~/bundles/jquery").Include(
                        "~/Scripts/jquery-{version}.js"));

            bundles.Add(new ScriptBundle("~/bundles/jqueryval").Include(
                        "~/Scripts/jquery.validate*"));

            bundles.Add(new ScriptBundle("~/bundles/summernote").Include(
                        "~/Scripts/summernote/summernote*"));

            // Use the development version of Modernizr to develop with and learn from. Then, when you're
            // ready for production, use the build tool at https://modernizr.com to pick only the tests you need.
            bundles.Add(new ScriptBundle("~/bundles/modernizr").Include(
                        "~/Scripts/modernizr-*"));

            bundles.Add(new ScriptBundle("~/bundles/bootstrap").Include(
                      "~/Scripts/bootstrap.js"));

            bundles.Add(new StyleBundle("~/Content/css").Include(
                      "~/Content/bootstrap.css",
                      "~/Content/site.css"));            

            bundles.Add(new ScriptBundle("~/bundle/base").Include(
               "~/Scripts/jquery-3.3.1.js",
                "~/Scripts/bootstrap.js"
               ));

            bundles.Add(new StyleBundle("~/Content/summernote")
                .Include("~/Content/summernote*"));

            bundles.Add(new ScriptBundle("~/bundles/Script-custom-editor").Include(
           "~/Scripts/script-custom-editor.js"));

            bundles.UseCdn = false;   //enable CDN support
            BundleTable.EnableOptimizations = false;
        }
 
    }
}
